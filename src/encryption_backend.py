from cryptography.fernet import Fernet
import jwt
import datetime

AUTH_TOKEN_KEY = "BUMIATHTOKENKEY"


def create_auth_token():
    """Creating a token

    Returns:
        String: encoded token
    """
    payload = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(hours=10) #The token expires after 10 hours
        }
    token = jwt.encode(
            payload,
            AUTH_TOKEN_KEY,
            algorithm='HS256'
        )
    return token

token = create_auth_token()
print(token)
