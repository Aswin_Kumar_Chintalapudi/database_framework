from utils.response import create_success_response,create_error_response
from utils.encryption import decrypt_message
from utils.encryption_backend import decode_auth_token_backend
from flask import make_response,request
from models import UserModel
from functools import wraps


def authenticate(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        payload = request.get_json()
        if not payload:
            return make_response(create_error_response(None, "Request payload not present", 400), 400)
        if isinstance(payload, dict):
            if "session_user_id" not in payload:
                return make_response(create_error_response(None, "Session UserId not present in payload", 400), 400)
        else:
            return make_response(create_error_response(None, "Bad Request", 400), 400)
        user_id = str(payload["session_user_id"])
        user_data = UserModel.query.filter(UserModel.user_id == user_id).first()
        if user_data:
            if not user_data.authToken:
                return make_response(
                    create_error_response(None, "Authorization token is not Available.Please login again.", 401),
                    401)
            userToken = decrypt_message(user_data.authToken)
            if not userToken:
                return make_response(
                    create_error_response(None, "Authorization token is not Available.Please login again.", 401),
                    401)
            if userToken == user_id:
                return func(*args, **kwargs)
            else:
                return make_response(create_error_response(None, "Invalid Token.Please login again.", 401), 401)
        return make_response(create_error_response(None, "UserId not Found", 401), 401)

    return wrapper


def authenticate_backend(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        auth_token = request.args.get('auth_token', '')
        if not auth_token:
            return make_response(create_error_response(None, "Authorization token not present in the request", 400), 400)
        auth_response = decode_auth_token_backend(auth_token)
        if "ERROR_SIGN" in auth_response:
            return make_response(create_error_response(None, "Authorization token expired. Please log in again.", 403), 403)
        if "ERROR_INVALID" in auth_response:
            return make_response(create_error_response(None, "Invalid authorization token. Please log in again.", 403), 403)
        return func(*args, **kwargs)
    return wrapper