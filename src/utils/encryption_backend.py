from cryptography.fernet import Fernet
import jwt
import datetime

AUTH_TOKEN_KEY = "BUMIATHTOKENKEY"


def create_auth_token():
    """Creating a token

    Returns:
        String: encoded token
    """
    payload = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(hours=10) #The token expires after 10 hours
        }
    token = jwt.encode(
            payload,
            AUTH_TOKEN_KEY,
            algorithm='HS256'
        )
    return token

token = create_auth_token()
print(token)


def decode_auth_token_backend(authToken):
    """decode auth token for backend modules

    Args:
        authToken (str): auth token from backend

    Returns:
        str: status of token
    """
    try:
        payload = jwt.decode(authToken, AUTH_TOKEN_KEY, algorithms=["HS256"])
        return payload
    except jwt.ExpiredSignatureError:
        return "ERROR_SIGN: Signature expired. Please log in again."
    except jwt.InvalidTokenError:
        return "ERROR_INVALID: Invalid token. Please log in again."
