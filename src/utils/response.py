from flask import json

def create_success_response(result, message=None):
    """Create success response in json format

    Args:
        result (str): data
        message ([type], optional): Success Message. Defaults to None.

    Returns:
        json: response json
    """
    response_data = dict()
    response_data["status"] = "success"
    response_data["code"] = 200
    response_data["message"] = message
    response_data["data"] = result
    response_json = json.jsonify(response_data)
    return response_json


def create_error_response(result, message=None, error_code=500):
    """create error response in json format

    Args:
        result (str): data
        message (str, optional): Error Message. Defaults to None.
        error_code (int, optional): status code. Defaults to 500.

    Returns:
        json: reponse json
    """
    response_data = dict()
    response_data["status"] = "error"
    response_data["code"] = error_code
    response_data["message"] = message
    response_data["data"] = None
    response_json = json.jsonify(response_data)
    return response_json