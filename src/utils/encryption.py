from cryptography.fernet import Fernet

import jwt

import datetime


def encrypt_message(message):
    """Encryption of input string

    Args:
        message (str): input string

    Returns:
        str: encrypted string with respect to key defined
    """
    key = b"Q7Ph9zTJ0uc3TxyPK5QSlBnqD2bxHQwXQZVG2aO4bCo="
    encoded_text = message.encode()
    cipher_suite = Fernet(key)
    encoded_message = cipher_suite.encrypt(encoded_text)
    return encoded_message


def decrypt_message(msg_received):
    """Decryption of input string

    Args:
        msg_received (str): input string

    Returns:
        str: decrypted string
    """
    key = b"Q7Ph9zTJ0uc3TxyPK5QSlBnqD2bxHQwXQZVG2aO4bCo="
    cipher_suite = Fernet(key)
    encoded_text = msg_received  # .encode()
    decoded_text = cipher_suite.decrypt(encoded_text)
    decoded_text = decoded_text.decode("utf-8")
    return decoded_text



def create_auth_token(uuid,AUTH_TOKEN_KEY):
    payload = {
        'exp': datetime.datetime.utcnow() + datetime.timedelta(hours=8),
        'sub': uuid
    }
    token = jwt.encode(
        payload,
        AUTH_TOKEN_KEY,
        algorithm='HS256'
    )
    return token


def decode_auth_token(authToken,AUTH_TOKEN_KEY):
    try:
        payload = jwt.decode(authToken, AUTH_TOKEN_KEY, algorithms=["HS256"])
        return payload['sub']
    except jwt.ExpiredSignatureError:
        return "ERROR: Signature expired. Please log in again."
    except jwt.InvalidTokenError:
        return "ERROR: Invalid token. Please log in again."

def decrypt_db_password(msg_received):
    """
    decrypts db password
    :param msg_received: Db password
    :return: decrypted db password
    """
    try:
        key = b"Q7Ph9zTJ0uc3TxyPK5QSlBnqD2bxHQwXQZVG2aO4bCo="  # this is your "password"
        cipher_suite = Fernet(key)
        encoded_text = msg_received.encode()
        decoded_text = cipher_suite.decrypt(encoded_text)
        decoded_text = decoded_text.decode("utf-8")
        return decoded_text
    except Exception as e:
        print(f"decrypt message error : {e}")
        return "error obtained"
#token = create_auth_token("garima")
# print(decode_auth_token("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MTMxMjY3OTUsInN1YiI6IkFTV0lOIn0.6jBr0gkjWxOKFcOwlEv15OzI7EqTWaxNPi4FdKAkscM"))
