
from flask_restplus import reqparse

ComplimentarySectionsParser = reqparse.RequestParser()
ComplimentarySectionsParser.add_argument('keywords')
ComplimentarySectionsParser.add_argument('synonyms')



ComplimentarySectionsInsertParser = reqparse.RequestParser()
ComplimentarySectionsInsertParser.add_argument('sections', required=True)
ComplimentarySectionsInsertParser.add_argument('keywords',default=None)
ComplimentarySectionsInsertParser.add_argument('synonyms',default=None)
