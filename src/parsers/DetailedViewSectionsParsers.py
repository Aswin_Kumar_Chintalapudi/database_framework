from flask_restplus import reqparse

detailedViewSectionsCreateParser = reqparse.RequestParser()
detailedViewSectionsCreateParser.add_argument('comment', required=True)
detailedViewSectionsCreateParser.add_argument('department_Name', required=True)
detailedViewSectionsCreateParser.add_argument('section_Name', required=True)
detailedViewSectionsCreateParser.add_argument('section_Value', required=True)
detailedViewSectionsCreateParser.add_argument('tId', required=True)
detailedViewSectionsCreateParser.add_argument('user_Name', required=True)

detailedViewSectionsUpdateParser = reqparse.RequestParser()
detailedViewSectionsUpdateParser.add_argument('comment', required=True)
detailedViewSectionsUpdateParser.add_argument('department_Name', required=True)
detailedViewSectionsUpdateParser.add_argument('section_Name', required=True)
detailedViewSectionsUpdateParser.add_argument('section_Value', required=True)
detailedViewSectionsUpdateParser.add_argument('tId', required=True)
detailedViewSectionsUpdateParser.add_argument('user_Name', required=True)

detailedViewSectionsDetailsParser = reqparse.RequestParser()
detailedViewSectionsDetailsParser.add_argument('tId', required=True)