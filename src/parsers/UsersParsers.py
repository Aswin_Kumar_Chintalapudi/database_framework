from flask_restplus import reqparse

userCreateParser = reqparse.RequestParser()
userCreateParser.add_argument('user_id', required=True)
userCreateParser.add_argument('password', required=True)
userCreateParser.add_argument('departmentName', required=True)
userCreateParser.add_argument('emailId', required=True)

userUpdateParser = reqparse.RequestParser()
userUpdateParser.add_argument('password', default=None)
userUpdateParser.add_argument('departmentName', default=None)
userUpdateParser.add_argument('emailId', default=None)
userUpdateParser.add_argument('WelcomeFlag', default=0)

userDetailsParser = reqparse.RequestParser()
userDetailsParser.add_argument('user_id', required=True)

filterByDeptParser = reqparse.RequestParser()
filterByDeptParser.add_argument('departmentName')

loginParser = reqparse.RequestParser()
loginParser.add_argument('user_id', required=True)
loginParser.add_argument('password', required=True)

logoutParser = reqparse.RequestParser()
logoutParser.add_argument('user_id', required=True)

adminCreateParser = reqparse.RequestParser()
adminCreateParser.add_argument('password', required=True)
adminCreateParser.add_argument('departmentName', default=None)
adminCreateParser.add_argument('emailId', default=None)