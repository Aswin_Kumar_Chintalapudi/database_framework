from flask_restplus import reqparse

CustomSeededsectionsParser = reqparse.RequestParser()
CustomSeededsectionsParser.add_argument('id')
CustomSeededsectionsParser.add_argument('domain', required=True, default=None)
CustomSeededsectionsParser.add_argument('instance', required=True, default=None)
CustomSeededsectionsParser.add_argument('keywords', default=None)
CustomSeededsectionsParser.add_argument('sections', default=None)
CustomSeededsectionsParser.add_argument('synonyms', default=None)


CustomSeededsectionsUpdateParser = reqparse.RequestParser()
CustomSeededsectionsUpdateParser.add_argument('id')
CustomSeededsectionsUpdateParser.add_argument('domain', default=None)
CustomSeededsectionsUpdateParser.add_argument('instance', default=None)
CustomSeededsectionsUpdateParser.add_argument('keywords', default=None)
CustomSeededsectionsUpdateParser.add_argument('sections', default=None)
CustomSeededsectionsUpdateParser.add_argument('synonyms', default=None)