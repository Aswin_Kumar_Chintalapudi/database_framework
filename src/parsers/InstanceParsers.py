from flask_restplus import reqparse

customInstanceParser = reqparse.RequestParser()
customInstanceParser.add_argument('instance_name', required=True)
customInstanceParser.add_argument('instance_domain', required=True)


filterByInstanceParser = reqparse.RequestParser()
filterByInstanceParser.add_argument('instance_name', required=True)


instanceCreateParser = reqparse.RequestParser()
instanceCreateParser.add_argument('instance_name', required=True)
instanceCreateParser.add_argument('instance_description', default=None)
instanceCreateParser.add_argument('created_by', default=None)
instanceCreateParser.add_argument('instance_domain', default=None)
instanceCreateParser.add_argument('updated_by', default=None)
instanceCreateParser.add_argument('departmentName', default=None)

instanceUpdateParser = reqparse.RequestParser()
instanceUpdateParser.add_argument('instance_name', default=None)
instanceUpdateParser.add_argument('instance_description', default=None)
instanceUpdateParser.add_argument('created_by', default=None)
instanceUpdateParser.add_argument('instance_domain', default=None)
instanceUpdateParser.add_argument('updated_by', default=None)
instanceUpdateParser.add_argument('departmentName', default=None)

instanceDetailsParser = reqparse.RequestParser()
instanceDetailsParser.add_argument('Instance_id', required=True)
