from flask_restplus import reqparse

customConfigrePathInsertParser = reqparse.RequestParser()
customConfigrePathInsertParser.add_argument('Id')
customConfigrePathInsertParser.add_argument('Input')
customConfigrePathInsertParser.add_argument('Working')
customConfigrePathInsertParser.add_argument('Output')
customConfigrePathInsertParser.add_argument('License_Key')
customConfigrePathInsertParser.add_argument('License_Flag')
customConfigrePathInsertParser.add_argument('Archive')
customConfigrePathInsertParser.add_argument('License_Desc')
customConfigrePathInsertParser.add_argument('tempInput')
customConfigrePathInsertParser.add_argument('TestFile')


customConfigrePathDeleteParser = reqparse.RequestParser()
customConfigrePathDeleteParser.add_argument('Id', required=True)


customConfigrePathUpdateParser = reqparse.RequestParser()
customConfigrePathUpdateParser.add_argument('Id', required=True)
customConfigrePathUpdateParser.add_argument('Input')
customConfigrePathUpdateParser.add_argument('Working')
customConfigrePathUpdateParser.add_argument('Output')
customConfigrePathUpdateParser.add_argument('License_Key')
customConfigrePathUpdateParser.add_argument('License_Flag')
customConfigrePathUpdateParser.add_argument('Archive')
customConfigrePathUpdateParser.add_argument('License_Desc')
customConfigrePathUpdateParser.add_argument('tempInput')
customConfigrePathUpdateParser.add_argument('TestFile')
