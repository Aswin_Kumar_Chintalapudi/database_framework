from flask_restplus import reqparse

CustomDomainInstanceParser = reqparse.RequestParser()
CustomDomainInstanceParser.add_argument('id')
CustomDomainInstanceParser.add_argument('domain', required=True, default=None)
CustomDomainInstanceParser.add_argument('instance', required=True, default=None)
CustomDomainInstanceParser.add_argument('keywords', default=None)
CustomDomainInstanceParser.add_argument('sections', default=None)
CustomDomainInstanceParser.add_argument('synonyms', default=None)


CustomDomainInstanceUpdateParser = reqparse.RequestParser()
CustomDomainInstanceUpdateParser.add_argument('id')
CustomDomainInstanceUpdateParser.add_argument('domain', default=None)
CustomDomainInstanceUpdateParser.add_argument('instance', default=None)
CustomDomainInstanceUpdateParser.add_argument('keywords', default=None)
CustomDomainInstanceUpdateParser.add_argument('sections', default=None)
CustomDomainInstanceUpdateParser.add_argument('synonyms', default=None)