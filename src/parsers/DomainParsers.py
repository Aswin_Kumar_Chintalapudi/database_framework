from flask_restplus import reqparse

domainCreateParser = reqparse.RequestParser()
domainCreateParser.add_argument('domainName', required=True)
domainCreateParser.add_argument('domainDescription', default=None)
domainCreateParser.add_argument('isDeletable', default=0)
domainCreateParser.add_argument('createdBy', required=True)
domainCreateParser.add_argument('departmentName', default=None)

domainUpdateParser = reqparse.RequestParser()
domainUpdateParser.add_argument('domainName', default=None)
domainUpdateParser.add_argument('domainDescription', default=None)
domainUpdateParser.add_argument('isDeletable', default=None)
domainUpdateParser.add_argument('createdBy', default=None)
domainUpdateParser.add_argument('departmentName', default=None)