from flask_restplus import reqparse

departmentCreateParser = reqparse.RequestParser()
departmentCreateParser.add_argument('departmentName', required=True)
departmentCreateParser.add_argument('departmentDescription', default=None)
departmentCreateParser.add_argument('createdBy', required=True)

departmentUpdateParser = reqparse.RequestParser()
departmentUpdateParser.add_argument('departmentName')
departmentUpdateParser.add_argument('departmentDescription', default=None)
departmentUpdateParser.add_argument('createdBy', default=None)

departmentDetailsParser = reqparse.RequestParser()
departmentDetailsParser.add_argument('departmentName', required=True)