from flask_restplus import reqparse

customTransactionParser = reqparse.RequestParser()
customTransactionParser.add_argument('ID')
customTransactionParser.add_argument('InstanceName', required=True)
customTransactionParser.add_argument('DomainType', required=True)
customTransactionParser.add_argument('Filepath', required=True)
customTransactionParser.add_argument('Filename', required=True)
customTransactionParser.add_argument('Filestatus', required=True)
customTransactionParser.add_argument('departmentName', required=True)
customTransactionParser.add_argument('userName', required=True)
customTransactionParser.add_argument('wdfilepath_exists')
customTransactionParser.add_argument('ui_flag', default=0)
customTransactionParser.add_argument('no_pages', default=None)
customTransactionParser.add_argument('working_file_path', default=None)
customTransactionParser.add_argument('out_csv_file_path', default=None)
customTransactionParser.add_argument('error_description', default=None)
customTransactionParser.add_argument('payload_request', default=None)
customTransactionParser.add_argument('payload_response', default=None)


customTransactionFileUpdateParser = reqparse.RequestParser()
customTransactionFileUpdateParser.add_argument('ID', type=int)
customTransactionFileUpdateParser.add_argument('Error_Description', type=str, default=None)
customTransactionFileUpdateParser.add_argument('WorkingDirFilepath', type=str, default=None)
customTransactionFileUpdateParser.add_argument('Payload_Request', type=str, default=None)
customTransactionFileUpdateParser.add_argument('Payload_Response', type=str, default=None)
customTransactionFileUpdateParser.add_argument('InstanceName')
customTransactionFileUpdateParser.add_argument('DomainType')
customTransactionFileUpdateParser.add_argument('Filepath')
customTransactionFileUpdateParser.add_argument('Filename')
customTransactionFileUpdateParser.add_argument('FileStatus')
customTransactionFileUpdateParser.add_argument('departmentName')
customTransactionFileUpdateParser.add_argument('userName')
customTransactionFileUpdateParser.add_argument('wdfilepath_exists')
customTransactionFileUpdateParser.add_argument('ui_flag', default=0)
customTransactionFileUpdateParser.add_argument('no_pages', default=None)
customTransactionFileUpdateParser.add_argument('OutputCSVPath', default=None)


customTransactionMaxParser = reqparse.RequestParser()
customTransactionMaxParser.add_argument('auth_token', required=True)


