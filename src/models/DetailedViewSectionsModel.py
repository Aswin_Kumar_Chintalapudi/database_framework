db =None
from sqlalchemy_serializer import SerializerMixin

class DetailedViewSectionsModel(db.Model, SerializerMixin):
    __tablename__ = 'detailedviewsections'
    Id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    comment = db.Column(db.String(255), default=None)
    department_Name = db.Column(db.String(255), default=None)
    section_Name = db.Column(db.String(255), default=None)
    section_Value = db.Column(db.String(255), default=None)
    tId = db.Column(db.Integer, default=None)
    user_Name = db.Column(db.String(255), default=None)