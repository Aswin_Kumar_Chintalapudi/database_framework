db =None
from sqlalchemy_serializer import SerializerMixin

class UserModel(db.Model, SerializerMixin):
    __tablename__ = 'users'
    user_id = db.Column(db.String(255), primary_key=True)
    password = db.Column(db.LargeBinary, default=None)
    create_date = db.Column(db.DateTime, default=dt.utcnow)
    updated_date = db.Column(db.DateTime, default=dt.utcnow, onupdate=dt.utcnow)
    WelcomeFlag = db.Column(db.Integer, default=0)
    departmentName = db.Column(db.String(255), default=None)
    role = db.Column(db.String(255), default=None)
    emailId = db.Column(db.String(255), default=None)
    authToken = db.Column(db.LargeBinary, default=None)
