db =None
from sqlalchemy_serializer import SerializerMixin

class DomainModel(db.Model, SerializerMixin):
    __tablename__ = 'domain'
    domainName = db.Column(db.String(500), primary_key=True)
    domainDescription = db.Column(db.Text, default=None)
    isDeletable = db.Column(db.Integer, default=0)
    createdBy = db.Column(db.String(255))
    departmentName = db.Column(db.String(200), default=None)