db =None
from sqlalchemy_serializer import SerializerMixin


class CustomDomainInstance(db.Model, SerializerMixin):
    """
        A class to represent a database model for the custom domain instance table.

        ...

        Attributes
        ----------
        id : int
            unique identifier to get the custom domain instance
        domain : str
            name of the domain
        instance : str
            name of the instance
        keywords : str
            all keywords associated with that domain
        sections : str
            all sections to identify
        synonyms : str
            all synonyms to search for
        """
    __tablename__ = 'customdomainforinstance'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    domain = db.Column(db.String(30))
    instance = db.Column(db.String(30))
    keywords = db.Column(db.String(200))
    sections = db.Column(db.String(200))
    synonyms = db.Column(db.String(200))