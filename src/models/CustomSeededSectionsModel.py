
db =None
from sqlalchemy_serializer import SerializerMixin



class CustomSeededsections(db.Model, SerializerMixin):
    """
    A class to represent the database model customseededsections.

    ...

    Attributes
    ----------
    id : int
        unique identifier to get the custom seeded sections
    domain : str
        name of the domain
    instance : str
        name of the instance
    keywords : str
        all keywords associated with that section
    sections : str
        all sections to identify
    synonyms : str
        all synonyms associated with the custom sections

    """
    __tablename__ = 'customseededsections'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    domain = db.Column(db.String(30))
    instance = db.Column(db.String(30))
    keywords = db.Column(db.String(200), default=None)
    sections = db.Column(db.String(200), default=None)
    synonyms = db.Column(db.String(200), default=None)