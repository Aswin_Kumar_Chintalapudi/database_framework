db =None
from sqlalchemy_serializer import SerializerMixin

class DepartmentModel(db.Model, SerializerMixin):
    __tablename__ = 'department'
    departmentName = db.Column(db.String(255), primary_key=True)
    departmentDescription = db.Column(db.String(255), default=None)
    createdBy = db.Column(db.String(255), default=None)
    createDate = db.Column(db.DateTime, default=dt.utcnow, onupdate=dt.utcnow)