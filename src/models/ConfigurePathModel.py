
from sqlalchemy_serializer import SerializerMixin





class ConfigurePath(db.Model, SerializerMixin):
    """
       A class to represent a database model for configuring paths.

       ...

       Attributes
       ----------
       Id : int
           unique identifier to configure the file paths
       Input : str
           path to the input folder
       Working : str
           path to the working folder
       Output : str
           path to the output folder
       License_Key : str
           key for the licence authorisation
       License_Flag : str
       Archive : str
           path for the archive folder
       License_Desc : str
           description about the license
       TestFile : str
           path to test file
       tempInput : str
           for temporary input
       """

    __tablename__ = 'configure_path'
    Id = db.Column(db.Integer, primary_key=True, autoincrement=True, default=1)
    Input = db.Column(db.String(30))
    Working = db.Column(db.String(30))
    Output = db.Column(db.String(200))
    License_Key = db.Column(db.String(200))
    License_Flag = db.Column(db.String(200))
    Archive = db.Column(db.String(200))
    License_Desc = db.Column(db.String(200))
    TestFile = db.Column(db.String(200))
    tempInput = db.Column(db.String(200))