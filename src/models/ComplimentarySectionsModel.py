db =None
from sqlalchemy_serializer import SerializerMixin


class ComplimentarySections(db.Model, SerializerMixin):
    """
       A class to represent the database table complimentorysections

       ...

       Attributes
       ----------
       sections : str
           name of the section
       keywords : str
           all keywords for a section
       synonyms : str
           all synonyms associated with a section

       """
    __tablename__ = "complimentorysections"
    sections = db.Column(db.String(30), primary_key=True)
    keywords = db.Column(db.String(500))
    synonyms = db.Column(db.String(500))