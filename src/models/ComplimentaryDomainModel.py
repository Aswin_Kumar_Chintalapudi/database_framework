from sqlalchemy_serializer import SerializerMixin


class ComplimentaryDomain(db.Model, SerializerMixin):
    """
       A class to represent the database table complimentarydomain.

       ...

       Attributes
       ----------
       domain : str
           name of the domain
       keywords : str
           all keywords
       sections : str
           all sections to identify
       synonyms : str
           all synonyms associated with the sections

       """
    __tablename__ = 'complimentorydomain'
    Id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    domain = db.Column(db.String(30))
    keywords = db.Column(db.String(500))
    sections = db.Column(db.String(30))
    synonyms = db.Column(db.String(500))