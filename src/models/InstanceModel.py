db =None
from sqlalchemy_serializer import SerializerMixin


class InstanceModel(db.Model, SerializerMixin):
    __tablename__ = 'instance'
    Instance_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    create_date = db.Column(db.DateTime, default=dt.utcnow)
    created_by = db.Column(db.String(255), default=None)
    instance_description = db.Column(db.String(200), default=None)
    instance_domain = db.Column(db.String(200), default=None)
    instance_name = db.Column(db.String(200), default=None)
    updated_by = db.Column(db.String(200), default=None)
    updated_date = db.Column(db.DateTime, default=dt.utcnow, onupdate=dt.utcnow)
    departmentName = db.Column(db.String(200), default=None)