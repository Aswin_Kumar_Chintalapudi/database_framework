db =None
from sqlalchemy_serializer import SerializerMixin


class Transaction(db.Model, SerializerMixin):
    """
       A class to represent a transaction.
       It is used to create a db model for the transaction table.

       ...

       Attributes
       ----------
       ID : Big int
           unique id to identify the transaction
       no_pages : int
           number of pages
       InstanceName : str
           name of the instance
       DomainType : str
           type of the domain
       Filepath : str
           path to the file
       WorkingDirFilepath : str
           path to the working directory
       Filename : str
           name of the file
       Timestamp : datetime
           time of the transaction
       Filestatus : str
           status of the file
       Error_Description : str
           shows the error description
       OutputCSVPath : str
           shows the path to the output csv folder
       Payload_Request : str
           contains the payload request
       Payload_Response : str
           contains the payload response
       dateOfContract : datetime
           date of the contract
       departmentName : str
           department name that is giving the request
       partyOne : str
       partyTwo : str
       userName : str
           shows the user's name
       ui_flag : int
       """
    __tablename__ = 'transaction'
    ID = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    no_pages = db.Column(db.Integer)
    InstanceName = db.Column(db.String(30))
    DomainType = db.Column(db.String(30))
    Filepath = db.Column(db.String(200))
    WorkingDirFilepath = db.Column(db.String(200))
    Filename = db.Column(db.String(200))
    Timestamp = db.Column(db.DateTime, default=dt.utcnow, onupdate=dt.utcnow)
    Filestatus = db.Column(db.String(50))
    Error_Description = db.Column(db.String(200))
    OutputCSVPath = db.Column(db.String(200))
    Payload_Request = db.Column(db.Text)
    Payload_Response = db.Column(db.Text)
    dateOfContract = db.Column(db.DateTime, default=dt.utcnow, onupdate=dt.utcnow)
    departmentName = db.Column(db.String(200))
    partyOne = db.Column(db.String(400))
    partyTwo = db.Column(db.String(400))
    userName = db.Column(db.String(200))
    ui_flag = db.Column(db.Integer, default=0)