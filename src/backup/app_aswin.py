from typing import Pattern
from flask.signals import request_finished
from flask.wrappers import Response
from sqlalchemy.sql.expression import true
import werkzeug
from datetime import datetime as dt
from werkzeug.utils import cached_property
werkzeug.cached_property = cached_property
from flask import Flask, json,make_response
from flask_restplus import Api, Resource
from flask_restplus import reqparse
from sqlalchemy_serializer import SerializerMixin
from flask_sqlalchemy import SQLAlchemy
from flask_restplus import abort
from sqlalchemy import func,exc
import configparser
from flask import request
config_path = r"C:\BUMI"

config = configparser.ConfigParser()
config.read(f"{config_path}\\db_config.ini")
print(config.sections())
MYSQL_HOST = config["MySQL"]["host"]
MYSQL_DATABASE = config["MySQL"]["database"]
MYSQL_USERNAME = config["MySQL"]["user"]
MYSQL_PASSWORD = config["MySQL"]["password"]

flask_app = Flask(__name__)
host, database, username, password = MYSQL_HOST,MYSQL_DATABASE,MYSQL_USERNAME,MYSQL_PASSWORD

uri = "mysql://" + username + ":" + password + "@" + host + "/" + database
flask_app.config['SQLALCHEMY_DATABASE_URI'] = uri
flask_app.config['SQLALCHEMY_ECHO'] = False
flask_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
flask_app.secret_key = "secret_super_key"
# flask_app.config['SQLALCHEMY_ENGINE_OPTIONS'] = {'poolclass': NullPool}

flask_app.config['SQLALCHEMY_POOL_RECYCLE'] = 180
flask_app.config['SQLALCHEMY_POOL_SIZE'] = 0
# flask_app.config['SQLALCHEMY_POOL_TIMEOUT'] = None
flask_app.config['SQLALCHEMY_MAX_OVERFLOW'] = -1
db = SQLAlchemy(flask_app)
app = Api(app=flask_app)
name_space = app.namespace('bumiapi/', description='Bumi APIs')

class Transaction(db.Model,SerializerMixin):
    __tablename__ = 'transaction'
    ID = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    no_pages = db.Column(db.Integer)
    InstanceName = db.Column(db.String(30))
    DomainType = db.Column(db.String(30))
    Filepath = db.Column(db.String(200))
    WorkingDirFilepath = db.Column(db.String(200))
    Filename = db.Column(db.String(200))
    Timestamp = db.Column(db.DateTime, default=dt.utcnow, onupdate=dt.utcnow)
    Filestatus = db.Column(db.String(50))
    Error_Description = db.Column(db.String(200))
    OutputCSVPath = db.Column(db.String(200))
    Payload_Request = db.Column(db.Text)
    Payload_Response = db.Column(db.Text)
    dateOfContract = db.Column(db.DateTime, default=dt.utcnow, onupdate=dt.utcnow)
    departmentName = db.Column(db.String(200))
    partyOne = db.Column(db.String(400))
    partyTwo = db.Column(db.String(400))
    userName = db.Column(db.String(200))
    ui_flag = db.Column(db.Integer,default=0)
    
class Customseededsections(db.Model,SerializerMixin):
    __tablename__ = 'customseededsections'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True, default=1)
    domain = db.Column(db.String(30), index=True)
    instance = db.Column(db.String(30), index=True)
    keywords = db.Column(db.String(200), index=True)
    sections = db.Column(db.String(200), index=True)
    synonyms = db.Column(db.String(200), index=True)

class CustomDomainInstance(db.Model,SerializerMixin):
    __tablename__ = 'customdomainforinstance'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True, default=1)
    domain = db.Column(db.String(30), index=True)
    instance = db.Column(db.String(30), index=True)
    keywords = db.Column(db.String(200), index=True)
    sections = db.Column(db.String(200), index=True)
    synonyms = db.Column(db.String(200), index=True)
    
    


       
class ConfigurePath(db.Model,SerializerMixin):
    __tablename__ = 'configure_path'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True, default=1)
    Input = db.Column(db.String(30), index=True)
    Working = db.Column(db.String(30), index=True)
    Output = db.Column(db.String(200), index=True)
    License_Key = db.Column(db.String(200), index=True)
    License_Flag = db.Column(db.String(200), index=True)
    Archive = db.Column(db.String(200), index=True)
    License_Desc = db.Column(db.String(200), index=True)
    TestFile = db.Column(db.String(200), index=True)
    tempInput = db.Column(db.String(200), index=True)


    
class DomainModel(db.Model,SerializerMixin):
    __tablename__ = 'domain'
    domainName = db.Column(db.String(500), primary_key=True)
    domainDescription  = db.Column(db.Text, default = None)
    isDeletable  = db.Column(db.Integer, default=0)
    createdBy = db.Column(db.String(255))
    departmentName = db.Column(db.String(200), default=None)  


domainCreateParser = reqparse.RequestParser()
domainCreateParser.add_argument('domainName',required=True)
domainCreateParser.add_argument('domainDescription',default=None)
domainCreateParser.add_argument('isDeletable',default=0)
domainCreateParser.add_argument('createdBy',required=True)
domainCreateParser.add_argument('departmentName',default=None)

domainUpdateParser = reqparse.RequestParser()
domainUpdateParser.add_argument('domainName',default=None)
domainUpdateParser.add_argument('domainDescription',default=None)
domainUpdateParser.add_argument('isDeletable',default=None)
domainUpdateParser.add_argument('createdBy',default=None)
domainUpdateParser.add_argument('departmentName',default=None)



@name_space.route("/v1/domain")
class MainClass(Resource):
    def get(self):
        all_data = DomainModel.query.all()
        response_list = []
        for data in all_data:
            response_list.append(data.to_dict())
        response = create_success_response(response_list)
        return make_response(response,200)
    @name_space.expect(domainCreateParser)
    def post(self):
        args = domainCreateParser.parse_args()
        domain_name,description,isdeletable,createdBy,departmentName = args["domainName"],args["domainDescription"],args["isDeletable"],args["createdBy"],args["departmentName"]
        new_domain = DomainModel()
        new_domain.domainName = domain_name
        new_domain.domainDescription = description
        new_domain.isDeletable = isdeletable
        new_domain.createdBy = createdBy
        new_domain.departmentName = departmentName
        db.session.add(new_domain)
        db.session.commit()
        db.session.close()
        return make_response(create_success_response(None,f'New Domain {domain_name} is created'),200)
    
@name_space.route("/v1/domain/<inputDomain>")
class MainClass(Resource):
    def get(self,inputDomain):
        search_domain = inputDomain
        db_data = DomainModel.query.filter(DomainModel.domainName == search_domain).first()
        if not db_data:
            response = create_error_response(None,"Domain Name is not found",404)
            return make_response(response,404)
        response = create_success_response(db_data.to_dict())
        return make_response(response,200)
            
    def delete(self,inputDomain):
        delete_domain = inputDomain
        print("DELETE Domain : ",delete_domain)
        data = DomainModel.query.filter(DomainModel.domainName == delete_domain).delete()
        db.session.commit()
        db.session.close()
        response = create_success_response(None,"Domain is deleted")
        return make_response(response,200)
       
    @name_space.expect(domainUpdateParser)
    def put(self,inputDomain):
        args = domainUpdateParser.parse_args()
        domain_name,description,isdeletable,createdBy,departmentName = args["domainName"],args["domainDescription"],args["isDeletable"],args["createdBy"],args["departmentName"]
        update_domain = DomainModel.query.filter(DomainModel.domainName == inputDomain).first()
        if not update_domain:
            response = create_error_response(None,"Domain Name is not found",404)
            return make_response(response,404)
        if domain_name:
            update_domain.domainName = domain_name
        if  description:
            update_domain.domainDescription = description
        if  isdeletable:
            update_domain.isDeletable = isdeletable
        if  createdBy:
            update_domain.createdBy = createdBy
        if  departmentName:
            update_domain.departmentName = departmentName
        db.session.add(update_domain)
        db.session.commit()
        db.session.close()
        return make_response(create_success_response(None,"Domain is Updated"),200)
        
        
        

customSeededSectionParser = reqparse.RequestParser()
customSeededSectionParser.add_argument('domain', required=True)
customSeededSectionParser.add_argument('instance', required=True)

@name_space.route("/v1/customseededsections")
class MainClass(Resource):
    @name_space.expect(customSeededSectionParser)
    def get(self):
        args = customSeededSectionParser.parse_args()
        domain = args["domain"]
        instance = args["instance"]
        try:
            transaction = Customseededsections.query.filter(Customseededsections.domain == domain).filter(Customseededsections.instance == instance).one()
            result = transaction.to_dict()
            response = create_success_response(result)
            return make_response(response,200)
        except:
            return make_response("Data is not found",404)
        
customDomainForInstanceParser = reqparse.RequestParser()
customDomainForInstanceParser.add_argument('domain', required=True)
customDomainForInstanceParser.add_argument('instance', required=True)

@name_space.route("/v1/customdomainforinstance")
class MainClass(Resource):
    @name_space.expect(customDomainForInstanceParser)
    def get(self):
        args = customDomainForInstanceParser.parse_args()
        domain = args["domain"]
        instance = args["instance"]
        try:
            transaction = CustomDomainInstance.query.filter(CustomDomainInstance.domain == domain).filter(CustomDomainInstance.instance == instance).one()
            result = transaction.to_dict()
            response = create_success_response(result)
            return make_response(response,200)
        except:
            return make_response("Data is not found",404)

customConfigrePathParser = reqparse.RequestParser()
customConfigrePathParser.add_argument('Input')
customConfigrePathParser.add_argument('Working')
customConfigrePathParser.add_argument('Output')
customConfigrePathParser.add_argument('Archive')


@name_space.route("/v1/configure_path")
class MainClass(Resource):
    def get(self):
        result = dict()
        # try:
        transaction = ConfigurePath.query.limit(1).all()
        if isinstance(transaction,list):
            all_result = transaction[0].to_dict()
            result["Input"] = all_result["Input"]
            result["Working"] = all_result["Working"]
            result["Output"] = all_result["Output"]
            result["Archive"] = all_result["Archive"]

        else:
            result = transaction.to_dict()
        response = create_success_response(result)
        return make_response(response,200)

#Generates generic response in case of a successful API call
def create_success_response(result,message=None):
    response_data = dict()
    response_data["status"] = "success"
    response_data["code"] = 200
    response_data["message"] = message
    response_data["data"] = result #array object
    response_json = json.jsonify(response_data)
    return response_json
def create_error_response(result,message=None,code=500):
    response_data = dict()
    response_data["status"] = "error"
    response_data["code"] = code
    response_data["message"] = message
    response_data["data"] = None
    response_json = json.jsonify(response_data)
    return response_json

customInstanceParser = reqparse.RequestParser()
customInstanceParser.add_argument('instance_name', required=True)
customInstanceParser.add_argument('instance_domain', required=True)



customTransactionParser = reqparse.RequestParser()
customTransactionParser.add_argument('ID')
customTransactionParser.add_argument('InstanceName', required=True)
customTransactionParser.add_argument('DomainType', required=True)
customTransactionParser.add_argument('Filepath', required=True)
customTransactionParser.add_argument('Filename', required=True)
customTransactionParser.add_argument('FileStatus', required=True)
customTransactionParser.add_argument('departmentName', required=True)
customTransactionParser.add_argument('userName', required=True)
customTransactionParser.add_argument('wdfilepath_exists')
customTransactionParser.add_argument('ui_flag', default=0)
customTransactionParser.add_argument('no_pages',default=None)
customTransactionParser.add_argument('working_file_path',default=None)
customTransactionParser.add_argument('out_csv_file_path',default=None)
customTransactionParser.add_argument('error_description',default=None)
customTransactionParser.add_argument('payload_request',default=None)
customTransactionParser.add_argument('payload_response',default=None)

#insert data into transaction table
@name_space.route("/v1/transaction")
class MainClass(Resource):
    @name_space.expect(customTransactionParser)
    def post(self):
        args = customTransactionParser.parse_args()
        transaction = Transaction()
        result = dict()
        instance, domain, filepath, filename, filestatus, uiFlag, department, username = args['InstanceName'], args[
            'DomainType'], args['Filepath'], args['Filename'], args['FileStatus'], args['ui_flag'], args['departmentName'], args['userName']

        #common instance , domaintype, department, username

        no_pages,  wdfilepath, outcsvfilepath,error_description,payload_request,payload_response = args['no_pages'], args[
            'working_file_path'], args['out_csv_file_path'], args['error_description'], args['payload_request'], args['payload_response']

        transaction.InstanceName = instance
        transaction.DomainType = domain
        transaction.Filepath = filepath
        transaction.Filename = filename
        transaction.FileStatus = filestatus
        transaction.ui_flag = uiFlag
        transaction.departmentName = department
        transaction.userName = username

        transaction.no_pages = no_pages
        transaction.InstanceName = instance
        transaction.DomainType = domain
        transaction.Filepath = filepath
        transaction.WorkingDirFilepath = wdfilepath
        transaction.OutputCSVPath = outcsvfilepath
        transaction.Filename = filename
        transaction.Filestatus = filestatus
        transaction.Error_Description = error_description
        transaction.Payload_Request = payload_request
        transaction.Payload_Response = payload_response
        transaction.departmentName = department
        transaction.userName = username

        db.session.add(transaction)
        db.session.flush()
        new_transanction_id = transaction.ID
        db.session.commit()
        db.session.close()
        result["transaction_id"] =  new_transanction_id
        response = create_success_response(result)
        return make_response(response, 200)

updateIDParser = reqparse.RequestParser()
updateIDParser.add_argument('ID', type=int, required=True)
updateIDParser.add_argument('error_desc', type=str, default=None)
updateIDParser.add_argument('working_file_path', type=str, default=None)
updateIDParser.add_argument('payload_request', type=str, default=None)

@name_space.route("/v1/transaction")
class MainClass(Resource):
    @name_space.expect(updateIDParser)
    def put(self):
        result = dict()
        args = updateIDParser.parse_args()
        id = args['ID']
        error_desc = args["error_desc"]
        working_file_path = args["working_file_path"]
        payload_request = args["payload_request"]
        transaction = Transaction.query.filter(Transaction.ID == id).one()
        if error_desc:
            transaction.Error_Description = error_desc
        if working_file_path:
            transaction.WorkingDirFilepath = working_file_path
        if payload_request:
            transaction.Payload_Request = payload_request
        db.session.add(transaction)
        db.session.commit()
        db.session.close()
        result["update message"] = "Transaction table is updated"
        response = create_success_response(result)
        return make_response(response, 200)


# getMaxIDParser = reqparse.RequestParser()
# getMaxIDParser.add_argument('ID', type=int, required=True)

@name_space.route("/v1/transaction/get_max_transaction_id")
class MainClass(Resource):
    @name_space.expect()
    def get(self):
        result = dict()
        result["max_transaction_id"] = db.session.query(func.max(Transaction.ID)).scalar()
        response = create_success_response(result)
        return make_response(response, 200)

customTransactionFileUpdateParser = reqparse.RequestParser()
customTransactionFileUpdateParser.add_argument('ID',required=True)
customTransactionFileUpdateParser.add_argument('InstanceName')
customTransactionFileUpdateParser.add_argument('DomainType')
customTransactionFileUpdateParser.add_argument('Filepath')
customTransactionFileUpdateParser.add_argument('Filename')
customTransactionFileUpdateParser.add_argument('FileStatus')
customTransactionFileUpdateParser.add_argument('departmentName')
customTransactionFileUpdateParser.add_argument('userName')
customTransactionFileUpdateParser.add_argument('wdfilepath_exists')
customTransactionFileUpdateParser.add_argument('ui_flag', default=0)
customTransactionFileUpdateParser.add_argument('no_pages',default=None)
customTransactionFileUpdateParser.add_argument('working_file_path',default=None)
customTransactionFileUpdateParser.add_argument('out_csv_file_path',default=None)
customTransactionFileUpdateParser.add_argument('error_desc',default=None)
customTransactionFileUpdateParser.add_argument('payload_request',default=None)
customTransactionFileUpdateParser.add_argument('payload_response',default=None)

@name_space.route("/v1/transaction/file_status")
class MainClass(Resource):
    def put(self):
        result = dict()
        args = customTransactionFileUpdateParser.parse_args()
        transaction_id,instance, domain, filepath, filename, filestatus, uiFlag, department, username,wdfilepath_exists = args['ID'],args['InstanceName'], args[
            'DomainType'], args['Filepath'], args['Filename'], args['FileStatus'], args['ui_flag'], args[
                                                                                             'departmentName'], args[
                                                                                             'userName'], args['wdfilepath_exists']
        wdfilepath, payload_response,error_desc =  args['working_file_path'],args[ 'payload_response'],args['error_desc']
        transaction = Transaction.query.get(transaction_id)
        # ISSUE
        print("transaction:::",transaction)
        print("error desc from req",error_desc, filestatus)
        if transaction is not None:
            transaction.InstanceName = instance
            transaction.departmentName = department
            transaction.userName = username
            transaction.DomainType = domain
            transaction.Filepath = filepath
            transaction.WorkingDirFilepath = wdfilepath
            transaction.Filename = filename
            if not filestatus:
                transaction.Filestatus = 'Validated'
            else:
                transaction.Filestatus = filestatus
            print("transaction.Filestatus = filestatus",transaction.Filestatus )
            if not error_desc:
                transaction.Error_Description = 'No error'
            else:
                transaction.Error_Description = error_desc
            print("transaction.Error_Description", transaction.Error_Description)
            transaction.Payload_Response = payload_response
            if not wdfilepath_exists:
                transaction.Error_Description = 'Working directoryy file not exist'
                transaction.Filestatus = 'Error'
            db.session.add(transaction)
            db.session.commit()
            db.session.close()
        result["update message"] = "File status for given transaction Id is updated"
        response = create_success_response(result)
        return make_response(response, 200)

@name_space.route("/v1/transaction")
class MainClass(Resource):
    def get(self):
        tran_list = list()
        fields = ['ID', 'InstanceName']
        transaction = Transaction.query.with_entities(Transaction.ID, Transaction.Timestamp, Transaction.DomainType,
                                                      Transaction.InstanceName, Transaction.no_pages, Transaction.Filestatus,
                                                      Transaction.Error_Description).all()
        for tran in transaction:
            #tran_list.append(tran.to_dict())
            tran_list.append({"ID":tran[0],"Timestamp": tran[1],"DomainType":tran[2],"InstanceName": tran[3],
                              "no_pages":tran[4],"Filestatus": tran[5],"Error_Description":tran[6]})
        print("result",tran_list)
        #tran_json = jsonify(tran_list)
        response = create_success_response(tran_list)
        return make_response(response, 200)
    
##Developer -- aswin

#Generates generic response in case of a successful API call
def create_success_response(result,message=None):
    response_data = dict()
    response_data["status"] = "success"
    response_data["code"] = 200
    response_data["message"] = message
    response_data["data"] = result
    response_json = json.jsonify(response_data)
    return response_json
def create_error_response(result,message=None,code=500):
    response_data = dict()
    response_data["status"] = "error"
    response_data["code"] = code
    response_data["message"] = message
    response_data["data"] = None
    response_json = json.jsonify(response_data)
    return response_json

class DepartmentModel(db.Model,SerializerMixin):
    __tablename__ = 'department'
    departmentName = db.Column(db.String(255), primary_key=True)
    departmentDescription  = db.Column(db.String(255), default=None)
    createdBy  = db.Column(db.String(255), default=None)
    createDate = db.Column(db.DateTime, default=dt.utcnow, onupdate=dt.utcnow)

departmentCreateParser = reqparse.RequestParser()
departmentCreateParser.add_argument('departmentName',required=True)
departmentCreateParser.add_argument('departmentDescription',default=None)
departmentCreateParser.add_argument('createdBy',required=True)

departmentUpdateParser = reqparse.RequestParser()
departmentUpdateParser.add_argument('departmentName',required=True)
departmentUpdateParser.add_argument('departmentDescription',default=None)
departmentUpdateParser.add_argument('createdBy',default=None)

departmentDetailsParser = reqparse.RequestParser()
departmentDetailsParser.add_argument('departmentName',required=True)


@name_space.route("/v1/department")
class MainClass(Resource):
    #get all depatments data
    def get(self):
        all_data = DepartmentModel.query.all()
        data_list = []
        for data in all_data:
            data_list.append(data.to_dict())
        response = create_success_response(data_list)
        return make_response(response,200)
    
    def delete(self):
        try:
            num_rows_deleted = db.session.query(DepartmentModel).delete()
            db.session.commit()
        except:
            db.session.rollback()
        response = create_success_response(None,"All the departments are removed")
        return make_response(response,200)
    
    @name_space.expect(departmentCreateParser)
    #insert new department
    def post(self):
        args = departmentCreateParser.parse_args()
        
        departmentName,departmentDescription,createdBy = args["departmentName"],args["departmentDescription"],args["createdBy"]
        new_department = DepartmentModel()
        new_department.departmentName = departmentName
        new_department.departmentDescription = departmentDescription
        new_department.createdBy = createdBy
        db.session.add(new_department)
        db.session.commit()
        db.session.close()
        return make_response(f'New Department {departmentName} is created',200)
    
    
@name_space.route("/v1/department/<deptName>")
class MainClass(Resource):
    #extract dept details
    def get(self,deptName):
        search_dept = deptName
        data_dict = DepartmentModel.query.filter(DepartmentModel.departmentName == search_dept).first()
        if not data_dict:
            response =  create_error_response(None,"Depatment data not available",404)
            return make_response(response,404)
        response_dict = data_dict.to_dict()
        response = create_success_response(data_dict)
        return make_response(response,200)
    #delete department
    def delete(self,deptName):
        search_dept = deptName
        data_dict = DepartmentModel.query.filter(DepartmentModel.departmentName == search_dept).delete()
        db.session.commit()
        db.session.close()
        response = create_success_response(None,"Department deleted successfully")
        return make_response(response,200)
    #update department
    @name_space.expect(departmentUpdateParser)
    def put(self,deptName):
        args = departmentUpdateParser.parse_args()
        departmentName,departmentDescription,createdBy = args["departmentName"],args["departmentDescription"],args["createdBy"]
        update_department = DepartmentModel.query.filter(DepartmentModel.departmentName == deptName).first()
        if not update_department:
            response =  create_error_response(None,"Department data not available",404)
            return make_response(response,404)
        
        if  departmentName:
            update_department.departmentName = departmentName
        if  departmentDescription:
            update_department.departmentDescription = departmentDescription
        if  createdBy:
            update_department.createdBy = createdBy
        db.session.add(update_department)
        db.session.commit()
        db.session.close()
        return make_response(f'department {departmentName} is updated',200)

class InstanceModel(db.Model,SerializerMixin):
    __tablename__ = 'instance'
    Instance_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    create_date  = db.Column(db.DateTime, default=dt.utcnow)
    created_by  = db.Column(db.String(255),default=None)
    instance_description = db.Column(db.String(200),default=None)
    instance_domain = db.Column(db.String(200),default=None)
    instance_name = db.Column(db.String(200),default=None)
    updated_by = db.Column(db.String(200),default=None)
    updated_date  = db.Column(db.DateTime, default=dt.utcnow, onupdate=dt.utcnow)
    departmentName = db.Column(db.String(200),default=None)


@name_space.route("/v1/instance/get_instance_count")
class MainClass(Resource):
    @name_space.expect(customInstanceParser)
    def get(self):
        count = dict()
        args = customInstanceParser.parse_args()
        instance = args["instance_name"]
        domain = args["instance_domain"]
        count["count"] = InstanceModel.query.filter(InstanceModel.instance_name == instance).filter(
                                                    InstanceModel.instance_domain == domain).count()
        response = create_success_response(count)
        return make_response(response,200)
    
@name_space.route("/v1/instance/filter/instance/domain")
class MainClass(Resource):
    @name_space.expect(customInstanceParser)
    def get(self):
        args = customInstanceParser.parse_args()
        instance = args["instance_name"]
        domain = args["instance_domain"]
        db_data = InstanceModel.query.filter(InstanceModel.instance_name == instance).filter(
                                                    InstanceModel.instance_domain == domain).all()
        all_data = []
        for data in db_data:
            all_data.append(data.to_dict())
        if not all_data:
            response = create_error_response(None,"No data Found",404)
            return make_response(response,404)
        response = create_success_response(all_data,None)
        return make_response(response,200)

filterByInstanceParser = reqparse.RequestParser()
filterByInstanceParser.add_argument('instance_name',required=True)

@name_space.route("/v1/instance/filter/instance")
class MainClass(Resource):
    @name_space.expect(filterByInstanceParser)
    def get(self):
        args = filterByInstanceParser.parse_args()
        instance = args["instance_name"]
        db_data = InstanceModel.query.filter(InstanceModel.instance_name == instance).all()
        all_data = []
        for data in db_data:
            all_data.append(data.to_dict())
        if not all_data:
            response = create_error_response(None,"No data Found",404)
            return make_response(response,404)
        response = create_success_response(all_data,None)
        return make_response(response,200)
     
instanceCreateParser = reqparse.RequestParser()
instanceCreateParser.add_argument('instance_name',required=True)
instanceCreateParser.add_argument('instance_description',default=None)
instanceCreateParser.add_argument('created_by',default=None)
instanceCreateParser.add_argument('instance_domain',default=None)
instanceCreateParser.add_argument('updated_by',default=None)
instanceCreateParser.add_argument('departmentName',default=None)

instanceUpdateParser = reqparse.RequestParser()
instanceUpdateParser.add_argument('instance_name',default=None)
instanceUpdateParser.add_argument('instance_description',default=None)
instanceUpdateParser.add_argument('created_by',default=None)
instanceUpdateParser.add_argument('instance_domain',default=None)
instanceUpdateParser.add_argument('updated_by',default=None)
instanceUpdateParser.add_argument('departmentName',default=None)

instanceDetailsParser = reqparse.RequestParser()
instanceDetailsParser.add_argument('Instance_id',required=True)



@name_space.route("/v1/instance")
class MainClass(Resource):
    #get all instance data
    def get(self):
        all_data = InstanceModel.query.all()
        data_list = []
        for data in all_data:
            data_list.append(data.to_dict())
        response = create_success_response(data_list)
        return make_response(response,200)
    
    @name_space.expect(instanceCreateParser)
    #insert new instance
    def post(self):
        args = instanceCreateParser.parse_args()
        
        instance_name,instance_description,created_by,instance_domain,updated_by,departmentName = args["instance_name"],args["instance_description"],args["created_by"],args["instance_domain"],args["updated_by"],args["departmentName"]
        new_instance = InstanceModel()
        new_instance.instance_name = instance_name
        new_instance.instance_description = instance_description
        new_instance.created_by = created_by
        new_instance.instance_domain = instance_domain
        new_instance.updated_by = updated_by
        new_instance.departmentName = departmentName
        db.session.add(new_instance)
        db.session.commit()
        db.session.close()
        return make_response(create_success_response(None,f'New Instance {instance_name} is created'),200)


@name_space.route("/v1/instance/<check_instance_id>")
class MainClass(Resource):
    #extract instance details
    def get(self,check_instance_id):
        search_instance = int(check_instance_id)
        data_dict = InstanceModel.query.filter(InstanceModel.Instance_id == search_instance).first()
        if not data_dict:
            response =  create_error_response(None,"Instance data not available",404)
            return make_response(response,404)
        response_dict = data_dict.to_dict()
        response = create_success_response(response_dict)
        return make_response(response,200)
    #delete instance
    def delete(self,check_instance_id):
        search_instance = int(check_instance_id)
        data_dict = InstanceModel.query.filter(InstanceModel.Instance_id == search_instance).delete()
        db.session.commit()
        db.session.close()
        response = create_success_response(None,"Instance deleted successfully")
        return make_response(response,200)
    #update instance
    @name_space.expect(instanceUpdateParser)
    def put(self,check_instance_id):
        search_instance = int(check_instance_id)
        args = instanceUpdateParser.parse_args()
        instance_name,instance_description,created_by,instance_domain,updated_by,departmentName = args["instance_name"],args["instance_description"],args["created_by"],args["instance_domain"],args["updated_by"],args["departmentName"]
        update_instance = InstanceModel.query.filter(InstanceModel.Instance_id == search_instance).first()
        if not update_instance:
            response =  create_error_response(None,"Instance data not available",404)
            return make_response(response,404)
        
        if  instance_name:
            update_instance.instance_name = instance_name
        if  instance_description:
            update_instance.instance_description = instance_description
        if  created_by:
            update_instance.created_by = created_by
        if  instance_domain:
            update_instance.instance_domain = instance_domain
        if  updated_by:
            update_instance.updated_by = updated_by
        if  departmentName:
            update_instance.departmentName = departmentName
            
        db.session.add(update_instance)
        db.session.commit()
        db.session.close()
        return make_response(create_success_response(None,f'Instance {instance_name} is updated'),200)

#user model


class UserModel(db.Model,SerializerMixin):
    __tablename__ = 'users'
    user_id = db.Column(db.String(255), primary_key=True)
    password  = db.Column(db.LargeBinary, default=None)
    create_date  = db.Column(db.DateTime, default=dt.utcnow)
    updated_date = db.Column(db.DateTime, default=dt.utcnow, onupdate=dt.utcnow)
    WelcomeFlag = db.Column(db.Integer, default=0)
    departmentName = db.Column(db.String(255), default=None)
    role = db.Column(db.String(255), default=None)
    emailId = db.Column(db.String(255), default=None)
    authToken = db.Column(db.LargeBinary, default=None)

userCreateParser = reqparse.RequestParser()
userCreateParser.add_argument('user_id',required=True)
userCreateParser.add_argument('password',required=True)
userCreateParser.add_argument('departmentName',required=True)
userCreateParser.add_argument('emailId',required=True)

userUpdateParser = reqparse.RequestParser()
userUpdateParser.add_argument('password',default=None)
userUpdateParser.add_argument('departmentName',default=None)
userUpdateParser.add_argument('emailId',default=None)
userUpdateParser.add_argument('WelcomeFlag',default=0)

userDetailsParser = reqparse.RequestParser()
userDetailsParser.add_argument('user_id',required=True)


def convert_blob_to_text(blob_string):
    return blob_string.decode("utf-8", errors="ignore")

def convert_text_to_blob(string):
    return bytes(string, encoding='utf8')

from functools import wraps

def authenticate(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        payload = request.get_json()
        if not payload:
            return make_response(create_error_response(None,"Session UserId not present in payload",400),400)
        if isinstance(payload,dict):
            if "session_user_id" not in payload:
                return make_response(create_error_response(None,"Session UserId not present in payload",400),400)
        else:
            return make_response(create_error_response(None,"Bad Request",400),400)
        user_id = str(payload["session_user_id"])
        user_data = UserModel.query.filter(UserModel.user_id == user_id).first()
        if user_data:
            if not user_data.authToken:
                return make_response(create_error_response(None,"Token is not Available.Please login again.",401),401)
            userToken = decrypt_message(user_data.authToken)
            if not userToken:
                return make_response(create_error_response(None,"Token is not Available.Please login again.",401),401)
            if userToken == user_id:
                return func(*args, **kwargs)
            else:
                return make_response(create_error_response(None,"Invalid Token.Please login again.",401),401)
        return make_response(create_error_response(None,"UserId not Found",401),401)
    return wrapper
    

@name_space.route("/v1/users")
class MainClass(Resource):
    method_decorators = [authenticate]
    #get all user data
    def get(self):
        all_data = UserModel.query.all()
        data_list = []
        for data in all_data:
            data.password = decrypt_message(data.password)#(data.password)
            row_dict = data.to_dict()
            # del row_dict["password"]
            data_list.append(row_dict)
        response = create_success_response(data_list)
        return make_response(response,200)
    
    # @name_space.expect(userCreateParser)
    # #insert new user
    # def post(self):
    #     args = userCreateParser.parse_args()
    #     user_id,password,departmentName,emailId = args["user_id"],args["password"],args["departmentName"],args["emailId"]
    #     new_user = UserModel()
    #     new_user.user_id = user_id
    #     new_user.password =  convert_text_to_blob(password)
    #     new_user.departmentName = departmentName
    #     new_user.emailId = emailId
    #     db.session.add(new_user)
    #     db.session.commit()
    #     db.session.close()
    #     return make_response(create_success_response(None,f'New User {user_id} is created'),200)

filterByDeptParser = reqparse.RequestParser()
filterByDeptParser.add_argument('departmentName',required=True)

@name_space.route("/v1/users/filter/department")
class MainClass(Resource):
    #extract instance details
    @name_space.expect(filterByDeptParser)
    def get(self):
        args = filterByDeptParser.parse_args()
        filter_department = args['departmentName']
        db_data = UserModel.query.filter(UserModel.departmentName == filter_department).all()
        all_data = []
        for data in db_data:
            all_data.append(data.to_dict())
        if not all_data:
            response =  create_error_response(None,"Users data not available",404)
            return make_response(response,404)
        response = create_success_response(all_data)
        return make_response(response,200)

@name_space.route("/v1/users/delete/department")
class MainClass(Resource):
    #extract instance details
    @name_space.expect(filterByDeptParser)
    def get(self):
        args = filterByDeptParser.parse_args()
        filter_department = args['departmentName']
        db_data = UserModel.query.filter(UserModel.departmentName == filter_department).delete()
        db.session.commit()
        db.session.close()
        response = create_success_response(None,"Data is deleted successfully")
        return make_response(response,200)
 
@name_space.route("/v1/users/<search_user>")
class MainClass(Resource):
    #extract instance details
    def get(self,search_user):
        data_dict = UserModel.query.filter(UserModel.user_id == search_user).first()
        if not data_dict:
            response =  create_error_response(None,"User data not available",404)
            return make_response(response,404)
        data_dict.password = decrypt_message(data_dict.password)
        response_dict = data_dict.to_dict()
        response = create_success_response(response_dict)
        return make_response(response,200)
    #delete instance
    def delete(self,search_user):
        data_dict = UserModel.query.filter(UserModel.user_id == search_user).delete()
        db.session.commit()
        db.session.close()
        response = create_success_response(None,"User deleted successfully")
        return make_response(response,200)
    
    @name_space.expect(userUpdateParser)
    def put(self,search_user):
        args = userUpdateParser.parse_args()
        password,departmentName,emailId,WelcomeFlag = args["password"],args["departmentName"],args["emailId"],args["WelcomeFlag"]
        update_user = UserModel.query.filter(UserModel.user_id == search_user).first()
        if not update_user:
            response =  create_error_response(None,"User data not available",404)
            return make_response(response,404)
        if  password:
            update_user.password = convert_text_to_blob(password)
        if  departmentName:
            update_user.departmentName = departmentName
        if  emailId:
            update_user.emailId = emailId
        if  WelcomeFlag:
            update_user.WelcomeFlag = WelcomeFlag
        db.session.add(update_user)
        db.session.commit()
        db.session.close()
        return make_response(create_success_response(None,f'User {search_user} is updated'),200)

#detailedviewsections model

class DetailedViewSectionsModel(db.Model,SerializerMixin):
    __tablename__ = 'detailedviewsections'
    Id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    comment  = db.Column(db.String(255), default=None)
    department_Name  = db.Column(db.String(255), default=None)
    section_Name = db.Column(db.String(255), default=None)
    section_Value = db.Column(db.String(255), default=None)
    tId = db.Column(db.Integer, default=None)
    user_Name = db.Column(db.String(255), default=None)

detailedViewSectionsCreateParser = reqparse.RequestParser()
detailedViewSectionsCreateParser.add_argument('comment',required=True)
detailedViewSectionsCreateParser.add_argument('department_Name',required=True)
detailedViewSectionsCreateParser.add_argument('section_Name',required=True)
detailedViewSectionsCreateParser.add_argument('section_Value',required=True)
detailedViewSectionsCreateParser.add_argument('tId',required=True)
detailedViewSectionsCreateParser.add_argument('user_Name',required=True)

detailedViewSectionsUpdateParser = reqparse.RequestParser()
detailedViewSectionsUpdateParser.add_argument('comment',required=True)
detailedViewSectionsUpdateParser.add_argument('department_Name',required=True)
detailedViewSectionsUpdateParser.add_argument('section_Name',required=True)
detailedViewSectionsUpdateParser.add_argument('section_Value',required=True)
detailedViewSectionsUpdateParser.add_argument('tId',required=True)
detailedViewSectionsUpdateParser.add_argument('user_Name',required=True)

detailedViewSectionsDetailsParser = reqparse.RequestParser()
detailedViewSectionsDetailsParser.add_argument('tId',required=True)


@name_space.route("/v1/detailedviewsections")
class MainClass(Resource):
    #get all comment data
    def get(self):
        all_data = DetailedViewSectionsModel.query.all()
        data_list = []
        for data in all_data:
            row_dict = data.to_dict()
            data_list.append(row_dict)
        response = create_success_response(data_list)
        return make_response(response,200)
    
    @name_space.expect(detailedViewSectionsCreateParser)
    #insert new comment
    def post(self):
        args = detailedViewSectionsCreateParser.parse_args()
        comment,department_Name,section_Name,section_Value,tId,user_Name = args["comment"],args["department_Name"],args["section_Name"],args["section_Value"],args["tId"],args["user_Name"]
        new_comment = DetailedViewSectionsModel() 
        new_comment.comment = comment
        new_comment.department_Name =  department_Name
        new_comment.section_Name = section_Name
        new_comment.section_Value = section_Value
        new_comment.tId = tId
        new_comment.user_Name = user_Name
        db.session.add(new_comment)
        db.session.commit()
        db.session.close()
        return make_response(create_success_response(None,f'New comment is created'),200)

@name_space.route("/v1/detailedviewsections/<comment_id>")
class MainClass(Resource):
    #extract detailedviewsections details
    def get(self,comment_id):
        data_dict = DetailedViewSectionsModel.query.filter(DetailedViewSectionsModel.Id == comment_id).first()
        if not data_dict:
            response =  create_error_response(None,"Comment data not available",404)
            return make_response(response,404)
        response_dict = data_dict.to_dict()
        response = create_success_response(response_dict)
        return make_response(response,200)
    #delete detailedviewsections
    def delete(self,comment_id):
        data_dict = DetailedViewSectionsModel.query.filter(DetailedViewSectionsModel.Id == comment_id).delete()
        db.session.commit()
        db.session.close()
        response = create_success_response(None,"Comment deleted successfully")
        return make_response(response,200)
    
    @name_space.expect(detailedViewSectionsUpdateParser)
    def put(self,comment_id):
        args = detailedViewSectionsUpdateParser.parse_args()
        comment,department_Name,section_Name,section_Value,tId,user_Name = args["comment"],args["department_Name"],args["section_Name"],args["section_Value"],args["tId"],args["user_Name"]
        update_comment = DetailedViewSectionsModel.query.filter(DetailedViewSectionsModel.Id == comment_id).first()
        if not update_comment:
            response =  create_error_response(None,"Comment data not available",404)
            return make_response(response,404)
        if  comment:
            update_comment.comment = comment
        if  department_Name:
            update_comment.department_Name = department_Name
        if  section_Name:
            update_comment.section_Name = section_Name
        if  section_Value:
            update_comment.section_Value = section_Value
        if  tId:
            update_comment.tId = tId
        if  user_Name:
            update_comment.user_Name = user_Name
        db.session.add(update_comment)
        db.session.commit()
        db.session.close()
        return make_response(create_success_response(None,f'Comment is updated'),200)


#register new user

from encryption import encrypt_message,decrypt_message,create_auth_token,decode_auth_token

   
@name_space.route("/v1/register")
class MainClass(Resource):
    @name_space.expect(userCreateParser)
    #insert new user
    def post(self):
        args = userCreateParser.parse_args()
        user_id,password,departmentName,emailId = args["user_id"],args["password"],args["departmentName"],args["emailId"]
        #check user
        new_user = UserModel()
        new_user.user_id = user_id
        new_user.password = encrypt_message(password)
        new_user.departmentName = departmentName
        new_user.emailId = emailId
        
        try:
            db.session.add(new_user)
            db.session.commit()
            db.session.close()
        except exc.IntegrityError:
            response =  create_error_response(None,"User name already exists",409)
            return make_response(response,409)
        
        user_data = {"user_id":user_id,"departmentName":departmentName,"emailId":emailId}
        return make_response(create_success_response(user_data,f'New User {user_id} is created'),200)
    
loginParser = reqparse.RequestParser()
loginParser.add_argument('user_id',required=True)
loginParser.add_argument('password',required=True)



@name_space.route("/v1/login")
class MainClass(Resource):
    @name_space.expect(loginParser)
    #insert new user
    def get(self):
        args = loginParser.parse_args()
        user_id,input_password = args['user_id'],args["password"]
        data_dict = UserModel.query.filter(UserModel.user_id == user_id).first()
        print("")
        if not data_dict:
            response =  create_error_response(None,"Username not available.Please try again",404)
            return make_response(response,404)
        else:
            departmentName = data_dict.departmentName
            emailId = data_dict.emailId
        user_password = decrypt_message(data_dict.password)
        if input_password != user_password:
            response =  create_error_response(None,"Password is incorrect. Please try again",401)
            return make_response(response,401)
        #create authToken
        authToken = encrypt_message(user_id) #create_auth_token(user_id)
        #user_data = {"user_id":data_dict.user_id,"departmentName":data_dict.departmentName,"emailId":data_dict.emailId,"authToken":authToken}
        data_dict.authToken = authToken
        db.session.add(data_dict)
        db.session.commit()
        db.session.close()
        user_data = {"user_id":user_id,"departmentName":departmentName,"emailId":emailId}
        return make_response(create_success_response(user_data,None),200)
    
logoutParser = reqparse.RequestParser()
logoutParser.add_argument('user_id',required=True)

@name_space.route("/v1/logout")
class MainClass(Resource):
    @name_space.expect(logoutParser)
    #insert new user
    def put(self):
        args = logoutParser.parse_args()
        user_id = args['user_id']
        user_data = UserModel.query.filter(UserModel.user_id == user_id).first()
        
        if not user_data:
            response =  create_error_response(None,"Username not available.Please try again",404)
            return make_response(response,404)
        user_data.authToken = None
        db.session.add(user_data)
        db.session.commit()
        db.session.close()
        return make_response(create_success_response(None,"User logged out successfully"),200)


tokenParser = reqparse.RequestParser()
tokenParser.add_argument('user_id',required=True)
tokenParser.add_argument('authToken',required=True)

@name_space.route("/v1/check_token")
class MainClass(Resource):
    @name_space.expect(tokenParser)
    def get(self):
        args = tokenParser.parse_args()
        user_id,authToken = args["user_id"],args["authToken"]
        #check for user in db        
        token_response = decode_auth_token(authToken)
        if user_id == token_response:
            return make_response(create_success_response("Token is valid",200),200)
        elif 'login' not in token_response:
            return make_response(create_error_response(None,"Token in invalid",401),401)
        else:    
            return make_response(create_error_response(None,token_response,401),401)
                        
    
##end
if __name__ == '__main__':
    print("FLASK APP STARTED")
    flask_app.run(debug=True, threaded=True, port=5010)
    
    








