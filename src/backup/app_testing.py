from datetime import datetime as dt

import werkzeug
from werkzeug.utils import cached_property

werkzeug.cached_property = cached_property
from flask import Flask, json, make_response, request
from flask_restplus import Api, Resource
from flask_restplus import reqparse
from sqlalchemy_serializer import SerializerMixin
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func, exc
import configparser
import datetime as dtime
import jwt
import logging
from encryption import encrypt_message, decrypt_message
from functools import wraps
from werkzeug.exceptions import InternalServerError
logging.basicConfig(filename="activity.log", level=logging.INFO,
                    format="[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s")
config_path = r"C:\BUMI"

config = configparser.ConfigParser()
config.read(f"{config_path}\\db_config.ini")
MYSQL_HOST = config["MySQL"]["host"]
MYSQL_DATABASE = config["MySQL"]["database"]
MYSQL_USERNAME = config["MySQL"]["user"]
MYSQL_PASSWORD = config["MySQL"]["password"]

flask_app = Flask(__name__)
host, database, username, password = MYSQL_HOST, MYSQL_DATABASE, MYSQL_USERNAME, MYSQL_PASSWORD

uri = "mysql://" + username + ":" + password + "@" + host + "/" + database
flask_app.config['SQLALCHEMY_DATABASE_URI'] = uri
flask_app.config['SQLALCHEMY_ECHO'] = False
flask_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
flask_app.secret_key = "secret_super_key"
# flask_app.config['SQLALCHEMY_ENGINE_OPTIONS'] = {'poolclass': NullPool}

flask_app.config['SQLALCHEMY_POOL_RECYCLE'] = 180
flask_app.config['SQLALCHEMY_POOL_SIZE'] = 0
# flask_app.config['SQLALCHEMY_POOL_TIMEOUT'] = None
flask_app.config['SQLALCHEMY_MAX_OVERFLOW'] = -1
db = SQLAlchemy(flask_app)
app = Api(app=flask_app)
name_space = app.namespace('bumiapi/', description='Bumi APIs')

AUTH_TOKEN_KEY = "BUMIATHTOKENKEY"


def authenticate(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        payload = request.get_json()
        if not payload:
            return make_response(create_error_response(None, "Payload not present", 400), 400)
        if isinstance(payload, dict):
            if "session_user_id" not in payload:
                return make_response(create_error_response(None, "Session UserId not present in payload", 400), 400)
        else:
            return make_response(create_error_response(None, "Bad Request", 400), 400)
        user_id = str(payload["session_user_id"])
        user_data = UserModel.query.filter(UserModel.user_id == user_id).first()
        if user_data:
            if not user_data.authToken:
                return make_response(
                    create_error_response(None, "Authorization token is not Available.Please login again.", 401),
                    401)
            userToken = decrypt_message(user_data.authToken)
            if not userToken:
                return make_response(
                    create_error_response(None, "Authorization token is not Available.Please login again.", 401),
                    401)
            if userToken == user_id:
                return func(*args, **kwargs)
            else:
                return make_response(create_error_response(None, "Invalid Token.Please login again.", 401), 401)
        return make_response(create_error_response(None, "UserId not Found", 401), 401)

    return wrapper


def authenticate_backend(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        auth_token = request.args.get('auth_token', '')
        if not auth_token:
            return make_response(create_error_response(None, "Authorization token not present in the request", 400), 400)
        auth_response = decode_auth_token_backend(auth_token)
        if "ERROR_SIGN" in auth_response:
            return make_response(create_error_response(None, "Authorization token expired. Please log in again.", 403), 403)
        if "ERROR_INVALID" in auth_response:
            return make_response(create_error_response(None, "Invalid authorization token. Please log in again.", 403), 403)
        return func(*args, **kwargs)
    return wrapper

class ConfigurePath(db.Model, SerializerMixin):
    """
       A class to represent a database model for configuring paths.

       ...

       Attributes
       ----------
       Id : int
           unique identifier to configure the file paths
       Input : str
           path to the input folder
       Working : str
           path to the working folder
       Output : str
           path to the output folder
       License_Key : str
           key for the licence authorisation
       License_Flag : str
       Archive : str
           path for the archive folder
       License_Desc : str
           description about the license
       TestFile : str
           path to test file
       tempInput : str
           for temporary input
       """

    __tablename__ = 'configure_path'
    Id = db.Column(db.Integer, primary_key=True, autoincrement=True, default=1)
    Input = db.Column(db.String(30), index=True)
    Working = db.Column(db.String(30), index=True)
    Output = db.Column(db.String(200), index=True)
    License_Key = db.Column(db.String(200), index=True)
    License_Flag = db.Column(db.String(200), index=True)
    Archive = db.Column(db.String(200), index=True)
    License_Desc = db.Column(db.String(200), index=True)
    TestFile = db.Column(db.String(200), index=True)
    tempInput = db.Column(db.String(200), index=True)


@name_space.route("/v1/configure_path")
class MainClass(Resource):
    # Gets all data from configure_path table
    def get(self):
        """
            gets all the configure path records
            ...

            Methods
            -------
            get():
                to get all the configure path records
            """
        flask_app.logger.debug("\nStart time : " + str(
            dtime.datetime.now()) + " -- In function: get" + "configure_path" + "\n")
        result = dict()
        all_result = list()
        # try:
        transaction = ConfigurePath.query.limit(1).all()
        all_result = transaction[0].to_dict()
        result["tempInput"] = all_result["tempInput"]
        result["Input"] = all_result["Input"]
        result["Working"] = all_result["Working"]
        result["Output"] = all_result["Output"]
        result["Archive"] = all_result["Archive"]
        result["TestFile"] = all_result["TestFile"]
        response = create_success_response(result)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- exiting function: get" + "configure_path" + "\n")
        return make_response(response, 200)


customConfigrePathInsertParser = reqparse.RequestParser()
customConfigrePathInsertParser.add_argument('Id')
customConfigrePathInsertParser.add_argument('Input')
customConfigrePathInsertParser.add_argument('Working')
customConfigrePathInsertParser.add_argument('Output')
customConfigrePathInsertParser.add_argument('License_Key')
customConfigrePathInsertParser.add_argument('License_Flag')
customConfigrePathInsertParser.add_argument('Archive')
customConfigrePathInsertParser.add_argument('License_Desc')
customConfigrePathInsertParser.add_argument('tempInput')
customConfigrePathInsertParser.add_argument('TestFile')


@name_space.route("/v1/configure_path")
class MainClass(Resource):
    @name_space.expect(customConfigrePathInsertParser)
    def post(self):
        """
            Inserts a new record into configure_path table
            ...

            Methods
            -------
            get():
                to insert a new record into configure_path table
            """
        flask_app.logger.debug("\nStart time : " + str(
            dtime.datetime.now()) + " -- In function: post " + "configure_path" + "\n")
        args = customConfigrePathInsertParser.parse_args()
        configure_path = ConfigurePath()
        result = dict()
        id, input, Working, output, license_key, license_flag, Archive, license_desc, TestFile, tempInput = args['Id'], \
                                                                                                            args[
                                                                                                                'Input'], \
                                                                                                            args[
                                                                                                                'Working'], \
                                                                                                            args[
                                                                                                                'Output'], \
                                                                                                            args[
                                                                                                                'License_Key'], \
                                                                                                            args[
                                                                                                                'License_Flag'], \
                                                                                                            args[
                                                                                                                'Archive'], \
                                                                                                            args[
                                                                                                                'License_Desc'], \
                                                                                                            args[
                                                                                                                'tempInput'], \
                                                                                                            args[
                                                                                                                'TestFile']
        configure_path.Id = id
        configure_path.Input = input
        configure_path.Working = Working
        configure_path.Output = output
        configure_path.Archive = Archive
        configure_path.TestFile = TestFile
        configure_path.tempInput = tempInput
        if license_key:
            configure_path.License_Key = license_key
        if license_desc:
            configure_path.License_Desc = license_desc
        if license_flag:
            configure_path.License_Flag = license_flag

        db.session.add(configure_path)
        db.session.flush()
        new__id = configure_path.Id
        db.session.commit()
        db.session.close()
        result["configure_path Id"] = new__id
        flask_app.logger.debug("\nnew configure path id : " + new__id + "\n")
        response = create_success_response(result)
        flask_app.logger.debug("\n end time : " + str(
            dtime.datetime.now()) + " -- exiting function: " + "configure_path" + "\n")
        return make_response(response, 200)


# sqlalchemy.exc.IntegrityError: (MySQLdb._exceptions.IntegrityError) (1062, "Duplicate entry '1' for key 'configure_path.PRIMARY'")
customConfigrePathDeleteParser = reqparse.RequestParser()
customConfigrePathDeleteParser.add_argument('Id', required=True)


@name_space.route("/v1/configure_path/<id>")
class MainClass(Resource):
    @name_space.expect(customConfigrePathDeleteParser)
    def delete(self, id):
        """
            Delete an existing  record from configure_path table
            ...

            Methods
            -------
            get():
                to delete an existing record from configure_path table
            """
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: delete" + "configure_path" + "\n")
        result = dict()
        args = customConfigrePathDeleteParser.parse_args()
        config_id = args['Id']
        ConfigurePath.query.filter(ConfigurePath.Id == config_id).delete()
        db.session.commit()
        db.session.close()
        result["Delete message"] = "Configure Path table is updated after delete"
        response = create_success_response(result)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: delete " + "configure_path" + "\n")
        return make_response(response, 200)


customConfigrePathUpdateParser = reqparse.RequestParser()
customConfigrePathUpdateParser.add_argument('Id', required=True)
customConfigrePathUpdateParser.add_argument('Input')
customConfigrePathUpdateParser.add_argument('Working')
customConfigrePathUpdateParser.add_argument('Output')
customConfigrePathUpdateParser.add_argument('License_Key')
customConfigrePathUpdateParser.add_argument('License_Flag')
customConfigrePathUpdateParser.add_argument('Archive')
customConfigrePathUpdateParser.add_argument('License_Desc')
customConfigrePathUpdateParser.add_argument('tempInput')
customConfigrePathUpdateParser.add_argument('TestFile')


@name_space.route("/v1/configure_path")
class MainClass(Resource):
    @name_space.expect(customConfigrePathUpdateParser)
    def put(self):

        """
            Updates an existing record in configure_path table
            ...

            Methods
            -------
            get():
                to insert a new record into configure_path table
            """
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: put" + "configure_path" + "\n")
        result = dict()
        args = customConfigrePathUpdateParser.parse_args()
        id = args['Id']
        input = args['Input'],
        working = args['Working'],
        output = args['Output'],
        license_key = args['License_Key'],
        license_flag = args['License_Flag'],
        archive = args['Archive'],
        license_desc = args['License_Desc'],
        temp_input = args['tempInput'],
        test_file = args['TestFile']
        configure_path = ConfigurePath.query.get(id)
        if input:
            configure_path.Input = input
        if output:
            configure_path.Output = output
        if archive:
            configure_path.Archive = archive
        if test_file:
            configure_path.TestFile = test_file
        if temp_input:
            configure_path.tempInput = temp_input
        db.session.add(configure_path)
        db.session.commit()
        db.session.close()
        result["update message"] = "Configure Path table is updated"
        response = create_success_response(result)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: put" + "configure_path" + "\n")
        return make_response(response, 200)


class ComplimentaryDomain(db.Model, SerializerMixin):
    """
       A class to represent the database table complimentarydomain.

       ...

       Attributes
       ----------
       domain : str
           name of the domain
       keywords : str
           all keywords
       sections : str
           all sections to identify
       synonyms : str
           all synonyms associated with the sections

       """
    __tablename__ = 'complimentorydomain'
    Id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    domain = db.Column(db.String(30), index=True)
    keywords = db.Column(db.String(500), index=True)
    sections = db.Column(db.String(30), index=True)
    synonyms = db.Column(db.String(500), index=True)


@name_space.route("/v1/complimentarydomain")
class MainClass(Resource):
    def get(self):
        """
            Fetches all records present in the complimentarydomain table
            ...

            Methods
            -------
            get():
                to get data from complimentarydomain table
            """
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get" + "complimentarydomain" + "\n")
        result = dict()
        all_result = list()
        all_data = ComplimentaryDomain.query.all()
        data_list = []
        for data in all_data:
            data_list.append(data.to_dict())
        response = create_success_response(data_list)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get" + "complimentarydomain" + "\n")
        return make_response(response, 200)


@name_space.route("/v1/complimentarydomain/<domain_Name>")
class MainClass(Resource):
    def get(self, domain_Name):
        """
            Fetches the record present in the complimentarydomain table for given domain name
            ...

            Methods
            -------
            get():
                to get data from complimentarydomain table for a given domain name
            """
        flask_app.logger.debug(
            "\nstart time : " + str(dtime.datetime.now()) + " -- In function: get" + "complimentarydomain" + "\n")
        result = dict()
        data_dict = list()
        domain_name = domain_Name
        # try:)
        data_dict = ComplimentaryDomain.query.filter(ComplimentaryDomain.domain == domain_name).first()
        if not data_dict:
            response = create_error_response(None, "complimentorysections data not available", 404)
            flask_app.logger.debug("\nend time : " + str(
                dtime.datetime.now()) + " -- Exiting function: get" + "complimentarydomain" + "\n")
            return make_response(response, 404)
        response_dict = data_dict.to_dict()
        response = create_success_response(response_dict)
        flask_app.logger.debug(
            "\nend time : " + str(dtime.datetime.now()) + " -- Exiting function: get" + "complimentarydomain" + "\n")
        return make_response(response, 200)


class ComplimentarySections(db.Model, SerializerMixin):
    """
       A class to represent the database table complimentorysections

       ...

       Attributes
       ----------
       sections : str
           name of the section
       keywords : str
           all keywords for a section
       synonyms : str
           all synonyms associated with a section

       """
    __tablename__ = 'complimentorysections'
    sections = db.Column(db.String(30), primary_key=True, index=True)
    keywords = db.Column(db.String(500), index=True)
    synonyms = db.Column(db.String(500), index=True)


@name_space.route("/v1/complimentarysections")
class MainClass(Resource):
    method_decorators = [authenticate]

    # get all ComplimentarySections data present in the table
    def get(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get" + "complimentarysections" + "\n")
        all_data = ComplimentarySections.query.all()
        data_list = []
        for data in all_data:
            data_list.append(data.to_dict())
        response = create_success_response(data_list)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get" + "complimentarysections" + "\n")
        return make_response(response, 200)


ComplimentarySectionsParser = reqparse.RequestParser()
ComplimentarySectionsParser.add_argument('keywords')
ComplimentarySectionsParser.add_argument('synonyms')


@name_space.route("/v1/complimentarysections/<sections>")
class MainClass(Resource):
    method_decorators = [authenticate]

    def get(self, sections):
        """
                    Fetches the record present in the complimentarysections table for a given section name
                    ...

                    Methods
                    -------
                    get():
                        to get data from complimentarysections table for a given section name
                    """
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get" + "complimentarysections" + "\n")
        sections_name = sections
        data_dict = ComplimentarySections.query.filter(ComplimentarySections.sections == sections).first()
        if not data_dict:
            response = create_error_response(None, "complimentorysections data not available", 404)
            flask_app.logger.debug("\nend time : " + str(
                dtime.datetime.now()) + " -- Exiting function: get" + "complimentarysections" + "\n")
            return make_response(response, 404)
        response_dict = data_dict.to_dict()
        response = create_success_response(response_dict)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get" + "complimentarysections" + "\n")
        return make_response(response, 200)

    def delete(self, sections):
        """
                    Deletes an existing record present in the complimentarysections table for a given section name
                    ...

                    Methods
                    -------
                    delete():
                        to delete data from complimentarysections table for a given section name
                    """
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: delete" + "complimentarysections" + "\n")
        section_name = sections
        data_dict = ComplimentarySections.query.filter(ComplimentarySections.sections == section_name).delete()
        db.session.commit()
        db.session.close()
        response = create_success_response(None, "ComplimentarySections deleted successfully")
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: delete" + "complimentarysections" + "\n")
        return make_response(response, 200)

    # update Complimentary Sections
    @name_space.expect(ComplimentarySectionsParser)
    def put(self, sections):
        """
        Updates details for an existing record in the complimentarysections table for a given section name
        ...

        Methods
        -------
         get():
               to update details for an existing record in the complimentarysections table for a given section name
        """
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: put" + "complimentarysections" + "\n")
        sections_name = sections
        args = ComplimentarySectionsParser.parse_args()
        keywords, synonyms = args["keywords"], args["synonyms"]
        complimentary_sections = ComplimentarySections.query.filter(
            ComplimentarySections.sections == sections_name).one()
        if keywords:
            complimentary_sections.keywords = keywords
        if synonyms:
            complimentary_sections.synonyms = synonyms
        db.session.add(complimentary_sections)
        db.session.commit()
        db.session.close()
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: put" + "complimentarysections" + "\n")
        return make_response(f'Domain {sections} is updated', 200)


ComplimentarySectionsInsertParser = reqparse.RequestParser()
ComplimentarySectionsInsertParser.add_argument('sections', required=True)
ComplimentarySectionsInsertParser.add_argument('keywords')
ComplimentarySectionsInsertParser.add_argument('synonyms')


@name_space.route("/v1/complimentarysections")
class MainClass(Resource):
    # get all complimentorysections data
    method_decorators = [authenticate]

    def get(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get" + "complimentarysections" + "\n")
        all_data = ComplimentarySections.query.all()
        data_list = []
        for data in all_data:
            data_list.append(data.to_dict())
        response = create_success_response(data_list)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get" + "complimentarysections" + "\n")
        return make_response(response, 200)

    @name_space.expect(ComplimentarySectionsInsertParser)
    # insert new department
    def post(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: post" + "complimentarysections" + "\n")
        args = ComplimentarySectionsInsertParser.parse_args()
        sections, keywords, synonyms = args["sections"], args["keywords"], args["synonyms"]
        complimentarySections = ComplimentarySections()
        complimentarySections.sections = sections
        complimentarySections.keywords = keywords
        complimentarySections.synonyms = synonyms
        db.session.add(complimentarySections)
        db.session.commit()
        db.session.close()
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: post" + "complimentarysections" + "\n")
        return make_response(f'New Complimentary section {sections} is created', 200)


class CustomDomainInstance(db.Model, SerializerMixin):
    """
        A class to represent a database model for the custom domain instance table.

        ...

        Attributes
        ----------
        id : int
            unique identifier to get the custom domain instance
        domain : str
            name of the domain
        instance : str
            name of the instance
        keywords : str
            all keywords associated with that domain
        sections : str
            all sections to identify
        synonyms : str
            all synonyms to search for
        """
    __tablename__ = 'customdomainforinstance'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    domain = db.Column(db.String(30), index=True)
    instance = db.Column(db.String(30), index=True)
    keywords = db.Column(db.String(200), index=True)
    sections = db.Column(db.String(200), index=True)
    synonyms = db.Column(db.String(200), index=True)


CustomDomainInstanceParser = reqparse.RequestParser()
CustomDomainInstanceParser.add_argument('id')
CustomDomainInstanceParser.add_argument('domain', required=True, default=None)
CustomDomainInstanceParser.add_argument('instance', required=True, default=None)
CustomDomainInstanceParser.add_argument('keywords', default=None)
CustomDomainInstanceParser.add_argument('sections', default=None)
CustomDomainInstanceParser.add_argument('synonyms', default=None)


@name_space.route("/v1/customdomainforinstance")
class MainClass(Resource):
    method_decorators = [authenticate]

    # get all CustomDomainInstance data
    def get(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get" + "customdomainforinstance" + "\n")
        all_data = CustomDomainInstance.query.all()
        data_list = []
        for data in all_data:
            data_list.append(data.to_dict())
        response = create_success_response(data_list)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get" + "customdomainforinstance" + "\n")
        return make_response(response, 200)

    @name_space.expect(CustomDomainInstanceParser)
    # insert new CustomDomainInstance
    def post(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: post" + "customdomainforinstance" + "\n")
        args = CustomDomainInstanceParser.parse_args()
        id, domain, instance, keywords, sections, synonyms = args['id'], args['domain'], args['instance'], args[
            'keywords'], \
                                                             args['sections'], args['synonyms']
        customDomainInstance = CustomDomainInstance()
        customDomainInstance.id = id
        customDomainInstance.domain = domain
        customDomainInstance.instance = instance
        customDomainInstance.keywords = keywords
        customDomainInstance.sections = sections
        customDomainInstance.synonyms = synonyms
        db.session.add(customDomainInstance)
        db.session.flush()
        new_customDomainInstance_id = customDomainInstance.id
        db.session.commit()
        db.session.close()
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: post" + "customdomainforinstance" + "\n")
        return make_response(f'New CustomDomainInstance for domain {domain} and instance {instance} is created with '
                             f'id {new_customDomainInstance_id}', 200)


@name_space.route("/v1/backend/customdomainforinstance")
class MainClass(Resource):
    method_decorators = [authenticate_backend]
    # get all CustomDomainInstance data
    def get(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get" + "customdomainforinstance" + "\n")
        all_data = CustomDomainInstance.query.all()
        data_list = []
        for data in all_data:
            data_list.append(data.to_dict())
        response = create_success_response(data_list)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get" + "customdomainforinstance" + "\n")
        return make_response(response, 200)

    @name_space.expect(CustomDomainInstanceParser)
    # insert new CustomDomainInstance
    def post(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: post" + "customdomainforinstance" + "\n")
        args = CustomDomainInstanceParser.parse_args()
        id, domain, instance, keywords, sections, synonyms = args['id'], args['domain'], args['instance'], args[
            'keywords'], \
                                                             args['sections'], args['synonyms']
        customDomainInstance = CustomDomainInstance()
        customDomainInstance.id = id
        customDomainInstance.domain = domain
        customDomainInstance.instance = instance
        customDomainInstance.keywords = keywords
        customDomainInstance.sections = sections
        customDomainInstance.synonyms = synonyms
        db.session.add(customDomainInstance)
        db.session.flush()
        new_customDomainInstance_id = customDomainInstance.id
        db.session.commit()
        db.session.close()
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: post" + "customdomainforinstance" + "\n")
        return make_response(f'New CustomDomainInstance for domain {domain} and instance {instance} is created with '
                             f'id {new_customDomainInstance_id}', 200)


CustomDomainInstanceUpdateParser = reqparse.RequestParser()
CustomDomainInstanceUpdateParser.add_argument('id')
CustomDomainInstanceUpdateParser.add_argument('domain', default=None)
CustomDomainInstanceUpdateParser.add_argument('instance', default=None)
CustomDomainInstanceUpdateParser.add_argument('keywords', default=None)
CustomDomainInstanceUpdateParser.add_argument('sections', default=None)
CustomDomainInstanceUpdateParser.add_argument('synonyms', default=None)


@name_space.route("/v1/customdomainforinstance/<domainName>/<InstanceName>")
class MainClass(Resource):
    # extract CustomDomainInstance details
    method_decorators = [authenticate]

    def get(self, domainName, InstanceName):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get" + "customdomainforinstance" + "\n")
        search_domain = domainName
        search_instance = InstanceName
        data_dict = CustomDomainInstance.query.filter(CustomDomainInstance.domain == search_domain). \
            filter(CustomDomainInstance.instance == search_instance).first()
        if not data_dict:
            response = create_error_response(None, "CustomDomainInstance data not available", 404)
            flask_app.logger.debug("\nend time : " + str(
                dtime.datetime.now()) + " -- Exiting function: get" + "customdomainforinstance" + "\n")
            return make_response(response, 404)
        response_dict = data_dict.to_dict()
        response = create_success_response(response_dict)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get" + "customdomainforinstance" + "\n")
        return make_response(response, 200)

    # delete CustomDomainInstance
    def delete(self, domainName, InstanceName):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: delete" + "customdomainforinstance" + "\n")
        search_domain = domainName
        search_instance = InstanceName
        data_dict = CustomDomainInstance.query.filter(CustomDomainInstance.domain == search_domain). \
            filter(CustomDomainInstance.instance == search_instance).delete()
        db.session.commit()
        db.session.close()
        response = create_success_response(None, "CustomDomainInstance deleted successfully")
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: delete" + "customdomainforinstance" + "\n")
        return make_response(response, 200)

    # update CustomDomainInstance
    @name_space.expect(CustomDomainInstanceUpdateParser)
    def put(self, domainName, InstanceName):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: put" + "customdomainforinstance" + "\n")
        search_domain = domainName
        search_instance = InstanceName
        args = CustomDomainInstanceUpdateParser.parse_args()
        keywords, sections, synonyms = args["keywords"], args["sections"], args["synonyms"]
        update_CustomDomainInstance = CustomDomainInstance.query.filter(CustomDomainInstance.domain == search_domain). \
            filter(CustomDomainInstance.instance == search_instance).first()
        if keywords:
            update_CustomDomainInstance.keywords = keywords
        if sections:
            update_CustomDomainInstance.sections = sections
        if synonyms:
            update_CustomDomainInstance.synonyms = synonyms
        db.session.add(update_CustomDomainInstance)
        db.session.commit()
        db.session.close()
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: put" + "customdomainforinstance" + "\n")
        return make_response(
            f'CustomDomainInstance details are updated for Domain {search_domain} and Instance {search_instance}', 200)


class CustomSeededsections(db.Model, SerializerMixin):
    """
    A class to represent the database model customseededsections.

    ...

    Attributes
    ----------
    id : int
        unique identifier to get the custom seeded sections
    domain : str
        name of the domain
    instance : str
        name of the instance
    keywords : str
        all keywords associated with that section
    sections : str
        all sections to identify
    synonyms : str
        all synonyms associated with the custom sections

    """
    __tablename__ = 'customseededsections'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    domain = db.Column(db.String(30), index=True)
    instance = db.Column(db.String(30), index=True)
    keywords = db.Column(db.String(200), index=True)
    sections = db.Column(db.String(200), index=True)
    synonyms = db.Column(db.String(200), index=True)


CustomSeededsectionsParser = reqparse.RequestParser()
CustomSeededsectionsParser.add_argument('id')
CustomSeededsectionsParser.add_argument('domain', required=True, default=None)
CustomSeededsectionsParser.add_argument('instance', required=True, default=None)
CustomSeededsectionsParser.add_argument('keywords', default=None)
CustomSeededsectionsParser.add_argument('sections', default=None)
CustomSeededsectionsParser.add_argument('synonyms', default=None)


@name_space.route("/v1/customseededsections")
class MainClass(Resource):
    method_decorators = [authenticate]

    # Gets all customseededsections data
    def get(self):
        """
            gets all the custom seeded sections
            ...

            Methods
            -------
            get():
                to get all the custom seeded sections
            """
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get" + "customseededsections" + "\n")
        all_data = CustomSeededsections.query.all()
        data_list = []
        for data in all_data:
            data_list.append(data.to_dict())
        response = create_success_response(data_list)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get" + "customseededsections" + "\n")
        return make_response(response, 200)

    @name_space.expect(CustomSeededsectionsParser)
    # insert new customseededsections
    def post(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: post" + "customseededsections" + "\n")
        args = CustomSeededsectionsParser.parse_args()
        id, domain, instance, keywords, sections, synonyms = args['id'], args['domain'], args['instance'], args[
            'keywords'], \
                                                             args['sections'], args['synonyms']
        customSeededsections = CustomSeededsections()
        customSeededsections.id = id
        customSeededsections.domain = domain
        customSeededsections.instance = instance
        customSeededsections.keywords = keywords
        customSeededsections.sections = sections
        customSeededsections.synonyms = synonyms
        db.session.add(customSeededsections)
        db.session.flush()
        new_customSeededsections_id = customSeededsections.id
        db.session.commit()
        db.session.close()
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: post" + "customseededsections" + "\n")
        return make_response(
            f'New new_customSeededsections_id for domain {domain} and instance {instance} is created with '
            f'id {new_customSeededsections_id}', 200)


CustomSeededsectionsUpdateParser = reqparse.RequestParser()
CustomSeededsectionsUpdateParser.add_argument('id')
CustomSeededsectionsUpdateParser.add_argument('domain', default=None)
CustomSeededsectionsUpdateParser.add_argument('instance', default=None)
CustomSeededsectionsUpdateParser.add_argument('keywords', default=None)
CustomSeededsectionsUpdateParser.add_argument('sections', default=None)
CustomSeededsectionsUpdateParser.add_argument('synonyms', default=None)


@name_space.route("/v1/customseededsections/<domainName>/<InstanceName>")
class MainClass(Resource):
    method_decorators = [authenticate]

    # extract customseededsections details for given domain name and instance name
    def get(self, domainName, InstanceName):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get" + "customseededsections" + "\n")
        search_domain = domainName
        search_instance = InstanceName
        data_dict = CustomSeededsections.query.filter(CustomSeededsections.domain == search_domain). \
            filter(CustomSeededsections.instance == search_instance).first()
        if not data_dict:
            response = create_error_response(None, "CustomSeededsections data not available", 404)
            flask_app.logger.debug("\nend time : " + str(
                dtime.datetime.now()) + " -- Exiting function: get" + "customseededsections" + "\n")
            return make_response(response, 404)
        response_dict = data_dict.to_dict()
        response = create_success_response(response_dict)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get" + "customseededsections" + "\n")
        return make_response(response, 200)

    # delete CustomSeededsections record for given domain name and instance name
    def delete(self, domainName, InstanceName):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: delete" + "customseededsections" + "\n")
        search_domain = domainName
        search_instance = InstanceName
        data_dict = CustomSeededsections.query.filter(CustomSeededsections.domain == search_domain). \
            filter(CustomSeededsections.instance == search_instance).delete()
        db.session.commit()
        db.session.close()
        response = create_success_response(None, "CustomSeededsections deleted successfully")
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: delete" + "customseededsections" + "\n")
        return make_response(response, 200)

    # Update CustomSeededsectionsUpdateParser for given domain name and instance name
    @name_space.expect(CustomSeededsectionsUpdateParser)
    def put(self, domainName, InstanceName):
        """
          Updates details for an existing record in the complimentarysections table for a given section name
          ...

           Methods
            -------
            put():
                 To update details for an existing record in the complimentarysections table for a given section name
            """
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: put" + "customseededsections" + "\n")
        search_domain = domainName
        search_instance = InstanceName
        args = CustomSeededsectionsUpdateParser.parse_args()
        keywords, sections, synonyms = args["keywords"], args["sections"], args["synonyms"]
        update_CustomSeededsections = CustomSeededsections.query.filter(CustomSeededsections.domain == search_domain). \
            filter(CustomSeededsections.instance == search_instance).first()
        if keywords:
            update_CustomSeededsections.keywords = keywords
        if sections:
            update_CustomSeededsections.sections = sections
        if synonyms:
            update_CustomSeededsections.synonyms = synonyms
        db.session.add(update_CustomSeededsections)
        db.session.commit()
        db.session.close()
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: put" + "customseededsections" + "\n")
        return make_response(
            f'CustomSeededsections details are updated for Domain {search_domain} and Instance {search_instance}', 200)


class Transaction(db.Model, SerializerMixin):
    """
       A class to represent a transaction.
       It is used to create a db model for the transaction table.

       ...

       Attributes
       ----------
       ID : Big int
           unique id to identify the transaction
       no_pages : int
           number of pages
       InstanceName : str
           name of the instance
       DomainType : str
           type of the domain
       Filepath : str
           path to the file
       WorkingDirFilepath : str
           path to the working directory
       Filename : str
           name of the file
       Timestamp : datetime
           time of the transaction
       Filestatus : str
           status of the file
       Error_Description : str
           shows the error description
       OutputCSVPath : str
           shows the path to the output csv folder
       Payload_Request : str
           contains the payload request
       Payload_Response : str
           contains the payload response
       dateOfContract : datetime
           date of the contract
       departmentName : str
           department name that is giving the request
       partyOne : str
       partyTwo : str
       userName : str
           shows the user's name
       ui_flag : int
       """
    __tablename__ = 'transaction'
    ID = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    no_pages = db.Column(db.Integer)
    InstanceName = db.Column(db.String(30))
    DomainType = db.Column(db.String(30))
    Filepath = db.Column(db.String(200))
    WorkingDirFilepath = db.Column(db.String(200))
    Filename = db.Column(db.String(200))
    Timestamp = db.Column(db.DateTime, default=dt.utcnow, onupdate=dt.utcnow)
    Filestatus = db.Column(db.String(50))
    Error_Description = db.Column(db.String(200))
    OutputCSVPath = db.Column(db.String(200))
    Payload_Request = db.Column(db.Text)
    Payload_Response = db.Column(db.Text)
    dateOfContract = db.Column(db.DateTime, default=dt.utcnow, onupdate=dt.utcnow)
    departmentName = db.Column(db.String(200))
    partyOne = db.Column(db.String(400))
    partyTwo = db.Column(db.String(400))
    userName = db.Column(db.String(200))
    ui_flag = db.Column(db.Integer, default=0)


customTransactionParser = reqparse.RequestParser()
customTransactionParser.add_argument('ID')
customTransactionParser.add_argument('InstanceName', required=True)
customTransactionParser.add_argument('DomainType', required=True)
customTransactionParser.add_argument('Filepath', required=True)
customTransactionParser.add_argument('Filename', required=True)
customTransactionParser.add_argument('FileStatus', required=True)
customTransactionParser.add_argument('departmentName', required=True)
customTransactionParser.add_argument('userName', required=True)
customTransactionParser.add_argument('wdfilepath_exists')
customTransactionParser.add_argument('ui_flag', default=0)
customTransactionParser.add_argument('no_pages', default=None)
customTransactionParser.add_argument('working_file_path', default=None)
customTransactionParser.add_argument('out_csv_file_path', default=None)
customTransactionParser.add_argument('error_description', default=None)
customTransactionParser.add_argument('payload_request', default=None)
customTransactionParser.add_argument('payload_response', default=None)


@name_space.route("/v1/transaction/row_count")
class MainClass(Resource):
    method_decorators = [authenticate]

    # get all Transaction data
    def get(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get" + "transaction" + "\n")
        result = dict()
        result["row_count"] = Transaction.query.count()
        # print("row", type(result["row_count"]))
        response = create_success_response(result)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get" + "transaction" + "\n")
        return make_response(response, 200)


@name_space.route("/v1/transaction/transaction_classified_count")
class MainClass(Resource):
    method_decorators = [authenticate]

    # get all Transaction data
    def get(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get" + "transaction" + "\n")
        result = dict()
        result["row_count"] = Transaction.query.filter(Transaction.Filestatus == "Unclassified").count()
        print("row", type(result["row_count"]))
        response = create_success_response(result)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get" + "transaction" + "\n")
        return make_response(response, 200)


@name_space.route("/v1/transaction/total_transactions_today")
class MainClass(Resource):
    method_decorators = [authenticate]

    # get all Transaction data
    def get(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get total transactions today" + "transaction" + "\n")
        result = dict()
        today = str(dtime.date.today()) + "%"
        result["row_count"] = Transaction.query.filter(Transaction.Timestamp.like(today)).count()
        response = create_success_response(result)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get total transactions today" + "transaction" + "\n")
        return make_response(response, 200)


@name_space.route("/v1/transaction/total_pages")
class MainClass(Resource):
    method_decorators = [authenticate]

    # get all Transaction data
    def get(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get total pages " + "transaction" + "\n")
        result = dict()
        result["total_pages"] = int(db.session.query(db.func.sum(Transaction.no_pages)).
                                    filter(Transaction.no_pages != None).scalar())
        response = create_success_response(result)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get total pages " + "transaction" + "\n")
        return make_response(response, 200)


@name_space.route("/v1/transaction/total_pages_today")
class MainClass(Resource):
    method_decorators = [authenticate]

    # get all Transaction data
    def get(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get total pages processed today:" + "transaction" + "\n")
        result = dict()
        today = str(dtime.date.today()) + "%"
        result["total_pages"] = int(db.session.query(db.func.sum(Transaction.no_pages)).
                                    filter(Transaction.no_pages != None).filter(
            Transaction.Timestamp.like(today)).scalar())
        response = create_success_response(result)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get total pages processed today:" + "transaction" + "\n")
        return make_response(response, 200)


@name_space.route("/v1/Transaction")
class MainClass(Resource):
    method_decorators = [authenticate]

    # get all Transaction data
    def get(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get:" + "transaction" + "\n")
        all_data = Transaction.query.all()
        data_list = []
        for data in all_data:
            data_list.append(data.to_dict())
        response = create_success_response(data_list)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get:" + "transaction" + "\n")
        return make_response(response, 200)

    @name_space.expect(customTransactionParser)
    # insert new transaction
    def post(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: insert new transaction:" + "transaction" + "\n")
        args = customTransactionParser.parse_args()

        instance, domain, filepath, filename, filestatus, uiFlag, department, username = args['InstanceName'], args[
            'DomainType'], args['Filepath'], args['Filename'], args['FileStatus'], args['ui_flag'], args[
                                                                                             'departmentName'], args[
                                                                                             'userName']
        result = dict()
        # common instance , domaintype, department, username

        no_pages, wdfilepath, outcsvfilepath, error_description, payload_request, payload_response = args['no_pages'], \
                                                                                                     args[
                                                                                                         'working_file_path'], \
                                                                                                     args[
                                                                                                         'out_csv_file_path'], \
                                                                                                     args[
                                                                                                         'error_description'], \
                                                                                                     args[
                                                                                                         'payload_request'], \
                                                                                                     args[
                                                                                                         'payload_response']
        transaction = Transaction()
        transaction.InstanceName = instance
        transaction.DomainType = domain
        transaction.Filepath = filepath
        transaction.Filename = filename
        transaction.FileStatus = filestatus
        transaction.ui_flag = uiFlag
        transaction.departmentName = department
        transaction.userName = username

        transaction.no_pages = no_pages
        transaction.InstanceName = instance
        transaction.DomainType = domain
        transaction.Filepath = filepath
        transaction.WorkingDirFilepath = wdfilepath
        transaction.OutputCSVPath = outcsvfilepath
        transaction.Filename = filename
        transaction.Filestatus = filestatus
        transaction.Error_Description = error_description
        transaction.Payload_Request = payload_request
        transaction.Payload_Response = payload_response
        transaction.departmentName = department
        transaction.userName = username

        db.session.add(transaction)
        db.session.flush()
        new_transanction_id = transaction.ID
        db.session.commit()
        db.session.close()
        result["transaction_id"] = new_transanction_id
        response = create_success_response(result)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: insert new transaction:" + "transaction" + "\n")
        return make_response(response, 200)


@name_space.route("/v1/backend/transaction")
class MainClass(Resource):
    # get all Transaction data
    method_decorators = [authenticate_backend]
    def get(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get:" + "transaction" + "\n")
        all_data = Transaction.query.all()
        data_list = []
        for data in all_data:
            data_list.append(data.to_dict())
        response = create_success_response(data_list)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get:" + "transaction" + "\n")
        return make_response(response, 200)

    @name_space.expect(customTransactionParser)
    # insert new transaction
    def post(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: insert new transaction:" + "transaction" + "\n")
        args = customTransactionParser.parse_args()

        instance, domain, filepath, filename, filestatus, uiFlag, department, username = args['InstanceName'], args[
            'DomainType'], args['Filepath'], args['Filename'], args['FileStatus'], args['ui_flag'], args[
                                                                                             'departmentName'], args[
                                                                                             'userName']
        result = dict()
        # common instance , domaintype, department, username

        no_pages, wdfilepath, outcsvfilepath, error_description, payload_request, payload_response = args['no_pages'], \
                                                                                                     args[
                                                                                                         'working_file_path'], \
                                                                                                     args[
                                                                                                         'out_csv_file_path'], \
                                                                                                     args[
                                                                                                         'error_description'], \
                                                                                                     args[
                                                                                                         'payload_request'], \
                                                                                                     args[
                                                                                                         'payload_response']
        transaction = Transaction()
        transaction.InstanceName = instance
        transaction.DomainType = domain
        transaction.Filepath = filepath
        transaction.Filename = filename
        transaction.FileStatus = filestatus
        transaction.ui_flag = uiFlag
        transaction.departmentName = department
        transaction.userName = username

        transaction.no_pages = no_pages
        transaction.InstanceName = instance
        transaction.DomainType = domain
        transaction.Filepath = filepath
        transaction.WorkingDirFilepath = wdfilepath
        transaction.OutputCSVPath = outcsvfilepath
        transaction.Filename = filename
        transaction.Filestatus = filestatus
        transaction.Error_Description = error_description
        transaction.Payload_Request = payload_request
        transaction.Payload_Response = payload_response
        transaction.departmentName = department
        transaction.userName = username

        db.session.add(transaction)
        db.session.flush()
        new_transanction_id = transaction.ID
        db.session.commit()
        db.session.close()
        result["transaction_id"] = new_transanction_id
        response = create_success_response(result)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: insert new transaction:" + "transaction" + "\n")
        return make_response(response, 200)


customTransactionFileUpdateParser = reqparse.RequestParser()
customTransactionFileUpdateParser.add_argument('ID', type=int, required=True)
customTransactionFileUpdateParser.add_argument('Error_Description', type=str, default=None)
customTransactionFileUpdateParser.add_argument('WorkingDirFilepath', type=str, default=None)
customTransactionFileUpdateParser.add_argument('Payload_Request', type=str, default=None)
customTransactionFileUpdateParser.add_argument('Payload_Response', type=str, default=None)
customTransactionFileUpdateParser.add_argument('InstanceName')
customTransactionFileUpdateParser.add_argument('DomainType')
customTransactionFileUpdateParser.add_argument('Filepath')
customTransactionFileUpdateParser.add_argument('Filename')
customTransactionFileUpdateParser.add_argument('FileStatus')
customTransactionFileUpdateParser.add_argument('departmentName')
customTransactionFileUpdateParser.add_argument('userName')
customTransactionFileUpdateParser.add_argument('wdfilepath_exists')
customTransactionFileUpdateParser.add_argument('ui_flag', default=0)
customTransactionFileUpdateParser.add_argument('no_pages', default=None)
customTransactionFileUpdateParser.add_argument('OutputCSVPath', default=None)


@name_space.route("/v1/backend/transaction/<transaction_id>")
class MainClass(Resource):
    method_decorators = [authenticate_backend]
    # extract transaction details
    def get(self, transaction_id):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get transaction for transaction_id" + transaction_id + "transaction" + "\n")
        search_tran = transaction_id
        data_dict = Transaction.query.filter(Transaction.ID == search_tran).first()
        if not data_dict:
            response = create_error_response(None, "Transaction data not available", 404)
            flask_app.logger.debug("\nend time : " + str(
                dtime.datetime.now()) + " -- Exiting function: get transaction for transaction_id" + transaction_id + "transaction" + "\n")
            return make_response(response, 404)
        response_dict = data_dict.to_dict()
        response = create_success_response(data_dict)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get transaction for transaction_id" + transaction_id + "transaction" + "\n")
        return make_response(response, 200)

    # delete transaction
    def delete(self, transaction_id):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: delete transaction for transaction_id" + transaction_id + "transaction" + "\n")
        search_tran = transaction_id
        data_dict = Transaction.query.filter(Transaction.ID == search_tran).delete()
        db.session.commit()
        db.session.close()
        response = create_success_response(None, "Transaction deleted successfully")
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: delete transaction for transaction_id" + transaction_id + "transaction" + "\n")
        return make_response(response, 200)

    # update department
    @name_space.expect(customTransactionFileUpdateParser)
    def put(self, transaction_id):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: update transaction for transaction_id" + transaction_id + "transaction" + "\n")
        result = dict()
        args = customTransactionFileUpdateParser.parse_args()
        transaction_id, instance, domain, filepath, filename, filestatus, uiFlag, department, username, wdfilepath_exists = \
            args['ID'], args['InstanceName'], args[
                'DomainType'], args['Filepath'], args['Filename'], args['FileStatus'], args['ui_flag'], args[
                'departmentName'], args[
                'userName'], args['wdfilepath_exists']
        wdfilepath, payload_response, error_desc, outputCSVPath = args['working_file_path'], args['payload_response'], \
                                                                  args[
                                                                      'error_desc'], args['OutputCSVPath']
        transaction = Transaction.query.get(transaction_id)
        if transaction is not None:
            if instance:
                transaction.InstanceName = instance
            if department:
                transaction.departmentName = department
            if username:
                transaction.userName = username
            if domain:
                transaction.DomainType = domain
            if filepath:
                transaction.Filepath = filepath
            if wdfilepath:
                transaction.WorkingDirFilepath = wdfilepath
            if filename:
                transaction.Filename = filename
            if not filestatus:
                transaction.Filestatus = 'Validated'
            else:
                transaction.Filestatus = filestatus
            if not error_desc:
                transaction.Error_Description = 'No error'
            else:
                transaction.Error_Description = error_desc
            transaction.Payload_Response = payload_response
            if wdfilepath_exists == "No":
                transaction.Error_Description = 'Working directory file not exist'
                transaction.Filestatus = 'Error'
            db.session.add(transaction)
            db.session.commit()
            db.session.close()
        result["update message"] = "File status for given transaction Id is updated"
        response = create_success_response(result)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: update transaction for transaction_id" + transaction_id + "transaction" + "\n")
        return make_response(response, 200)


customTransactionMaxParser = reqparse.RequestParser()
customTransactionMaxParser.add_argument('auth_token', required=True)


@name_space.route("/v1/backend/transaction/get_max_transaction_id")
class MainClass(Resource):
    method_decorators = [authenticate_backend]
    @name_space.expect(customTransactionMaxParser)
    def get(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get max transaction_id" + "transaction" + "\n")
        result = dict()
        result["max_transaction_id"] = db.session.query(func.max(Transaction.ID)).scalar()
        response = create_success_response(result)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get max transaction_id" + "transaction" + "\n")
        return make_response(response, 200)


# ALL The DEPARTMENT CODE
class DepartmentModel(db.Model, SerializerMixin):
    __tablename__ = 'department'
    departmentName = db.Column(db.String(255), primary_key=True)
    departmentDescription = db.Column(db.String(255), default=None)
    createdBy = db.Column(db.String(255), default=None)
    createDate = db.Column(db.DateTime, default=dt.utcnow, onupdate=dt.utcnow)


departmentCreateParser = reqparse.RequestParser()
departmentCreateParser.add_argument('departmentName', required=True)
departmentCreateParser.add_argument('departmentDescription', default=None)
departmentCreateParser.add_argument('createdBy', required=True)

departmentUpdateParser = reqparse.RequestParser()
departmentUpdateParser.add_argument('departmentName', required=True)
departmentUpdateParser.add_argument('departmentDescription', default=None)
departmentUpdateParser.add_argument('createdBy', default=None)

departmentDetailsParser = reqparse.RequestParser()
departmentDetailsParser.add_argument('departmentName', required=True)


@name_space.route("/v1/department")
class MainClass(Resource):
    method_decorators = [authenticate]

    # get all depatments data
    def get(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get" + "department" + "\n")
        all_data = DepartmentModel.query.all()
        data_list = []
        for data in all_data:
            data_list.append(data.to_dict())
        response = create_success_response(data_list)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get" + "department" + "\n")
        return make_response(response, 200)

    def delete(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: delete" + "department" + "\n")
        try:
            num_rows_deleted = db.session.query(DepartmentModel).delete()
            db.session.commit()
        except:
            db.session.rollback()
        response = create_success_response(None, "All the departments are removed")
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get" + "department" + "\n")
        return make_response(response, 200)

    @name_space.expect(departmentCreateParser)
    # insert new department
    def post(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: inserting department details" + "department" + "\n")
        args = departmentCreateParser.parse_args()

        departmentName, departmentDescription, createdBy = args["departmentName"], args["departmentDescription"], args[
            "createdBy"]
        new_department = DepartmentModel()
        new_department.departmentName = departmentName
        new_department.departmentDescription = departmentDescription
        new_department.createdBy = createdBy
        try:
            db.session.add(new_department)
            db.session.commit()
            db.session.close()
        except exc.IntegrityError:
            response = create_error_response(None, "Department name already exists", 409)
            return make_response(response, 409)

        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- In function: inserting department details" + "department" + "\n")
        return make_response(f'New Department {departmentName} is created', 200)


@name_space.route("/v1/department/<deptName>")
class MainClass(Resource):
    method_decorators = [authenticate]

    # extract dept details
    def get(self, deptName):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get department details for department:" + deptName + "department" + "\n")
        search_dept = deptName
        data_dict = DepartmentModel.query.filter(DepartmentModel.departmentName == search_dept).first()
        if not data_dict:
            response = create_error_response(None, "Depatment data not available", 404)
            return make_response(response, 404)
        response_dict = data_dict.to_dict()
        response = create_success_response(data_dict)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get department details for department:" + deptName + "department" + "\n")
        return make_response(response, 200)

    # delete department
    def delete(self, deptName):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: delete department details for department:" + deptName + "department" + "\n")
        search_dept = deptName
        data_dict = DepartmentModel.query.filter(DepartmentModel.departmentName == search_dept).delete()
        db.session.commit()
        db.session.close()
        response = create_success_response(None, "Department deleted successfully")
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: delete department details for department:" + deptName + "department" + "\n")
        return make_response(response, 200)

    # update department
    @name_space.expect(departmentUpdateParser)
    def put(self, deptName):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: update department details for department:" + deptName + "department" + "\n")
        args = departmentUpdateParser.parse_args()
        departmentName, departmentDescription, createdBy = args["departmentName"], args["departmentDescription"], args[
            "createdBy"]
        update_department = DepartmentModel.query.filter(DepartmentModel.departmentName == deptName).first()
        if not update_department:
            response = create_error_response(None, "Department data not available", 404)
            flask_app.logger.debug("\nend time : " + str(
                dtime.datetime.now()) + " -- Exiting function: update department details for department:" + deptName + "department" + "\n")
            return make_response(response, 404)

        if departmentName:
            update_department.departmentName = departmentName
        if departmentDescription:
            update_department.departmentDescription = departmentDescription
        if createdBy:
            update_department.createdBy = createdBy
        db.session.add(update_department)
        db.session.commit()
        db.session.close()
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: update department details for department:" + deptName + "department" + "\n")
        return make_response(f'department {departmentName} is updated', 200)


# ALL DETAILED VIEW SECTIONS CODE

class DetailedViewSectionsModel(db.Model, SerializerMixin):
    __tablename__ = 'detailedviewsections'
    Id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    comment = db.Column(db.String(255), default=None)
    department_Name = db.Column(db.String(255), default=None)
    section_Name = db.Column(db.String(255), default=None)
    section_Value = db.Column(db.String(255), default=None)
    tId = db.Column(db.Integer, default=None)
    user_Name = db.Column(db.String(255), default=None)


detailedViewSectionsCreateParser = reqparse.RequestParser()
detailedViewSectionsCreateParser.add_argument('comment', required=True)
detailedViewSectionsCreateParser.add_argument('department_Name', required=True)
detailedViewSectionsCreateParser.add_argument('section_Name', required=True)
detailedViewSectionsCreateParser.add_argument('section_Value', required=True)
detailedViewSectionsCreateParser.add_argument('tId', required=True)
detailedViewSectionsCreateParser.add_argument('user_Name', required=True)

detailedViewSectionsUpdateParser = reqparse.RequestParser()
detailedViewSectionsUpdateParser.add_argument('comment', required=True)
detailedViewSectionsUpdateParser.add_argument('department_Name', required=True)
detailedViewSectionsUpdateParser.add_argument('section_Name', required=True)
detailedViewSectionsUpdateParser.add_argument('section_Value', required=True)
detailedViewSectionsUpdateParser.add_argument('tId', required=True)
detailedViewSectionsUpdateParser.add_argument('user_Name', required=True)

detailedViewSectionsDetailsParser = reqparse.RequestParser()
detailedViewSectionsDetailsParser.add_argument('tId', required=True)


@name_space.route("/v1/detailedviewsections")
class MainClass(Resource):
    method_decorators = [authenticate]

    # get all comment data
    def get(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get" + "detailedviewsections" + "\n")
        all_data = DetailedViewSectionsModel.query.all()
        data_list = []
        for data in all_data:
            row_dict = data.to_dict()
            data_list.append(row_dict)
        response = create_success_response(data_list)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get" + "detailedviewsections" + "\n")
        return make_response(response, 200)

    @name_space.expect(detailedViewSectionsCreateParser)
    # insert new comment
    def post(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: Insert details into" + "detailedviewsections" + "\n")
        args = detailedViewSectionsCreateParser.parse_args()
        comment, department_Name, section_Name, section_Value, tId, user_Name = args["comment"], args[
            "department_Name"], args["section_Name"], args["section_Value"], args["tId"], args["user_Name"]
        new_comment = DetailedViewSectionsModel()
        new_comment.comment = comment
        new_comment.department_Name = department_Name
        new_comment.section_Name = section_Name
        new_comment.section_Value = section_Value
        new_comment.tId = tId
        new_comment.user_Name = user_Name
        db.session.add(new_comment)
        db.session.commit()
        db.session.close()
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: Insert details into" + "detailedviewsections" + "\n")
        return make_response(create_success_response(None, f'New comment is created'), 200)


@name_space.route("/v1/detailedviewsections/<comment_id>")
class MainClass(Resource):
    method_decorators = [authenticate]

    # extract detailedviewsections details
    def get(self, comment_id):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get details for" + comment_id + "detailedviewsections" + "\n")
        data_dict = DetailedViewSectionsModel.query.filter(DetailedViewSectionsModel.Id == comment_id).first()
        if not data_dict:
            response = create_error_response(None, "Comment data not available", 404)
            flask_app.logger.debug("\nend time : " + str(
                dtime.datetime.now()) + " -- Exiting function: get details for" + comment_id + "detailedviewsections" + "\n")
            return make_response(response, 404)
        response_dict = data_dict.to_dict()
        response = create_success_response(response_dict)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get details for" + comment_id + "detailedviewsections" + "\n")
        return make_response(response, 200)

    # delete detailedviewsections
    def delete(self, comment_id):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: delete details for" + comment_id + "detailedviewsections" + "\n")
        data_dict = DetailedViewSectionsModel.query.filter(DetailedViewSectionsModel.Id == comment_id).delete()
        db.session.commit()
        db.session.close()
        response = create_success_response(None, "Comment deleted successfully")
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: delete details for" + comment_id + "detailedviewsections" + "\n")
        return make_response(response, 200)

    @name_space.expect(detailedViewSectionsUpdateParser)
    def put(self, comment_id):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: update details for" + comment_id + "detailedviewsections" + "\n")
        args = detailedViewSectionsUpdateParser.parse_args()
        comment, department_Name, section_Name, section_Value, tId, user_Name = args["comment"], args[
            "department_Name"], args["section_Name"], args["section_Value"], args["tId"], args["user_Name"]
        update_comment = DetailedViewSectionsModel.query.filter(DetailedViewSectionsModel.Id == comment_id).first()
        if not update_comment:
            response = create_error_response(None, "Comment data not available", 404)
            return make_response(response, 404)
        if comment:
            update_comment.comment = comment
        if department_Name:
            update_comment.department_Name = department_Name
        if section_Name:
            update_comment.section_Name = section_Name
        if section_Value:
            update_comment.section_Value = section_Value
        if tId:
            update_comment.tId = tId
        if user_Name:
            update_comment.user_Name = user_Name
        db.session.add(update_comment)
        db.session.commit()
        db.session.close()
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: update details for" + comment_id + "detailedviewsections" + "\n")
        return make_response(create_success_response(None, f'Comment is updated'), 200)


# ALL DOMAIN CODE


class DomainModel(db.Model, SerializerMixin):
    __tablename__ = 'domain'
    domainName = db.Column(db.String(500), primary_key=True)
    domainDescription = db.Column(db.Text, default=None)
    isDeletable = db.Column(db.Integer, default=0)
    createdBy = db.Column(db.String(255))
    departmentName = db.Column(db.String(200), default=None)


domainCreateParser = reqparse.RequestParser()
domainCreateParser.add_argument('domainName', required=True)
domainCreateParser.add_argument('domainDescription', default=None)
domainCreateParser.add_argument('isDeletable', default=0)
domainCreateParser.add_argument('createdBy', required=True)
domainCreateParser.add_argument('departmentName', default=None)

domainUpdateParser = reqparse.RequestParser()
domainUpdateParser.add_argument('domainName', default=None)
domainUpdateParser.add_argument('domainDescription', default=None)
domainUpdateParser.add_argument('isDeletable', default=None)
domainUpdateParser.add_argument('createdBy', default=None)
domainUpdateParser.add_argument('departmentName', default=None)


@name_space.route("/v1/domain")
class MainClass(Resource):
    method_decorators = [authenticate]

    def get(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get details for" + "domain" + "\n")
        all_data = DomainModel.query.all()
        response_list = []
        for data in all_data:
            response_list.append(data.to_dict())
        response = create_success_response(response_list)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get details for" + "domain" + "\n")
        return make_response(response, 200)

    @name_space.expect(domainCreateParser)
    def post(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: Insert details for" + "domain" + "\n")
        args = domainCreateParser.parse_args()
        domain_name, description, isdeletable, createdBy, departmentName = args["domainName"], args[
            "domainDescription"], args["isDeletable"], args["createdBy"], args["departmentName"]
        new_domain = DomainModel()
        new_domain.domainName = domain_name
        new_domain.domainDescription = description
        new_domain.isDeletable = isdeletable
        new_domain.createdBy = createdBy
        new_domain.departmentName = departmentName
        try:
            db.session.add(new_domain)
            db.session.commit()
            db.session.close()
        except exc.IntegrityError:
            response = create_error_response(None, "Domain name already exists", 409)
            return make_response(response, 409)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: Insert details for" + "domain" + "\n")
        return make_response(create_success_response(None, f'New Domain {domain_name} is created'), 200)


@name_space.route("/v1/domain/<inputDomain>")
class MainClass(Resource):
    method_decorators = [authenticate]

    def get(self, inputDomain):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get details for" + inputDomain + "domain" + "\n")
        search_domain = inputDomain
        db_data = DomainModel.query.filter(DomainModel.domainName == search_domain).first()
        if not db_data:
            response = create_error_response(None, "Domain Name is not found", 404)
            return make_response(response, 404)
        response = create_success_response(db_data.to_dict())
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get details for" + inputDomain + "domain" + "\n")
        return make_response(response, 200)

    def delete(self, inputDomain):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: delete details for" + inputDomain + "domain" + "\n")
        delete_domain = inputDomain
        print("DELETE Domain : ", delete_domain)
        data = DomainModel.query.filter(DomainModel.domainName == delete_domain).delete()
        db.session.commit()
        db.session.close()
        response = create_success_response(None, "Domain is deleted")
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: delete details for" + inputDomain + "domain" + "\n")
        return make_response(response, 200)

    @name_space.expect(domainUpdateParser)
    def put(self, inputDomain):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: update details for" + inputDomain + "domain" + "\n")
        args = domainUpdateParser.parse_args()
        domain_name, description, isdeletable, createdBy, departmentName = args["domainName"], args[
            "domainDescription"], args["isDeletable"], args["createdBy"], args["departmentName"]
        update_domain = DomainModel.query.filter(DomainModel.domainName == inputDomain).first()
        if not update_domain:
            response = create_error_response(None, "Domain Name is not found", 404)
            flask_app.logger.debug("\nend time : " + str(
                dtime.datetime.now()) + " -- Exiting function: update details for" + inputDomain + "domain" + "\n")
            return make_response(response, 404)
        if domain_name:
            update_domain.domainName = domain_name
        if description:
            update_domain.domainDescription = description
        if isdeletable:
            update_domain.isDeletable = isdeletable
        if createdBy:
            update_domain.createdBy = createdBy
        if departmentName:
            update_domain.departmentName = departmentName
        db.session.add(update_domain)
        db.session.commit()
        db.session.close()
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: update details for" + inputDomain + "domain" + "\n")
        return make_response(create_success_response(None, "Domain is Updated"), 200)


# ALL INSTANCE CODE


class InstanceModel(db.Model, SerializerMixin):
    __tablename__ = 'instance'
    Instance_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    create_date = db.Column(db.DateTime, default=dt.utcnow)
    created_by = db.Column(db.String(255), default=None)
    instance_description = db.Column(db.String(200), default=None)
    instance_domain = db.Column(db.String(200), default=None)
    instance_name = db.Column(db.String(200), default=None)
    updated_by = db.Column(db.String(200), default=None)
    updated_date = db.Column(db.DateTime, default=dt.utcnow, onupdate=dt.utcnow)
    departmentName = db.Column(db.String(200), default=None)


customInstanceParser = reqparse.RequestParser()
customInstanceParser.add_argument('instance_name', required=True)
customInstanceParser.add_argument('instance_domain', required=True)


@name_space.route("/v1/backend/instance/get_instance_count")
class MainClass(Resource):
    method_decorators = [authenticate_backend]
    @name_space.expect(customInstanceParser)
    def get(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get instance count for" + "instance" + "\n")
        count = dict()
        args = customInstanceParser.parse_args()
        instance = args["instance_name"]
        domain = args["instance_domain"]
        count["count"] = InstanceModel.query.filter(InstanceModel.instance_name == instance).filter(
            InstanceModel.instance_domain == domain).count()
        response = create_success_response(count)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get instance count for" + "instance" + "\n")
        return make_response(response, 200)


@name_space.route("/v1/instance/filter/instance/domain")
class MainClass(Resource):
    method_decorators = [authenticate]

    @name_space.expect(customInstanceParser)
    def get(self, instance, domain):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get instance details" + instance + domain + "insatnce" + "\n")
        args = customInstanceParser.parse_args()
        instance_in = instance
        domain_in = domain
        db_data = InstanceModel.query.filter(InstanceModel.instance_name == instance_in).filter(
            InstanceModel.instance_domain == domain_in).all()
        all_data = []
        for data in db_data:
            all_data.append(data.to_dict())
        if not all_data:
            response = create_error_response(None, "No data Found", 404)
            flask_app.logger.debug("\nend time : " + str(
                dtime.datetime.now()) + " -- Exiting function: get instance details" + instance + domain + "insatnce" + "\n")
            return make_response(response, 404)
        response = create_success_response(all_data, None)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get instance count for" + instance + domain + "insatnce" + "\n")
        return make_response(response, 200)


filterByInstanceParser = reqparse.RequestParser()
filterByInstanceParser.add_argument('instance_name', required=True)


@name_space.route("/v1/instance/filter/instance")
class MainClass(Resource):
    method_decorators = [authenticate]

    @name_space.expect(filterByInstanceParser)
    def get(self, instance):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get instance details" + instance + "insatnce" + "\n")
        args = filterByInstanceParser.parse_args()
        instance_in = instance
        db_data = InstanceModel.query.filter(InstanceModel.instance_name == instance_in).all()
        all_data = []
        for data in db_data:
            all_data.append(data.to_dict())
        if not all_data:
            response = create_error_response(None, "No data Found", 404)
            flask_app.logger.debug("\nend time : " + str(
                dtime.datetime.now()) + " -- Exiting function: get instance details" + instance + "insatnce" + "\n")
            return make_response(response, 404)
        response = create_success_response(all_data, None)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get instance details" + instance + "insatnce" + "\n")
        return make_response(response, 200)


instanceCreateParser = reqparse.RequestParser()
instanceCreateParser.add_argument('instance_name', required=True)
instanceCreateParser.add_argument('instance_description', default=None)
instanceCreateParser.add_argument('created_by', default=None)
instanceCreateParser.add_argument('instance_domain', default=None)
instanceCreateParser.add_argument('updated_by', default=None)
instanceCreateParser.add_argument('departmentName', default=None)

instanceUpdateParser = reqparse.RequestParser()
instanceUpdateParser.add_argument('instance_name', default=None)
instanceUpdateParser.add_argument('instance_description', default=None)
instanceUpdateParser.add_argument('created_by', default=None)
instanceUpdateParser.add_argument('instance_domain', default=None)
instanceUpdateParser.add_argument('updated_by', default=None)
instanceUpdateParser.add_argument('departmentName', default=None)

instanceDetailsParser = reqparse.RequestParser()
instanceDetailsParser.add_argument('Instance_id', required=True)


@name_space.route("/v1/instance")
class MainClass(Resource):
    method_decorators = [authenticate]

    # get all instance data
    def get(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: get instance details" + "insatnce" + "\n")
        all_data = InstanceModel.query.all()
        data_list = []
        for data in all_data:
            data_list.append(data.to_dict())
        response = create_success_response(data_list)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get instance details" + "insatnce" + "\n")
        return make_response(response, 200)

    @name_space.expect(instanceCreateParser)
    # insert new instance
    def post(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: Insert instance details" + "insatnce" + "\n")
        args = instanceCreateParser.parse_args()

        instance_name, instance_description, created_by, instance_domain, updated_by, departmentName = args[
                                                                                                           "instance_name"], \
                                                                                                       args[
                                                                                                           "instance_description"], \
                                                                                                       args[
                                                                                                           "created_by"], \
                                                                                                       args[
                                                                                                           "instance_domain"], \
                                                                                                       args[
                                                                                                           "updated_by"], \
                                                                                                       args[
                                                                                                           "departmentName"]
        new_instance = InstanceModel()
        new_instance.instance_name = instance_name
        new_instance.instance_description = instance_description
        new_instance.created_by = created_by
        new_instance.instance_domain = instance_domain
        new_instance.updated_by = updated_by
        new_instance.departmentName = departmentName
        db.session.add(new_instance)
        db.session.commit()
        db.session.close()
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: Insert instance details" + "insatnce" + "\n")
        return make_response(create_success_response(None, f'New Instance {instance_name} is created'), 200)


@name_space.route("/v1/instance/<check_instance_id>")
class MainClass(Resource):
    method_decorators = [authenticate]

    # extract instance details
    def get(self, check_instance_id):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: Fetch instance details for instance id" + check_instance_id + "insatnce" + "\n")
        search_instance = int(check_instance_id)
        data_dict = InstanceModel.query.filter(InstanceModel.Instance_id == search_instance).first()
        if not data_dict:
            response = create_error_response(None, "Instance data not available", 404)
            flask_app.logger.debug("\nend time : " + str(
                dtime.datetime.now()) + " -- Exiting function: Fetch instance details for instance id" + check_instance_id + "insatnce" + "\n")
            return make_response(response, 404)
        response_dict = data_dict.to_dict()
        response = create_success_response(response_dict)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: Fetch instance details for instance id" + check_instance_id + "insatnce" + "\n")
        return make_response(response, 200)

    # delete instance
    def delete(self, check_instance_id):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: Fetch instance details for instance id" + check_instance_id + "insatnce" + "\n")
        search_instance = int(check_instance_id)
        data_dict = InstanceModel.query.filter(InstanceModel.Instance_id == search_instance).delete()
        db.session.commit()
        db.session.close()
        response = create_success_response(None, "Instance deleted successfully")
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: delete instance details for instance id" + check_instance_id + "insatnce" + "\n")
        return make_response(response, 200)

    # update instance
    @name_space.expect(instanceUpdateParser)
    def put(self, check_instance_id):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: Update instance details for instance id" + check_instance_id + "insatnce" + "\n")
        search_instance = int(check_instance_id)
        args = instanceUpdateParser.parse_args()
        instance_name, instance_description, created_by, instance_domain, updated_by, departmentName = args[
                                                                                                           "instance_name"], \
                                                                                                       args[
                                                                                                           "instance_description"], \
                                                                                                       args[
                                                                                                           "created_by"], \
                                                                                                       args[
                                                                                                           "instance_domain"], \
                                                                                                       args[
                                                                                                           "updated_by"], \
                                                                                                       args[
                                                                                                           "departmentName"]
        update_instance = InstanceModel.query.filter(InstanceModel.Instance_id == search_instance).first()
        if not update_instance:
            response = create_error_response(None, "Instance data not available", 404)
            return make_response(response, 404)

        if instance_name:
            update_instance.instance_name = instance_name
        if instance_description:
            update_instance.instance_description = instance_description
        if created_by:
            update_instance.created_by = created_by
        if instance_domain:
            update_instance.instance_domain = instance_domain
        if updated_by:
            update_instance.updated_by = updated_by
        if departmentName:
            update_instance.departmentName = departmentName

        db.session.add(update_instance)
        db.session.commit()
        db.session.close()
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: update instance details for instance id" + check_instance_id + "insatnce" + "\n")
        return make_response(create_success_response(None, f'Instance {instance_name} is updated'), 200)


# ALL USER CODE


class UserModel(db.Model, SerializerMixin):
    __tablename__ = 'users'
    user_id = db.Column(db.String(255), primary_key=True)
    password = db.Column(db.LargeBinary, default=None)
    create_date = db.Column(db.DateTime, default=dt.utcnow)
    updated_date = db.Column(db.DateTime, default=dt.utcnow, onupdate=dt.utcnow)
    WelcomeFlag = db.Column(db.Integer, default=0)
    departmentName = db.Column(db.String(255), default=None)
    role = db.Column(db.String(255), default=None)
    emailId = db.Column(db.String(255), default=None)
    authToken = db.Column(db.LargeBinary, default=None)


userCreateParser = reqparse.RequestParser()
userCreateParser.add_argument('user_id', required=True)
userCreateParser.add_argument('password', required=True)
userCreateParser.add_argument('departmentName', required=True)
userCreateParser.add_argument('emailId', required=True)

userUpdateParser = reqparse.RequestParser()
userUpdateParser.add_argument('password', default=None)
userUpdateParser.add_argument('departmentName', default=None)
userUpdateParser.add_argument('emailId', default=None)
userUpdateParser.add_argument('WelcomeFlag', default=0)

userDetailsParser = reqparse.RequestParser()
userDetailsParser.add_argument('user_id', required=True)


def convert_blob_to_text(blob_string):
    return blob_string.decode("utf-8", errors="ignore")


def convert_text_to_blob(string):
    return bytes(string, encoding='utf8')


@name_space.route("/v1/users")
class MainClass(Resource):
    method_decorators = [authenticate]

    # get all user data
    def get(self):
        all_data = UserModel.query.all()
        data_list = []
        for data in all_data:
            data.password = decrypt_message(data.password)  # (data.password)
            row_dict = data.to_dict()
            del row_dict["password"]
            data_list.append(row_dict)
        response = create_success_response(data_list)
        return make_response(response, 200)

    # @name_space.expect(userCreateParser)
    # #insert new user
    # def post(self):
    #     args = userCreateParser.parse_args()
    #     user_id,password,departmentName,emailId = args["user_id"],args["password"],args["departmentName"],args["emailId"]
    #     new_user = UserModel()
    #     new_user.user_id = user_id
    #     new_user.password =  convert_text_to_blob(password)
    #     new_user.departmentName = departmentName
    #     new_user.emailId = emailId
    #     db.session.add(new_user)
    #     db.session.commit()
    #     db.session.close()
    #     return make_response(create_success_response(None,f'New User {user_id} is created'),200)


filterByDeptParser = reqparse.RequestParser()
filterByDeptParser.add_argument('departmentName', required=True)


@name_space.route("/v1/users/filter/department")
class MainClass(Resource):
    method_decorators = [authenticate]

    # extract instance details
    @name_space.expect(filterByDeptParser)
    def get(self, department):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: Fetch User details for department" + department + "department" + "\n")
        args = filterByDeptParser.parse_args()
        filter_department = department
        db_data = UserModel.query.filter(UserModel.departmentName == filter_department).all()
        all_data = []
        for data in db_data:
            all_data.append(data.to_dict())
        if not all_data:
            response = create_error_response(None, "Users data not available", 404)
            flask_app.logger.debug("\nend time : " + str(
                dtime.datetime.now()) + " -- Exiting function: Fetch User details for department" + department + "department" + "\n")
            return make_response(response, 404)
        response = create_success_response(all_data)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: Fetch User details for department" + department + "department" + "\n")
        return make_response(response, 200)


@name_space.route("/v1/users/delete/department")
class MainClass(Resource):
    # extract instance details
    @name_space.expect(filterByDeptParser)
    def get(self, department):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: delete User details for department" + department + "user" + "\n")
        args = filterByDeptParser.parse_args()
        filter_department = args['departmentName']
        db_data = UserModel.query.filter(UserModel.departmentName == filter_department).delete()
        db.session.commit()
        db.session.close()
        response = create_success_response(None, "Data is deleted successfully")
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: delete User details for department" + department + "user" + "\n")
        return make_response(response, 200)


@name_space.route("/v1/users/<search_user>")
class MainClass(Resource):
    method_decorators = [authenticate]

    # extract instance details
    def get(self, search_user):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: Fetch User details for user" + search_user + "Users" + "\n")
        data_dict = UserModel.query.filter(UserModel.user_id == search_user).first()
        if not data_dict:
            response = create_error_response(None, "User data not available", 404)
            return make_response(response, 404)
        data_dict.password = decrypt_message(data_dict.password)
        response_dict = data_dict.to_dict()
        response = create_success_response(response_dict)
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- exiting function: Fetch User details for user" + search_user + "Users" + "\n")
        return make_response(response, 200)

    # delete instance
    def delete(self, search_user):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: delete User details for user" + search_user + "Users" + "\n")
        data_dict = UserModel.query.filter(UserModel.user_id == search_user).delete()
        db.session.commit()
        db.session.close()
        response = create_success_response(None, "User deleted successfully")
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: delete User details for user" + search_user + "Users" + "\n")
        return make_response(response, 200)

    @name_space.expect(userUpdateParser)
    def put(self, search_user):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: Update User details for user" + search_user + "Users" + "\n")
        args = userUpdateParser.parse_args()
        password, departmentName, emailId, WelcomeFlag = args["password"], args["departmentName"], args["emailId"], \
                                                         args["WelcomeFlag"]
        update_user = UserModel.query.filter(UserModel.user_id == search_user).first()
        if not update_user:
            response = create_error_response(None, "User data not available", 404)
            return make_response(response, 404)
        if password:
            update_user.password = convert_text_to_blob(password)
        if departmentName:
            update_user.departmentName = departmentName
        if emailId:
            update_user.emailId = emailId
        if WelcomeFlag:
            update_user.WelcomeFlag = WelcomeFlag
        db.session.add(update_user)
        db.session.commit()
        db.session.close()
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: Update User details for user" + search_user + "Users" + "\n")
        return make_response(create_success_response(None, f'User {search_user} is updated'), 200)


@name_space.route("/v1/register")
class MainClass(Resource):
    method_decorators = [authenticate]

    @name_space.expect(userCreateParser)
    # insert new user
    def post(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: register User " + "Users" + "\n")
        args = userCreateParser.parse_args()
        user_id, password, departmentName, emailId = args["user_id"], args["password"], args["departmentName"], args[
            "emailId"]
        # check user
        new_user = UserModel()
        new_user.user_id = user_id
        new_user.password = encrypt_message(password)
        new_user.departmentName = departmentName
        new_user.emailId = emailId

        try:
            db.session.add(new_user)
            db.session.commit()
            db.session.close()
        except exc.IntegrityError:
            response = create_error_response(None, "User name already exists", 409)
            return make_response(response, 409)

        user_data = {"user_id": user_id, "departmentName": departmentName, "emailId": emailId}
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: register User " + "Users" + "\n")
        return make_response(create_success_response(user_data, f'New User {user_id} is created'), 200)


loginParser = reqparse.RequestParser()
loginParser.add_argument('user_id', required=True)
loginParser.add_argument('password', required=True)


@name_space.route("/v1/login")
class MainClass(Resource):
    @name_space.expect(loginParser)
    # get details of user
    def get(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: fetch User details" + "Users" + "\n")
        args = loginParser.parse_args()
        user_id, input_password = args['user_id'], args["password"]
        data_dict = UserModel.query.filter(UserModel.user_id == user_id).first()
        if not data_dict:
            response = create_error_response(None, "Username not available.Please try again", 404)
            flask_app.logger.debug("\nend time : " + str(
                dtime.datetime.now()) + " -- Exiting function: fetch User details" + "Users" + "\n")
            return make_response(response, 404)
        else:
            departmentName = data_dict.departmentName
            emailId = data_dict.emailId
        user_password = decrypt_message(data_dict.password)
        if input_password != user_password:
            response = create_error_response(None, "Password is incorrect. Please try again", 401)
            flask_app.logger.debug("\nend time : " + str(
                dtime.datetime.now()) + " -- Exiting function: fetch User details" + "Users" + "\n")
            return make_response(response, 401)
        # create authToken
        authToken = encrypt_message(user_id)  # create_auth_token(user_id)
        # user_data = {"user_id":data_dict.user_id,"departmentName":data_dict.departmentName,"emailId":data_dict.emailId,"authToken":authToken}
        data_dict.authToken = authToken
        db.session.add(data_dict)
        db.session.commit()
        db.session.close()
        user_data = {"user_id": user_id, "departmentName": departmentName, "emailId": emailId}
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: get User details" + "Users" + "\n")
        return make_response(create_success_response(user_data, 200), 200)


logoutParser = reqparse.RequestParser()
logoutParser.add_argument('user_id', required=True)


@name_space.route("/v1/logout")
class MainClass(Resource):
    method_decorators = [authenticate]

    @name_space.expect(logoutParser)
    # insert new user
    def put(self):
        flask_app.logger.debug("\nstart time : " + str(
            dtime.datetime.now()) + " -- In function: update User details" + "Users" + "\n")
        args = logoutParser.parse_args()
        user_id = args['user_id']
        user_data = UserModel.query.filter(UserModel.user_id == user_id).first()

        if not user_data:
            response = create_error_response(None, "Username not available.Please try again", 404)
            flask_app.logger.debug("\nend time : " + str(
                dtime.datetime.now()) + " -- Exiting function: update User details" + "Users" + "\n")
            return make_response(response, 404)
        user_data.authToken = None
        db.session.add(user_data)
        db.session.commit()
        db.session.close()
        flask_app.logger.debug("\nend time : " + str(
            dtime.datetime.now()) + " -- Exiting function: update User details" + "Users" + "\n")
        return make_response(create_success_response(None, "User logged out successfully"), 200)

    # Generates generic response in case of a successful API call


def create_success_response(result, message=None):
    """Create success response in json format

    Args:
        result (str): data
        message ([type], optional): Success Message. Defaults to None.

    Returns:
        json: response json
    """
    response_data = dict()
    response_data["status"] = "success"
    response_data["code"] = 200
    response_data["message"] = message
    response_data["data"] = result
    response_json = json.jsonify(response_data)
    return response_json


def create_error_response(result, message=None, error_code=500):
    """create error response in json format

    Args:
        result (str): data
        message (str, optional): Error Message. Defaults to None.
        error_code (int, optional): status code. Defaults to 500.

    Returns:
        json: reponse json
    """
    response_data = dict()
    response_data["status"] = "error"
    response_data["code"] = error_code
    response_data["message"] = message
    response_data["data"] = None
    response_json = json.jsonify(response_data)
    return response_json


def decode_auth_token_backend(authToken):
    """decode auth token for backend modules

    Args:
        authToken (str): auth token from backend

    Returns:
        str: status of token
    """
    try:
        payload = jwt.decode(authToken, AUTH_TOKEN_KEY, algorithms=["HS256"])
        return payload
    except jwt.ExpiredSignatureError:
        return "ERROR_SIGN: Signature expired. Please log in again."
    except jwt.InvalidTokenError:
        return "ERROR_INVALID: Invalid token. Please log in again."

flask_app.errorhandler(InternalServerError)
def handle_exception(e):
    """Return JSON instead of HTML for HTTP errors."""
    # start with the correct headers and status code from the error
    response = e.get_response()
    # replace the body with JSON
    response.data = json.dumps({
        "code": e.code,
        "status":"error",
        "message": e.name + ":" + e.description,
        "data": None
    })
    response.content_type = "application/json"
    return response
if __name__ == '__main__':
    flask_app.run(debug=False, threaded=True, port=5010)
