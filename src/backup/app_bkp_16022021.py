import werkzeug
from datetime import datetime as dt
from werkzeug.utils import cached_property
werkzeug.cached_property = cached_property
from flask import Flask, json,make_response,request
from flask_restplus import Api, Resource
from flask_restplus import reqparse
from sqlalchemy_serializer import SerializerMixin
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func
import configparser


config_path = r"C:\BUMI"

config = configparser.ConfigParser()
config.read(f"{config_path}\\db_config.ini")
print(config.sections())
MYSQL_HOST = config["MySQL"]["host"]
MYSQL_DATABASE = config["MySQL"]["database"]
MYSQL_USERNAME = config["MySQL"]["user"]
MYSQL_PASSWORD = config["MySQL"]["password"]

flask_app = Flask(__name__)
host, database, username, password = MYSQL_HOST,MYSQL_DATABASE,MYSQL_USERNAME,MYSQL_PASSWORD

uri = "mysql://" + username + ":" + password + "@" + host + "/" + database
flask_app.config['SQLALCHEMY_DATABASE_URI'] = uri
flask_app.config['SQLALCHEMY_ECHO'] = False
flask_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
flask_app.secret_key = "secret_super_key"
# flask_app.config['SQLALCHEMY_ENGINE_OPTIONS'] = {'poolclass': NullPool}

flask_app.config['SQLALCHEMY_POOL_RECYCLE'] = 180
flask_app.config['SQLALCHEMY_POOL_SIZE'] = 0
# flask_app.config['SQLALCHEMY_POOL_TIMEOUT'] = None
flask_app.config['SQLALCHEMY_MAX_OVERFLOW'] = -1
db = SQLAlchemy(flask_app)
app = Api(app=flask_app)
name_space = app.namespace('bumiapi/', description='Bumi APIs')

class Transaction(db.Model,SerializerMixin):
    __tablename__ = 'transaction'
    ID = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    no_pages = db.Column(db.Integer)
    InstanceName = db.Column(db.String(30))
    DomainType = db.Column(db.String(30))
    Filepath = db.Column(db.String(200))
    WorkingDirFilepath = db.Column(db.String(200))
    Filename = db.Column(db.String(200))
    Timestamp = db.Column(db.DateTime, default=dt.utcnow, onupdate=dt.utcnow)
    Filestatus = db.Column(db.String(50))
    Error_Description = db.Column(db.String(200))
    OutputCSVPath = db.Column(db.String(200))
    Payload_Request = db.Column(db.Text)
    Payload_Response = db.Column(db.Text)
    dateOfContract = db.Column(db.DateTime, default=dt.utcnow, onupdate=dt.utcnow)
    departmentName = db.Column(db.String(200))
    partyOne = db.Column(db.String(400))
    partyTwo = db.Column(db.String(400))
    userName = db.Column(db.String(200))
    ui_flag = db.Column(db.Integer,default=0)
    
class Customseededsections(db.Model,SerializerMixin):
    __tablename__ = 'customseededsections'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True, default=1)
    domain = db.Column(db.String(30), index=True)
    instance = db.Column(db.String(30), index=True)
    keywords = db.Column(db.String(200), index=True)
    sections = db.Column(db.String(200), index=True)
    synonyms = db.Column(db.String(200), index=True)

class CustomDomainInstance(db.Model,SerializerMixin):
    __tablename__ = 'customdomainforinstance'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True, default=1)
    domain = db.Column(db.String(30), index=True)
    instance = db.Column(db.String(30), index=True)
    keywords = db.Column(db.String(200), index=True)
    sections = db.Column(db.String(200), index=True)
    synonyms = db.Column(db.String(200), index=True)

class InstanceModel(db.Model,SerializerMixin):
    __tablename__ = 'instance'
    Instance_id = db.Column(db.Integer, primary_key=True, autoincrement=True, default=1)
    create_date  = db.Column(db.DateTime, default=dt.utcnow, onupdate=dt.utcnow)
    created_by  = db.Column(db.DateTime, default=dt.utcnow, onupdate=dt.utcnow)
    instance_description = db.Column(db.String(200), index=True)
    instance_domain = db.Column(db.String(200), index=True)
    instance_name = db.Column(db.String(200), index=True)
    updated_by = db.Column(db.String(200), index=True)
    updated_date  = db.Column(db.DateTime, default=dt.utcnow, onupdate=dt.utcnow)
    departmentName = db.Column(db.String(200), index=True)
    
class DomainModel(db.Model,SerializerMixin):
    __tablename__ = 'domain'
    domainName = db.Column(db.String(500), primary_key=True)
    domainDescription  = db.Column(db.Text, default = None)
    isDeletable  = db.Column(db.Integer, default=0)
    createdBy = db.Column(db.String(255))
    departmentName = db.Column(db.String(200), default=None)  
       
class ConfigurePath(db.Model,SerializerMixin):
    __tablename__ = 'configure_path'
    Id = db.Column(db.Integer, primary_key=True, autoincrement=True, default=1)
    Input = db.Column(db.String(30), index=True)
    Working = db.Column(db.String(30), index=True)
    Output = db.Column(db.String(200), index=True)
    License_Key = db.Column(db.String(200), index=True)
    License_Flag = db.Column(db.String(200), index=True)
    Archive = db.Column(db.String(200), index=True)
    License_Desc = db.Column(db.String(200), index=True)
    TestFile = db.Column(db.String(200), index=True)
    tempInput = db.Column(db.String(200), index=True)


class ComplimentaryDomain(db.Model,SerializerMixin):
    __tablename__ = 'complimentorydomain'
    domain = db.Column(db.String(30), index=True)
    keywords = db.Column(db.String(500), index=True)
    sections = db.Column(db.String(30), index=True)
    synonyms = db.Column(db.String(500), index=True)


domainCreateParser = reqparse.RequestParser()
domainCreateParser.add_argument('domainName',required=True)
domainCreateParser.add_argument('domainDescription',default=None)
domainCreateParser.add_argument('isDeletable',default=0)
domainCreateParser.add_argument('createdBy',required=True)
domainCreateParser.add_argument('departmentName',default=None)

domainUpdateParser = reqparse.RequestParser()
domainUpdateParser.add_argument('domainName',required=True)
domainUpdateParser.add_argument('domainDescription',default=None)
domainUpdateParser.add_argument('isDeletable',default=None)
domainUpdateParser.add_argument('createdBy',default=None)
domainUpdateParser.add_argument('departmentName',default=None)



@name_space.route("/v1/domain")
class MainClass(Resource):
    @name_space.expect(domainCreateParser)
    def post(self):
        args = domainCreateParser.parse_args()
        domain_name,description,isdeletable,createdBy,departmentName = args["domainName"],args["domainDescription"],args["isDeletable"],args["createdBy"],args["departmentName"]
        new_domain = DomainModel()
        new_domain.domainName = domain_name
        new_domain.domainDescription = description
        new_domain.isDeletable = isdeletable
        new_domain.createdBy = createdBy
        new_domain.departmentName = departmentName
        db.session.add(new_domain)
        db.session.commit()
        db.session.close()
        return make_response(f'New Domain {domain_name} is created',200)
    
    @name_space.expect(domainUpdateParser)
    def put(self):
        args = domainUpdateParser.parse_args()
        domain_name,description,isdeletable,createdBy,departmentName = args["domainName"],args["domainDescription"],args["isDeletable"],args["createdBy"],args["departmentName"]
        update_domain = DomainModel.query.filter(DomainModel.domainName == domain_name).one()
        if  description:
            update_domain.domainDescription = description
        if  isdeletable:
            update_domain.isDeletable = isdeletable
        if  createdBy:
            update_domain.createdBy = createdBy
        if  departmentName:
            update_domain.departmentName = departmentName
        db.session.add(update_domain)
        db.session.commit()
        db.session.close()
        return make_response(f'Domain {domain_name} is updated',200)
        

customSeededSectionParser = reqparse.RequestParser()
customSeededSectionParser.add_argument('domain', required=True)
customSeededSectionParser.add_argument('instance', required=True)

@name_space.route("/v1/customseededsections")
class MainClass(Resource):
    @name_space.expect(customSeededSectionParser)
    def get(self):
        args = customSeededSectionParser.parse_args()
        domain = args["domain"]
        instance = args["instance"]
        try:
            transaction = Customseededsections.query.filter(Customseededsections.domain == domain).filter(Customseededsections.instance == instance).one()
            result = transaction.to_dict()
            response = create_success_response(result)
            return make_response(response,200)
        except:
            return make_response("Data is not found",404)
        
customDomainForInstanceParser = reqparse.RequestParser()
customDomainForInstanceParser.add_argument('domain', required=True)
customDomainForInstanceParser.add_argument('instance', required=True)

@name_space.route("/v1/customdomainforinstance")
class MainClass(Resource):
    @name_space.expect(customDomainForInstanceParser)
    def get(self):
        args = customDomainForInstanceParser.parse_args()
        domain = args["domain"]
        instance = args["instance"]
        try:
            transaction = CustomDomainInstance.query.filter(CustomDomainInstance.domain == domain).filter(CustomDomainInstance.instance == instance).one()
            result = transaction.to_dict()
            response = create_success_response(result)
            return make_response(response,200)
        except:
            return make_response("Data is not found",404)

customConfigrePathParser = reqparse.RequestParser()
customConfigrePathParser.add_argument('Id')
customConfigrePathParser.add_argument('Input')
customConfigrePathParser.add_argument('Working')
customConfigrePathParser.add_argument('Output')
customConfigrePathParser.add_argument('License_Key')
customConfigrePathParser.add_argument('License_Flag')
customConfigrePathParser.add_argument('Archive')
customConfigrePathParser.add_argument('License_Desc')
customConfigrePathParser.add_argument('tempInput')
customConfigrePathParser.add_argument('TestFile')


@name_space.route("/v1/configure_path")
class MainClass(Resource):
    def get(self):
        result = dict()
        all_result = list()
        # try:
        transaction = ConfigurePath.query.limit(1).all()
        all_result = transaction[0].to_dict()
        result["tempInput"] = all_result["tempInput"]
        result["Input"] = all_result["Input"]
        result["Working"] = all_result["Working"]
        result["Output"] = all_result["Output"]
        result["Archive"] = all_result["Archive"]
        result["TestFile"] = all_result["TestFile"]
        response = create_success_response(result)
        return make_response(response, 200)

@name_space.route("/v1/configure_path")
class MainClass(Resource):
    @name_space.expect(customConfigrePathParser)
    def post(self):
        args = customConfigrePathParser.parse_args()
        configure_path = ConfigurePath()
        result = dict()
        id, input, Working, output, license_key, license_flag,  Archive, license_desc, TestFile,tempInput = args['Id'],\
             args['Input'],args['Working'],args['Output'],args['License_Key'],args['License_Flag'],args['Archive'],\
            args['License_Desc'], args['tempInput'],args['TestFile']
        configure_path.Id = id
        configure_path.Input = input
        configure_path.Working = Working
        configure_path.Output = output
        configure_path.Archive = Archive
        configure_path.TestFile = TestFile
        configure_path.tempInput = tempInput
        if license_key:
            configure_path.License_Key = license_key
        if license_desc:
            configure_path.License_Desc = license_desc
        if license_flag:
            configure_path.License_Flag = license_flag

        db.session.add(configure_path)
        db.session.flush()
        new__id = configure_path.id
        db.session.commit()
        db.session.close()
        result["configure_path Id"] = new__id
        response = create_success_response(result)
        return make_response(response, 200)

#sqlalchemy.exc.IntegrityError: (MySQLdb._exceptions.IntegrityError) (1062, "Duplicate entry '1' for key 'configure_path.PRIMARY'")


@name_space.route("/v1/configure_path")
class MainClass(Resource):
    @name_space.expect(customConfigrePathParser)
    def delete(self):
        result = dict()
        args = customConfigrePathParser.parse_args()
        id = args["Id"]
        ConfigurePath.query.filter(ConfigurePath.Id == id).delete()
        db.session.commit()
        db.session.close()
        result["Delete message"] = "Configure Path table is updated after delete"
        response = create_success_response(result)
        return make_response(response, 200)


@name_space.route("/v1/configure_path")
class MainClass(Resource):
    @name_space.expect(customConfigrePathParser)
    def put(self):
        result = dict()
        args = customConfigrePathParser.parse_args()
        id = args['Id']
        input = args['Input'],
        working = args['Working'],
        output = args['Output'],
        license_key = args['License_Key'],
        license_flag = args['License_Flag'],
        archive = args['Archive'],
        license_desc = args['License_Desc'],
        temp_input = args['tempInput'],
        test_file =  args['TestFile']
        configure_path = ConfigurePath.query.get(id)
        if input:
            configure_path.Input = input
        if output:
            configure_path.Output = output
        if archive:
            configure_path.Archive = archive
        if test_file:
            configure_path.TestFile = test_file
        if temp_input:
            configure_path.tempInput = temp_input
        db.session.add(configure_path)
        db.session.commit()
        db.session.close()
        result["update message"] = "Configure Path table is updated"
        response = create_success_response(result)
        return make_response(response, 200)


customComplimentaryDomainParser = reqparse.RequestParser()
customComplimentaryDomainParser.add_argument('domain',required=True)
customComplimentaryDomainParser.add_argument('keywords')
customComplimentaryDomainParser.add_argument('sections')
customComplimentaryDomainParser.add_argument('synonyms')


@name_space.route("/v1/ComplimentaryDomain")
class MainClass(Resource):
    @name_space.expect(customComplimentaryDomainParser)
    def get(self):
        result = dict()
        tran_list = list()
        # try:
        args = customComplimentaryDomainParser.parse_args()
        domain_name = args["domain"]
        transaction = ComplimentaryDomain.query.filter(ComplimentaryDomain.domain == domain_name)
        for tran in transaction:
            #tran_list.append(tran.to_dict())
            tran_list.append({"domain":tran[0],"keywords": tran[1],"sections":tran[2],"synonyms": tran[3]})
        print("result",tran_list)
        #tran_json = jsonify(tran_list)
        response = create_success_response(result)
        return make_response(response, 200)


#Generates generic response in case of a successful API call
def create_success_response(result):
    response_data = dict()
    response_data["status"] = "success"
    response_data["code"] = 200
    response_data["message"] = "NA"
    response_data["data"] = result
    response_json = json.dumps(response_data)
    return response_json

customInstanceParser = reqparse.RequestParser()
customInstanceParser.add_argument('instance_name', required=True)
customInstanceParser.add_argument('instance_domain', required=True)


@name_space.route("/v1/Instance/get_instance_count")
class MainClass(Resource):
    @name_space.expect(customInstanceParser)
    def get(self):
        count = dict()
        args = customInstanceParser.parse_args()
        instance = args["instance_name"]
        domain = args["instance_domain"]
        count["count"] = InstanceModel.query.filter(InstanceModel.instance_name == instance).filter(
                                                    InstanceModel.instance_domain == domain).count()
        response = create_success_response(count)
        return make_response(response,200)

customTransactionParser = reqparse.RequestParser()
customTransactionParser.add_argument('ID')
customTransactionParser.add_argument('InstanceName', required=True)
customTransactionParser.add_argument('DomainType', required=True)
customTransactionParser.add_argument('Filepath', required=True)
customTransactionParser.add_argument('Filename', required=True)
customTransactionParser.add_argument('FileStatus', required=True)
customTransactionParser.add_argument('departmentName', required=True)
customTransactionParser.add_argument('userName', required=True)
customTransactionParser.add_argument('wdfilepath_exists')
customTransactionParser.add_argument('ui_flag', default=0)
customTransactionParser.add_argument('no_pages',default=None)
customTransactionParser.add_argument('working_file_path',default=None)
customTransactionParser.add_argument('out_csv_file_path',default=None)
customTransactionParser.add_argument('error_description',default=None)
customTransactionParser.add_argument('payload_request',default=None)
customTransactionParser.add_argument('payload_response',default=None)

#insert data into transaction table
@name_space.route("/v1/transaction")
class MainClass(Resource):
    @name_space.expect(customTransactionParser)
    def post(self):
        args = customTransactionParser.parse_args()
        transaction = Transaction()
        instance, domain, filepath, filename, filestatus, uiFlag, department, username = args['InstanceName'], args[
            'DomainType'], args['Filepath'], args['Filename'], args['FileStatus'], args['ui_flag'], args['departmentName'], args['userName']

        #common instance , domaintype, department, username

        no_pages,  wdfilepath, outcsvfilepath,error_description,payload_request,payload_response = args['no_pages'], args[
            'working_file_path'], args['out_csv_file_path'], args['error_description'], args['payload_request'], args['payload_response']

        transaction.InstanceName = instance
        transaction.DomainType = domain
        transaction.Filepath = filepath
        transaction.Filename = filename
        transaction.FileStatus = filestatus
        transaction.ui_flag = uiFlag
        transaction.departmentName = department
        transaction.userName = username

        transaction.no_pages = no_pages
        transaction.InstanceName = instance
        transaction.DomainType = domain
        transaction.Filepath = filepath
        transaction.WorkingDirFilepath = wdfilepath
        transaction.OutputCSVPath = outcsvfilepath
        transaction.Filename = filename
        transaction.Filestatus = filestatus
        transaction.Error_Description = error_description
        transaction.Payload_Request = payload_request
        transaction.Payload_Response = payload_response
        transaction.departmentName = department
        transaction.userName = username

        db.session.add(transaction)
        db.session.flush()
        new_transanction_id = transaction.ID
        db.session.commit()
        db.session.close()
        return make_response(f'New Transaction {new_transanction_id} is created', 200)


updateIDParser = reqparse.RequestParser()
updateIDParser.add_argument('ID', type=int, required=True)
updateIDParser.add_argument('error_desc', type=str, default=None)
updateIDParser.add_argument('working_file_path', type=str, default=None)
updateIDParser.add_argument('payload_request', type=str, default=None)

@name_space.route("/v1/transaction")
class MainClass(Resource):
    @name_space.expect(updateIDParser)
    def put(self):
        result = dict()
        args = updateIDParser.parse_args()
        id = args['ID']
        error_desc = args["error_desc"]
        working_file_path = args["working_file_path"]
        payload_request = args["payload_request"]
        transaction = Transaction.query.filter(Transaction.ID == id).one()
        if error_desc:
            transaction.Error_Description = error_desc
        if working_file_path:
            transaction.WorkingDirFilepath = working_file_path
        if payload_request:
            transaction.Payload_Request = payload_request
        db.session.add(transaction)
        db.session.commit()
        db.session.close()
        result["update message"] = "Transaction table is updated"
        response = create_success_response(result)
        return make_response(response, 200)


# getMaxIDParser = reqparse.RequestParser()
# getMaxIDParser.add_argument('ID', type=int, required=True)

@name_space.route("/v1/transaction/get_max_transaction_id")
class MainClass(Resource):
    @name_space.expect()
    def get(self):
        result = dict()
        result["max_transaction_id"] = db.session.query(func.max(Transaction.ID)).scalar()
        response = create_success_response(result)
        return make_response(response, 200)

customTransactionFileUpdateParser = reqparse.RequestParser()
customTransactionFileUpdateParser.add_argument('ID',required=True)
customTransactionFileUpdateParser.add_argument('InstanceName')
customTransactionFileUpdateParser.add_argument('DomainType')
customTransactionFileUpdateParser.add_argument('Filepath')
customTransactionFileUpdateParser.add_argument('Filename')
customTransactionFileUpdateParser.add_argument('FileStatus')
customTransactionFileUpdateParser.add_argument('departmentName')
customTransactionFileUpdateParser.add_argument('userName')
customTransactionFileUpdateParser.add_argument('wdfilepath_exists')
customTransactionFileUpdateParser.add_argument('ui_flag', default=0)
customTransactionFileUpdateParser.add_argument('no_pages',default=None)
customTransactionFileUpdateParser.add_argument('working_file_path',default=None)
customTransactionFileUpdateParser.add_argument('out_csv_file_path',default=None)
customTransactionFileUpdateParser.add_argument('error_desc',default=None)
customTransactionFileUpdateParser.add_argument('payload_request',default=None)
customTransactionFileUpdateParser.add_argument('payload_response',default=None)

@name_space.route("/v1/transaction/file_status")
class MainClass(Resource):
    def put(self):
        result = dict()
        args = customTransactionFileUpdateParser.parse_args()
        transaction_id,instance, domain, filepath, filename, filestatus, uiFlag, department, username,wdfilepath_exists = args['ID'],args['InstanceName'], args[
            'DomainType'], args['Filepath'], args['Filename'], args['FileStatus'], args['ui_flag'], args[
                                                                                             'departmentName'], args[
                                                                                             'userName'], args['wdfilepath_exists']
        wdfilepath, payload_response,error_desc =  args['working_file_path'],args[ 'payload_response'],args['error_desc']
        transaction = Transaction.query.get(transaction_id)
        # ISSUE
        print("transaction:::",transaction)
        print("error desc from req",error_desc, filestatus)
        if transaction is not None:
            transaction.InstanceName = instance
            transaction.departmentName = department
            transaction.userName = username
            transaction.DomainType = domain
            transaction.Filepath = filepath
            transaction.WorkingDirFilepath = wdfilepath
            transaction.Filename = filename
            if not filestatus:
                transaction.Filestatus = 'Validated'
            else:
                transaction.Filestatus = filestatus
            print("transaction.Filestatus = filestatus",transaction.Filestatus )
            if not error_desc:
                transaction.Error_Description = 'No error'
            else:
                transaction.Error_Description = error_desc
            print("transaction.Error_Description", transaction.Error_Description)
            transaction.Payload_Response = payload_response
            if not wdfilepath_exists:
                transaction.Error_Description = 'Working directoryy file not exist'
                transaction.Filestatus = 'Error'
            db.session.add(transaction)
            db.session.commit()
            db.session.close()
        result["update message"] = "File status for given transaction Id is updated"
        response = create_success_response(result)
        return make_response(response, 200)

@name_space.route("/v1/transaction")
class MainClass(Resource):
    def get(self):
        tran_list = list()
        fields = ['ID', 'InstanceName']
        transaction = Transaction.query.with_entities(Transaction.ID, Transaction.Timestamp, Transaction.DomainType,
                                                      Transaction.InstanceName, Transaction.no_pages, Transaction.Filestatus,
                                                      Transaction.Error_Description).all()
        for tran in transaction:
            #tran_list.append(tran.to_dict())
            tran_list.append({"ID":tran[0],"Timestamp": tran[1],"DomainType":tran[2],"InstanceName": tran[3],
                              "no_pages":tran[4],"Filestatus": tran[5],"Error_Description":tran[6]})
        print("result",tran_list)
        #tran_json = jsonify(tran_list)
        response = create_success_response(tran_list)
        return make_response(response, 200)

if __name__ == '__main__':
    print("FLASK APP STARTED")
    flask_app.run(debug=True, threaded=True, port=5010)

