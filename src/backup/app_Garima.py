import werkzeug
from datetime import datetime as dt
from werkzeug.utils import cached_property
werkzeug.cached_property = cached_property
from flask import Flask, json,make_response,request
from flask_restplus import Api, Resource
from flask_restplus import reqparse
from sqlalchemy_serializer import SerializerMixin
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func, cast, Date
import configparser
import datetime as dtime
import jwt

config_path = r"C:\BUMI"

config = configparser.ConfigParser()
config.read(f"{config_path}\\db_config.ini")
print(config.sections())
MYSQL_HOST = config["MySQL"]["host"]
MYSQL_DATABASE = config["MySQL"]["database"]
MYSQL_USERNAME = config["MySQL"]["user"]
MYSQL_PASSWORD = config["MySQL"]["password"]

flask_app = Flask(__name__)
host, database, username, password = MYSQL_HOST,MYSQL_DATABASE,MYSQL_USERNAME,MYSQL_PASSWORD

uri = "mysql://" + username + ":" + password + "@" + host + "/" + database
flask_app.config['SQLALCHEMY_DATABASE_URI'] = uri
flask_app.config['SQLALCHEMY_ECHO'] = False
flask_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
flask_app.secret_key = "secret_super_key"
# flask_app.config['SQLALCHEMY_ENGINE_OPTIONS'] = {'poolclass': NullPool}

flask_app.config['SQLALCHEMY_POOL_RECYCLE'] = 180
flask_app.config['SQLALCHEMY_POOL_SIZE'] = 0
# flask_app.config['SQLALCHEMY_POOL_TIMEOUT'] = None
flask_app.config['SQLALCHEMY_MAX_OVERFLOW'] = -1
db = SQLAlchemy(flask_app)
app = Api(app=flask_app)
name_space = app.namespace('bumiapi/', description='Bumi APIs')

AUTH_TOKEN_KEY = "BUMIATHTOKENKEY"


class ConfigurePath(db.Model,SerializerMixin):
    """
       A class to represent a database model for configuring paths.

       ...

       Attributes
       ----------
       Id : int
           unique identifier to configure the file paths
       Input : str
           path to the input folder
       Working : str
           path to the working folder
       Output : str
           path to the output folder
       License_Key : str
           key for the licence authorisation
       License_Flag : str
       Archive : str
           path for the archive folder
       License_Desc : str
           description about the license
       TestFile : str
           path to test file
       tempInput : str
           for temporary input
       """

    __tablename__ = 'configure_path'
    Id = db.Column(db.Integer, primary_key=True, autoincrement=True, default=1)
    Input = db.Column(db.String(30), index=True)
    Working = db.Column(db.String(30), index=True)
    Output = db.Column(db.String(200), index=True)
    License_Key = db.Column(db.String(200), index=True)
    License_Flag = db.Column(db.String(200), index=True)
    Archive = db.Column(db.String(200), index=True)
    License_Desc = db.Column(db.String(200), index=True)
    TestFile = db.Column(db.String(200), index=True)
    tempInput = db.Column(db.String(200), index=True)


@name_space.route("/v1/configure_path")
class MainClass(Resource):
    def get(self):
        result = dict()
        all_result = list()
        # try:
        transaction = ConfigurePath.query.limit(1).all()
        all_result = transaction[0].to_dict()
        result["tempInput"] = all_result["tempInput"]
        result["Input"] = all_result["Input"]
        result["Working"] = all_result["Working"]
        result["Output"] = all_result["Output"]
        result["Archive"] = all_result["Archive"]
        result["TestFile"] = all_result["TestFile"]
        response = create_success_response(result)
        return make_response(response, 200)


customConfigrePathInsertParser = reqparse.RequestParser()
customConfigrePathInsertParser.add_argument('Id')
customConfigrePathInsertParser.add_argument('Input')
customConfigrePathInsertParser.add_argument('Working')
customConfigrePathInsertParser.add_argument('Output')
customConfigrePathInsertParser.add_argument('License_Key')
customConfigrePathInsertParser.add_argument('License_Flag')
customConfigrePathInsertParser.add_argument('Archive')
customConfigrePathInsertParser.add_argument('License_Desc')
customConfigrePathInsertParser.add_argument('tempInput')
customConfigrePathInsertParser.add_argument('TestFile')

@name_space.route("/v1/configure_path")
class MainClass(Resource):
    @name_space.expect(customConfigrePathInsertParser)
    def post(self):
        args = customConfigrePathInsertParser.parse_args()
        configure_path = ConfigurePath()
        result = dict()
        id, input, Working, output, license_key, license_flag,  Archive, license_desc, TestFile,tempInput = args['Id'],\
             args['Input'],args['Working'],args['Output'],args['License_Key'],args['License_Flag'],args['Archive'],\
            args['License_Desc'], args['tempInput'],args['TestFile']
        configure_path.Id = id
        configure_path.Input = input
        configure_path.Working = Working
        configure_path.Output = output
        configure_path.Archive = Archive
        configure_path.TestFile = TestFile
        configure_path.tempInput = tempInput
        if license_key:
            configure_path.License_Key = license_key
        if license_desc:
            configure_path.License_Desc = license_desc
        if license_flag:
            configure_path.License_Flag = license_flag

        db.session.add(configure_path)
        db.session.flush()
        new__id = configure_path.Id
        db.session.commit()
        db.session.close()
        result["configure_path Id"] = new__id
        response = create_success_response(result)
        return make_response(response, 200)

#sqlalchemy.exc.IntegrityError: (MySQLdb._exceptions.IntegrityError) (1062, "Duplicate entry '1' for key 'configure_path.PRIMARY'")
customConfigrePathDeleteParser = reqparse.RequestParser()
customConfigrePathDeleteParser.add_argument('Id',required= True)

@name_space.route("/v1/configure_path/<id>")
class MainClass(Resource):
    @name_space.expect(customConfigrePathDeleteParser)
    def delete(self,id):
        result = dict()
        args = customConfigrePathDeleteParser.parse_args()
        #config_id = id
        config_id = args['Id']
        ConfigurePath.query.filter(ConfigurePath.Id == config_id).delete()
        db.session.commit()
        db.session.close()
        result["Delete message"] = "Configure Path table is updated after delete"
        response = create_success_response(result)
        return make_response(response, 200)


customConfigrePathUpdateParser = reqparse.RequestParser()
customConfigrePathUpdateParser.add_argument('Id',required=True)
customConfigrePathUpdateParser.add_argument('Input')
customConfigrePathUpdateParser.add_argument('Working')
customConfigrePathUpdateParser.add_argument('Output')
customConfigrePathUpdateParser.add_argument('License_Key')
customConfigrePathUpdateParser.add_argument('License_Flag')
customConfigrePathUpdateParser.add_argument('Archive')
customConfigrePathUpdateParser.add_argument('License_Desc')
customConfigrePathUpdateParser.add_argument('tempInput')
customConfigrePathUpdateParser.add_argument('TestFile')


@name_space.route("/v1/configure_path")
class MainClass(Resource):
    @name_space.expect(customConfigrePathUpdateParser)
    def put(self):
        result = dict()
        args = customConfigrePathUpdateParser.parse_args()
        id = args['Id']
        input = args['Input'],
        working = args['Working'],
        output = args['Output'],
        license_key = args['License_Key'],
        license_flag = args['License_Flag'],
        archive = args['Archive'],
        license_desc = args['License_Desc'],
        temp_input = args['tempInput'],
        test_file =  args['TestFile']
        configure_path = ConfigurePath.query.get(id)
        if input:
            configure_path.Input = input
        if output:
            configure_path.Output = output
        if archive:
            configure_path.Archive = archive
        if test_file:
            configure_path.TestFile = test_file
        if temp_input:
            configure_path.tempInput = temp_input
        db.session.add(configure_path)
        db.session.commit()
        db.session.close()
        result["update message"] = "Configure Path table is updated"
        response = create_success_response(result)
        return make_response(response, 200)


class ComplimentaryDomain(db.Model,SerializerMixin):
    """
       A class to represent the database model complimentary domain.

       ...

       Attributes
       ----------
       domain : str
           name of the domain
       keywords : str
           all keywords
       sections : str
           all sections to identify
       synonyms : str
           all synonyms associated with the sections

       """
    __tablename__ = 'complimentorydomain'
    Id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    domain = db.Column(db.String(30), index=True)
    keywords = db.Column(db.String(500), index=True)
    sections = db.Column(db.String(30), index=True)
    synonyms = db.Column(db.String(500), index=True)


@name_space.route("/v1/complimentarydomain")
class MainClass(Resource):
    def get(self):
        result = dict()
        all_result = list()
        # try:)
        all_data = ComplimentaryDomain.query.all()
        data_list = []
        for data in all_data:
            print("data", type(data), data)
            data_list.append(data.to_dict())
        response = create_success_response(data_list)
        return make_response(response, 200)


@name_space.route("/v1/complimentarydomain/<domain_Name>")
class MainClass(Resource):
    def get(self,domain_Name):
        result = dict()
        data_dict = list()
        domain_name = domain_Name
        # try:)
        data_dict = ComplimentaryDomain.query.filter(ComplimentaryDomain.domain==domain_name).first()
        if not data_dict:
            response = create_error_response(None, "complimentorysections data not available", 404)
            return make_response(response, 404)
        response_dict = data_dict.to_dict()
        response = create_success_response(response_dict)
        return make_response(response, 200)



class ComplimentarySections(db.Model,SerializerMixin):
    __tablename__ = 'complimentorysections'
    sections = db.Column(db.String(30), primary_key=True,index=True)
    keywords = db.Column(db.String(500), index=True)
    synonyms = db.Column(db.String(500), index=True)


@name_space.route("/v1/complimentarysections")
class MainClass(Resource):
    # get all ComplimentarySections data
    def get(self):
        all_data = ComplimentarySections.query.all()
        data_list = []
        for data in all_data:
            data_list.append(data.to_dict())
        response = create_success_response(data_list)
        return make_response(response, 200)

ComplimentarySectionsParser = reqparse.RequestParser()
ComplimentarySectionsParser.add_argument('keywords')
ComplimentarySectionsParser.add_argument('synonyms')


@name_space.route("/v1/complimentarysections/<sections>")
class MainClass(Resource):
    # extract dept details
    def get(self, sections):
        sections_name = sections
        data_dict = ComplimentarySections.query.filter(ComplimentarySections.sections == sections).first()
        if not data_dict:
            response = create_error_response(None, "complimentorysections data not available", 404)
            return make_response(response, 404)
        response_dict = data_dict.to_dict()
        response = create_success_response(response_dict)
        return make_response(response, 200)

        # delete Complimentary Sections
    def delete(self, sections):
        section_name = sections
        data_dict = ComplimentarySections.query.filter(ComplimentarySections.sections == section_name).delete()
        db.session.commit()
        db.session.close()
        response = create_success_response(None, "ComplimentarySections deleted successfully")
        return make_response(response, 200)

        # update Complimentary Sections
    @name_space.expect(ComplimentarySectionsParser)
    def put(self, sections):
        sections_name = sections
        args = ComplimentarySectionsParser.parse_args()
        keywords, synonyms = args["keywords"], args["synonyms"]
        complimentary_sections = ComplimentarySections.query.filter(
            ComplimentarySections.sections == sections_name).one()
        if keywords:
            complimentary_sections.keywords = keywords
        if synonyms:
            complimentary_sections.synonyms = synonyms
        db.session.add(complimentary_sections)
        db.session.commit()
        db.session.close()
        return make_response(f'Domain {sections} is updated', 200)

ComplimentarySectionsInsertParser = reqparse.RequestParser()
ComplimentarySectionsInsertParser.add_argument('sections',required=True)
ComplimentarySectionsInsertParser.add_argument('keywords')
ComplimentarySectionsInsertParser.add_argument('synonyms')

@name_space.route("/v1/complimentarysections")
class MainClass(Resource):
    # get all complimentorysections data
    def get(self):
        all_data = ComplimentarySections.query.all()
        data_list = []
        for data in all_data:
            data_list.append(data.to_dict())
        response = create_success_response(data_list)
        return make_response(response, 200)

    @name_space.expect(ComplimentarySectionsInsertParser)
    # insert new department
    def post(self):
        args = ComplimentarySectionsInsertParser.parse_args()
        sections, keywords, synonyms = args["sections"], args["keywords"], args["synonyms"]
        complimentarySections = ComplimentarySections()
        complimentarySections.sections = sections
        complimentarySections.keywords = keywords
        complimentarySections.synonyms = synonyms
        db.session.add(complimentarySections)
        db.session.commit()
        db.session.close()
        return make_response(f'New Complimentary section {sections} is created', 200)




class CustomDomainInstance(db.Model,SerializerMixin):
    __tablename__ = 'customdomainforinstance'
    """
        A class to represent a database model for the custom domain instance table.

        ...

        Attributes
        ----------
        id : int
            unique identifier to get the custom domain instance
        domain : str
            name of the domain
        instance : str
            name of the instance
        keywords : str
            all keywords associated with that domain
        sections : str
            all sections to identify
        synonyms : str
            all synonyms to search for
        """
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    domain = db.Column(db.String(30), index=True)
    instance = db.Column(db.String(30), index=True)
    keywords = db.Column(db.String(200), index=True)
    sections = db.Column(db.String(200), index=True)
    synonyms = db.Column(db.String(200), index=True)

CustomDomainInstanceParser = reqparse.RequestParser()
CustomDomainInstanceParser.add_argument('id')
CustomDomainInstanceParser.add_argument('domain',required=True, default=None)
CustomDomainInstanceParser.add_argument('instance',required=True, default=None)
CustomDomainInstanceParser.add_argument('keywords', default=None)
CustomDomainInstanceParser.add_argument('sections', default=None)
CustomDomainInstanceParser.add_argument('synonyms', default=None)


@name_space.route("/v1/customdomainforinstance")
class MainClass(Resource):
    # get all CustomDomainInstance data
    def get(self):
        all_data = CustomDomainInstance.query.all()
        data_list = []
        for data in all_data:
            data_list.append(data.to_dict())
        response = create_success_response(data_list)
        return make_response(response, 200)

    @name_space.expect(CustomDomainInstanceParser)
    # insert new CustomDomainInstance
    def post(self):
        args = CustomDomainInstanceParser.parse_args()
        id,domain, instance, keywords,sections,synonyms = args['id'],args['domain'], args['instance'],args['keywords'],\
                    args['sections'], args['synonyms']
        customDomainInstance = CustomDomainInstance()
        customDomainInstance.id = id
        customDomainInstance.domain = domain
        customDomainInstance.instance = instance
        customDomainInstance.keywords = keywords
        customDomainInstance.sections = sections
        customDomainInstance.synonyms = synonyms
        db.session.add(customDomainInstance)
        db.session.flush()
        new_customDomainInstance_id = customDomainInstance.id
        db.session.commit()
        db.session.close()
        return make_response(f'New CustomDomainInstance for domain {domain} and instance {instance} is created with '
                             f'id {new_customDomainInstance_id}', 200)


CustomDomainInstanceUpdateParser = reqparse.RequestParser()
CustomDomainInstanceUpdateParser.add_argument('id')
CustomDomainInstanceUpdateParser.add_argument('domain', default=None)
CustomDomainInstanceUpdateParser.add_argument('instance', default=None)
CustomDomainInstanceUpdateParser.add_argument('keywords', default=None)
CustomDomainInstanceUpdateParser.add_argument('sections', default=None)
CustomDomainInstanceUpdateParser.add_argument('synonyms', default=None)


@name_space.route("/v1/customdomainforinstance/<domainName>/<InstanceName>")
class MainClass(Resource):
    # extract CustomDomainInstance details
    def get(self,domainName,InstanceName):
        search_domain = domainName
        search_instance = InstanceName
        data_dict = CustomDomainInstance.query.filter(CustomDomainInstance.domain == search_domain).\
            filter(CustomDomainInstance.instance == search_instance).first()
        if not data_dict:
            response =  create_error_response(None,"CustomDomainInstance data not available",404)
            return make_response(response,404)
        response_dict = data_dict.to_dict()
        response = create_success_response(response_dict)
        return make_response(response,200)

    # delete CustomDomainInstance
    def delete(self,domainName,InstanceName):
        search_domain = domainName
        search_instance = InstanceName
        data_dict = CustomDomainInstance.query.filter(CustomDomainInstance.domain == search_domain).\
            filter(CustomDomainInstance.instance == search_instance).delete()
        db.session.commit()
        db.session.close()
        response = create_success_response(None, "CustomDomainInstance deleted successfully")
        return make_response(response, 200)


    #update CustomDomainInstance
    @name_space.expect(CustomDomainInstanceUpdateParser)
    def put(self, domainName,InstanceName):
        search_domain = domainName
        search_instance = InstanceName
        args = CustomDomainInstanceUpdateParser.parse_args()
        keywords,sections,synonyms = args["keywords"], args["sections"], args["synonyms"]
        update_CustomDomainInstance = CustomDomainInstance.query.filter(CustomDomainInstance.domain == search_domain).\
            filter(CustomDomainInstance.instance == search_instance).first()
        if keywords:
            update_CustomDomainInstance.keywords = keywords
        if sections:
            update_CustomDomainInstance.sections = sections
        if synonyms:
            update_CustomDomainInstance.synonyms = synonyms
        db.session.add(update_CustomDomainInstance)
        db.session.commit()
        db.session.close()
        return make_response(f'CustomDomainInstance details are updated for Domain {search_domain} and Instance {search_instance}',200)


class CustomSeededsections(db.Model,SerializerMixin):
    __tablename__ = 'customseededsections'
    """
    A class to represent the database model customseededsections.

    ...

    Attributes
    ----------
    id : int
        unique identifier to get the custom seeded sections
    domain : str
        name of the domain
    instance : str
        name of the instance
    keywords : str
        all keywords associated with that section
    sections : str
        all sections to identify
    synonyms : str
        all synonyms associated with the custom sections

    """
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    domain = db.Column(db.String(30), index=True)
    instance = db.Column(db.String(30), index=True)
    keywords = db.Column(db.String(200), index=True)
    sections = db.Column(db.String(200), index=True)
    synonyms = db.Column(db.String(200), index=True)


CustomSeededsectionsParser = reqparse.RequestParser()
CustomSeededsectionsParser.add_argument('id')
CustomSeededsectionsParser.add_argument('domain',required=True, default=None)
CustomSeededsectionsParser.add_argument('instance',required=True, default=None)
CustomSeededsectionsParser.add_argument('keywords', default=None)
CustomSeededsectionsParser.add_argument('sections', default=None)
CustomSeededsectionsParser.add_argument('synonyms', default=None)


@name_space.route("/v1/customseededsections")
class MainClass(Resource):
    # get all customseededsections data
    def get(self):
        """
            gets all the custom seeded sections
            ...

            Methods
            -------
            get():
                to get all the custom seeded sections
            """
        all_data = CustomSeededsections.query.all()
        data_list = []
        for data in all_data:
            data_list.append(data.to_dict())
        response = create_success_response(data_list)
        return make_response(response, 200)

    @name_space.expect(CustomSeededsectionsParser)
    # insert new customseededsections
    def post(self):
        args = CustomSeededsectionsParser.parse_args()
        id,domain, instance, keywords,sections,synonyms = args['id'],args['domain'], args['instance'],args['keywords'],\
                    args['sections'], args['synonyms']
        customSeededsections = CustomSeededsections()
        customSeededsections.id = id
        customSeededsections.domain = domain
        customSeededsections.instance = instance
        customSeededsections.keywords = keywords
        customSeededsections.sections = sections
        customSeededsections.synonyms = synonyms
        db.session.add(customSeededsections)
        db.session.flush()
        new_customSeededsections_id = customSeededsections.id
        db.session.commit()
        db.session.close()
        return make_response(f'New new_customSeededsections_id for domain {domain} and instance {instance} is created with '
                             f'id {new_customSeededsections_id}', 200)


CustomSeededsectionsUpdateParser = reqparse.RequestParser()
CustomSeededsectionsUpdateParser.add_argument('id')
CustomSeededsectionsUpdateParser.add_argument('domain', default=None)
CustomSeededsectionsUpdateParser.add_argument('instance', default=None)
CustomSeededsectionsUpdateParser.add_argument('keywords', default=None)
CustomSeededsectionsUpdateParser.add_argument('sections', default=None)
CustomSeededsectionsUpdateParser.add_argument('synonyms', default=None)


@name_space.route("/v1/customseededsections/<domainName>/<InstanceName>")
class MainClass(Resource):
    # extract customseededsections details
    def get(self,domainName,InstanceName):
        search_domain = domainName
        search_instance = InstanceName
        data_dict = CustomSeededsections.query.filter(CustomSeededsections.domain == search_domain).\
            filter(CustomSeededsections.instance == search_instance).first()
        if not data_dict:
            response =  create_error_response(None,"CustomSeededsections data not available",404)
            return make_response(response,404)
        response_dict = data_dict.to_dict()
        response = create_success_response(response_dict)
        return make_response(response,200)

    # delete CustomSeededsections
    def delete(self,domainName,InstanceName):
        search_domain = domainName
        search_instance = InstanceName
        data_dict = CustomSeededsections.query.filter(CustomSeededsections.domain == search_domain).\
            filter(CustomSeededsections.instance == search_instance).delete()
        db.session.commit()
        db.session.close()
        response = create_success_response(None, "CustomSeededsections deleted successfully")
        return make_response(response, 200)


    #update CustomSeededsectionsUpdateParser
    @name_space.expect(CustomSeededsectionsUpdateParser)
    def put(self, domainName,InstanceName):
        search_domain = domainName
        search_instance = InstanceName
        args = CustomSeededsectionsUpdateParser.parse_args()
        keywords,sections,synonyms = args["keywords"], args["sections"], args["synonyms"]
        update_CustomSeededsections = CustomSeededsections.query.filter(CustomSeededsections.domain == search_domain).\
            filter(CustomSeededsections.instance == search_instance).first()
        if keywords:
            update_CustomSeededsections.keywords = keywords
        if sections:
            update_CustomSeededsections.sections = sections
        if synonyms:
            update_CustomSeededsections.synonyms = synonyms
        db.session.add(update_CustomSeededsections)
        db.session.commit()
        db.session.close()
        return make_response(f'CustomSeededsections details are updated for Domain {search_domain} and Instance {search_instance}',200)



class Transaction(db.Model,SerializerMixin):
    __tablename__ = 'transaction'
    """
       A class to represent a transaction.
       It is used to create a db model for the transaction table.

       ...

       Attributes
       ----------
       ID : Big int
           unique id to identify the transaction
       no_pages : int
           number of pages
       InstanceName : str
           name of the instance
       DomainType : str
           type of the domain
       Filepath : str
           path to the file
       WorkingDirFilepath : str
           path to the working directory
       Filename : str
           name of the file
       Timestamp : datetime
           time of the transaction
       Filestatus : str
           status of the file
       Error_Description : str
           shows the error description
       OutputCSVPath : str
           shows the path to the output csv folder
       Payload_Request : str
           contains the payload request
       Payload_Response : str
           contains the payload response
       dateOfContract : datetime
           date of the contract
       departmentName : str
           department name that is giving the request
       partyOne : str
       partyTwo : str
       userName : str
           shows the user's name
       ui_flag : int
       """
    ID = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    no_pages = db.Column(db.Integer)
    InstanceName = db.Column(db.String(30))
    DomainType = db.Column(db.String(30))
    Filepath = db.Column(db.String(200))
    WorkingDirFilepath = db.Column(db.String(200))
    Filename = db.Column(db.String(200))
    Timestamp = db.Column(db.DateTime, default=dt.utcnow, onupdate=dt.utcnow)
    Filestatus = db.Column(db.String(50))
    Error_Description = db.Column(db.String(200))
    OutputCSVPath = db.Column(db.String(200))
    Payload_Request = db.Column(db.Text)
    Payload_Response = db.Column(db.Text)
    dateOfContract = db.Column(db.DateTime, default=dt.utcnow, onupdate=dt.utcnow)
    departmentName = db.Column(db.String(200))
    partyOne = db.Column(db.String(400))
    partyTwo = db.Column(db.String(400))
    userName = db.Column(db.String(200))
    ui_flag = db.Column(db.Integer,default=0)


customTransactionParser = reqparse.RequestParser()
customTransactionParser.add_argument('ID')
customTransactionParser.add_argument('InstanceName', required=True)
customTransactionParser.add_argument('DomainType', required=True)
customTransactionParser.add_argument('Filepath', required=True)
customTransactionParser.add_argument('Filename', required=True)
customTransactionParser.add_argument('FileStatus', required=True)
customTransactionParser.add_argument('departmentName', required=True)
customTransactionParser.add_argument('userName', required=True)
customTransactionParser.add_argument('wdfilepath_exists')
customTransactionParser.add_argument('ui_flag', default=0)
customTransactionParser.add_argument('no_pages',default=None)
customTransactionParser.add_argument('working_file_path',default=None)
customTransactionParser.add_argument('out_csv_file_path',default=None)
customTransactionParser.add_argument('error_description',default=None)
customTransactionParser.add_argument('payload_request',default=None)
customTransactionParser.add_argument('payload_response',default=None)


@name_space.route("/v1/transaction/row_count")
class MainClass(Resource):
    # get all Transaction data
    def get(self):
        result = dict()
        result["row_count"] = Transaction.query.count()
        print("row", type(result["row_count"]))
        response = create_success_response(result)
        return make_response(response, 200)


@name_space.route("/v1/transaction/transaction_classified_count")
class MainClass(Resource):
    # get all Transaction data
    def get(self):
        result = dict()
        result["row_count"] = Transaction.query.filter(Transaction.Filestatus == "Unclassified").count()
        print("row", type(result["row_count"]))
        response = create_success_response(result)
        return make_response(response, 200)


@name_space.route("/v1/transaction/total_transactions_today")
class MainClass(Resource):
    # get all Transaction data
    def get(self):
        result = dict()
        today = str(dtime.date.today())+"%"
        result["row_count"] = Transaction.query.filter(Transaction.Timestamp.like(today)).count()
        response = create_success_response(result)
        return make_response(response, 200)


@name_space.route("/v1/transaction/total_pages")
class MainClass(Resource):
    # get all Transaction data
    def get(self):
        result = dict()
        result["total_pages"] = int(db.session.query(db.func.sum(Transaction.no_pages)).
                                    filter(Transaction.no_pages != None).scalar())
        response = create_success_response(result)
        return make_response(response, 200)



@name_space.route("/v1/transaction/total_pages_today")
class MainClass(Resource):
    # get all Transaction data
    def get(self):
        result = dict()
        today = str(dtime.date.today())+"%"
        result["total_pages"] = int(db.session.query(db.func.sum(Transaction.no_pages)).
                                    filter(Transaction.no_pages != None).filter(Transaction.Timestamp.like(today)).scalar())
        response = create_success_response(result)
        return make_response(response, 200)


@name_space.route("/v1/Transaction")
class MainClass(Resource):
    # get all Transaction data
    def get(self):
        all_data = Transaction.query.all()
        data_list = []
        for data in all_data:
            data_list.append(data.to_dict())
        response = create_success_response(data_list)
        return make_response(response, 200)

    @name_space.expect(customTransactionParser)
    # insert new transaction
    def post(self):
        args = customTransactionParser.parse_args()

        instance, domain, filepath, filename, filestatus, uiFlag, department, username = args['InstanceName'], args[
            'DomainType'], args['Filepath'], args['Filename'], args['FileStatus'], args['ui_flag'], args[
                                                                                             'departmentName'], args[
                                                                                             'userName']
        result = dict()
        # common instance , domaintype, department, username

        no_pages, wdfilepath, outcsvfilepath, error_description, payload_request, payload_response = args['no_pages'], \
                args['working_file_path'], args[ 'out_csv_file_path'], args['error_description'], args['payload_request'],\
                args['payload_response']
        transaction = Transaction()
        transaction.InstanceName = instance
        transaction.DomainType = domain
        transaction.Filepath = filepath
        transaction.Filename = filename
        transaction.FileStatus = filestatus
        transaction.ui_flag = uiFlag
        transaction.departmentName = department
        transaction.userName = username

        transaction.no_pages = no_pages
        transaction.InstanceName = instance
        transaction.DomainType = domain
        transaction.Filepath = filepath
        transaction.WorkingDirFilepath = wdfilepath
        transaction.OutputCSVPath = outcsvfilepath
        transaction.Filename = filename
        transaction.Filestatus = filestatus
        transaction.Error_Description = error_description
        transaction.Payload_Request = payload_request
        transaction.Payload_Response = payload_response
        transaction.departmentName = department
        transaction.userName = username

        db.session.add(transaction)
        db.session.flush()
        new_transanction_id = transaction.ID
        db.session.commit()
        db.session.close()
        result["transaction_id"] = new_transanction_id
        response = create_success_response(result)
        return make_response(response, 200)


customTransactionFileUpdateParser = reqparse.RequestParser()
customTransactionFileUpdateParser.add_argument('ID', type=int, required=True)
customTransactionFileUpdateParser.add_argument('Error_Description', type=str, default=None)
customTransactionFileUpdateParser.add_argument('WorkingDirFilepath', type=str, default=None)
customTransactionFileUpdateParser.add_argument('Payload_Request', type=str, default=None)
customTransactionFileUpdateParser.add_argument('Payload_Response', type=str, default=None)
customTransactionFileUpdateParser.add_argument('InstanceName')
customTransactionFileUpdateParser.add_argument('DomainType')
customTransactionFileUpdateParser.add_argument('Filepath')
customTransactionFileUpdateParser.add_argument('Filename')
customTransactionFileUpdateParser.add_argument('FileStatus')
customTransactionFileUpdateParser.add_argument('departmentName')
customTransactionFileUpdateParser.add_argument('userName')
customTransactionFileUpdateParser.add_argument('wdfilepath_exists')
customTransactionFileUpdateParser.add_argument('ui_flag', default=0)
customTransactionFileUpdateParser.add_argument('no_pages',default=None)
customTransactionFileUpdateParser.add_argument('OutputCSVPath',default=None)


@name_space.route("/v1/transaction/<transaction_id>")
class MainClass(Resource):
    # extract transaction details
    def get(self, transaction_id):
        search_tran = transaction_id
        data_dict = Transaction.query.filter(Transaction.ID == search_tran).first()
        if not data_dict:
            response = create_error_response(None, "Transaction data not available", 404)
            return make_response(response, 404)
        response_dict = data_dict.to_dict()
        response = create_success_response(data_dict)
        return make_response(response, 200)

    # delete transaction
    def delete(self, transaction_id):
        search_tran = transaction_id
        data_dict = Transaction.query.filter(Transaction.ID == search_tran).delete()
        db.session.commit()
        db.session.close()
        response = create_success_response(None, "Transaction deleted successfully")
        return make_response(response, 200)

    # update department
    @name_space.expect(customTransactionFileUpdateParser)
    def put(self, transaction_id):
        result = dict()
        args = customTransactionFileUpdateParser.parse_args()
        transaction_id, instance, domain, filepath, filename, filestatus, uiFlag, department, username, wdfilepath_exists = \
        args['ID'], args['InstanceName'], args[
            'DomainType'], args['Filepath'], args['Filename'], args['FileStatus'], args['ui_flag'], args[
            'departmentName'], args[
            'userName'], args['wdfilepath_exists']
        wdfilepath, payload_response, error_desc,outputCSVPath = args['working_file_path'], args['payload_response'], args[
            'error_desc'], args['OutputCSVPath']
        transaction = Transaction.query.get(transaction_id)
        if transaction is not None:
            if instance:
                transaction.InstanceName = instance
            if department:
                transaction.departmentName = department
            if username:
                transaction.userName = username
            if domain:
                transaction.DomainType = domain
            if filepath:
                transaction.Filepath = filepath
            if wdfilepath:
                transaction.WorkingDirFilepath = wdfilepath
            if filename:
                transaction.Filename = filename
            if not filestatus:
                transaction.Filestatus = 'Validated'
            else:
                transaction.Filestatus = filestatus
            if not error_desc:
                transaction.Error_Description = 'No error'
            else:
                transaction.Error_Description = error_desc
            transaction.Payload_Response = payload_response
            if wdfilepath_exists== "No":
                transaction.Error_Description = 'Working directory file not exist'
                transaction.Filestatus = 'Error'
            db.session.add(transaction)
            db.session.commit()
            db.session.close()
        result["update message"] = "File status for given transaction Id is updated"
        response = create_success_response(result)
        return make_response(response, 200)


customTransactionMaxParser = reqparse.RequestParser()
customTransactionMaxParser.add_argument('auth_token',required=True)
@name_space.route("/v1/transaction/get_max_transaction_id")
class MainClass(Resource):
    @name_space.expect(customTransactionMaxParser)
    def get(self):
        result = dict()
        auth_token = request.args.get('auth_token', '')
        auth_response = decode_auth_token(auth_token)
        if "ERROR" not in decode_auth_token(auth_token):
            result["max_transaction_id"] = db.session.query(func.max(Transaction.ID)).scalar()
        else:
            status_code = 401
            response = create_error_response(auth_response, auth_response, status_code)
            return make_response(response, status_code)
        response = create_success_response(result)
        return make_response(response, 200)


#Generates generic response in case of a successful API call
def create_success_response(result,message=None):
    response_data = dict()
    response_data["status"] = "success"
    response_data["code"] = 200
    response_data["message"] = message
    response_data["data"] = result
    response_json = json.jsonify(response_data)
    return response_json


def create_error_response(result,message=None,error_code=500):
    response_data = dict()
    response_data["status"] = "error"
    response_data["code"] = error_code
    response_data["message"] = message
    response_data["data"] = None
    response_json = json.jsonify(response_data)
    return response_json


def decode_auth_token(authToken):
    try:
        payload = jwt.decode(authToken, AUTH_TOKEN_KEY, algorithms=["HS256"])
        return payload
    except jwt.ExpiredSignatureError:
        return "ERROR: Signature expired. Please log in again."
    except jwt.InvalidTokenError:
        return "ERROR: Invalid token. Please log in again."



if __name__ == '__main__':
    print("FLASK APP STARTED")
    flask_app.run(debug=True, threaded=True, port=5010)

