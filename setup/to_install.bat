@echo off
call cd C:\Users\Admin\Documents\HIGHIQ\BHUMI\JUPYTER\database_framework\setup\venv
call Scripts\activate
call python -m pip install --upgrade pip
call cd C:\Users\Admin\Documents\HIGHIQ\BHUMI\JUPYTER\database_framework\setup
call pip install -r C:\Users\Admin\Documents\HIGHIQ\BHUMI\JUPYTER\database_framework\requirements.txt
