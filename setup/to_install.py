import os
import shutil
import configparser


from pathlib import Path
os.system("pip install pathlib")
pwd = Path.cwd()
config_path = "C:/BUMI/config/db_config.ini"
config_path_obj = Path(config_path)
env_path = pwd /"venv"
python_path = env_path /"Scripts/python.exe"
requirement_text_path = pwd.parent / "requirements.txt"
run_bat = pwd.parent / "run.bat"
app_path = pwd.parent /"src/app.py"
db_config = pwd.parent / "db_config.ini"
if not config_path_obj.exists():
    print("Message: Setting db credentials")
    config_path_obj.parent.mkdir(parents=True,exist_ok=True)
    shutil.copy(db_config,config_path)
    parser = configparser.ConfigParser()
    parser.read(f"{config_path}")
    host_name = input("PLEASE PROVIDE HOST NAME:")
    database_name = input("PLEASE PROVIDE DATABASE NAME:")
    user_name = input("PLEASE PROVIDE USER NAME:")
    password = input("PLEASE PROVIDE USER PASSWORD:")
    parser.set("mySQLDB","host",str(host_name))
    parser.set("mySQLDB","database",str(database_name))
    parser.set("mySQLDB","user",str(user_name))
    parser.set("mySQLDB","password",str(password))
    # Writing our configuration file to 'example.ini'
    with open(str(config_path_obj), 'w') as configfile:
        parser.write(configfile)
    

#DB CONFIG SETUP


if env_path.exists():
    print("Message: Python Environment is setup already.")
    if run_bat.exists():
        print(f"Message: Please run.bat file in the below path {run_bat}")
    else:
        print("Creating batch file to run server")
        bat = open(f"{run_bat}","w")
        bat.write("@echo off\n")
        bat.write(f"call "+ f"{python_path}"+" "+ f"{str(app_path)}" +"\n")
        bat.close()
        print(f"Message: Please run.bat file in the below path {run_bat} to start the server")
    os.system()
else:
    print("Message: Python Environment setup started")
    os.system(f"python -m venv {str(env_path)}")
    bat = open("to_install.bat","w")
    bat.write("@echo off\n")
    bat.write(f"call cd {str(env_path)}"+"\n")
    bat.write("call Scripts\\activate"+"\n")
    bat.write("call python -m pip install --upgrade pip"+"\n")
    bat.write(f"call cd {str(pwd)}"+"\n")
    bat.write(f"call pip install -r {requirement_text_path}"+"\n")
    bat.close()
    os.system("to_install.bat")
    print("Message: Python Environment setup completed")
    print("Creating batch file to run server")
    bat = open(f"{run_bat}","w")
    bat.write("@echo off\n")
    bat.write(f"call "+ f"{python_path}"+" "+ f"{str(app_path)}" +"\n")
    bat.close()
    print(f"Message: Please run.bat file in the below path {run_bat} to start the server")